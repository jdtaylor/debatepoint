#
# This file is part of debatepoint.com - web-based debate software
#
#   Copyright (C) 2004  James D. Taylor
# 
#   debatepoint is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   any later version.
# 
#   debatepoint is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with debatepoint; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Contact James D. Taylor:  james.d.taylor@gmail.com
#

# MySQL dump 8.14
#
# Host: localhost    Database: 
#--------------------------------------------------------
# Server version	3.23.39-log

#
# Current Database: debatepoint
#

CREATE DATABASE /*!32312 IF NOT EXISTS*/ debatepoint;

USE debatepoint;

DROP TABLE IF EXISTS sys;
CREATE TABLE sys (
  sysId int(32),
  hits_user int(32) default 0,
  hits_anon int(32) default 0,
  PRIMARY KEY  (sysId)
) TYPE=MYISAM PACK_KEYS=1;

INSERT INTO sys (sysId) VALUES (0);

#
# Table structure for table 'user'
#
# sort_method:
#	0: no-op
#	1: by score
#	2: by date
#	3: by number of replies
#
# sort_filter:
#	0: no filtering
#	1: < 10% score
#	2: < 25%
#	3: < 50%
#	4: < 75%
#

DROP TABLE IF EXISTS user;
CREATE TABLE user (
  userId int(32) NOT NULL auto_increment,
  login char(32) default NULL,
  pass char(32) default NULL,
  pass_enc text default NULL,
  email char(128) default NULL,
  modgroup int(32) default 0,
  partyId int(10) default NULL,
  party char(32) default NULL,
  district int(32) default NULL,
  created datetime default NULL,
  score int(32) default 0,
  sort_method int(4) default 1,
  sort_reverse char(1) default 0,
  sort_filter float default 0.0,

  INDEX(login),
  PRIMARY KEY  (userId)
) TYPE=MYISAM PACK_KEYS=1;

INSERT INTO user (userId,login,pass) VALUES (1,"anonymous",PASSWORD("anonymous"));
INSERT INTO user (userId,login,pass) VALUES (10001,"consensus",PASSWORD(""));


#DROP TABLE IF EXISTS user_group;
#CREATE TABLE user_group (
#  groupId int(32) NOT NULL auto_increment,
#  title char(32) default NULL,
#  descr char(128) default NULL,
#
#  PRIMARY KEY  (groupId)
#) TYPE=MYISAM PACK_KEYS=1;
#
#DROP TABLE IF EXISTS group_user;
#CREATE TABLE group_user (
#  groupId int(32) NOT NULL,
#  userId int(32) NOT NULL,
#  level char(1) default 0,
#
#  PRIMARY KEY (groupId.userId)
#) TYPE=MYISAM PACK_KEYS=1;


DROP TABLE IF EXISTS party;
CREATE TABLE party (
  partyId int(10) NOT NULL auto_increment,
  title char(32) default NULL,
  PRIMARY KEY (partyId)
) TYPE=MYISAM PACK_KEYS=1;

#
# Table structure for table 'point'
#
DROP TABLE IF EXISTS point;
CREATE TABLE point (
  pointId int(32) NOT NULL auto_increment,
  text text default NULL,
  date datetime default NULL,
  submitter int(32) NOT NULL,
  tags char(255) default NULL,

  PRIMARY KEY  (pointId)
) TYPE=MYISAM PACK_KEYS=1;

#
# individual field of a sparse matrix (user vs point)
# stance contains the stance of the user on the point.
#
# effective: effective score, (pos-neg) - (score * oppose/support)
# support: summation of abs positive child argument effective scores
# oppose: summation of abs negative child argument effective scores
#
DROP TABLE IF EXISTS stance;
CREATE TABLE stance (
  stanceId int(32) NOT NULL auto_increment,
  pointId int(32) NOT NULL,
  userId int(32),
  effective float default 0.0,
  support float default 0.0,
  oppose float default 0.0,
  certainty float default 0.0,
  repliesSupport int(32) default 0,
  repliesOppose int(32) default 0,
  repliesTotal int(32) default 0,
  comments int(32) default 0,
  modified datetime default NULL,
  updated tinyint(1) default 0,

  INDEX(userId),
  INDEX(pointId,userId),
  PRIMARY KEY  (stanceId)

) TYPE=MYISAM PACK_KEYS=1;

#
# Table structure for table 'argument'
#
# stance: 0==opposes, 1==supports, 2==comment
# scorePos: summation of positive moderation
# scoreNeg: summation of negative moderation
#
DROP TABLE IF EXISTS argument;
CREATE TABLE argument (
  argumentId int(32) NOT NULL auto_increment,
  stanceId int(32) NOT NULL,
  parentId int(32) default NULL,
  stance int(2) default 0,
  userId int(32) NOT NULL,
  stanceLinkId int(32) default NULL,
  title char(64) default NULL,
  text text default NULL,
  tags char(255) default NULL,
  effective float default 0.0,
  scorePos int(32) default 0,
  scoreNeg int(32) default 0,
  stancePos int(32) default 0,
  stanceNeg int(32) default 0,
  stanceEffective float default 0.0,
  repliesSupport int(32) default 0,
  repliesOppose int(32) default 0,
  repliesTotal int(32) default 0,
  support float default 0.0,
  oppose float default 0.0,
  comments int(32) default 0,
  date datetime default NULL,
  modgroup int(32) NOT NULL,

  INDEX(stanceId,parentId),
  INDEX(userId),
  PRIMARY KEY  (argumentId)

) TYPE=MYISAM PACK_KEYS=1;

#
# row for each usage of a tag in an argument or point
#
DROP TABLE IF EXISTS tag_map;
CREATE TABLE tag_map (
  tagMapId int(32) NOT NULL auto_increment,
  tag char(32) default NULL,
  type int(3) default 0,
  id int(32) default 0,

  INDEX(tag),
  PRIMARY KEY  (tagMapId)

) TYPE=MYISAM PACK_KEYS=1;

#
# Link argument score to stance score
#
DROP TABLE IF EXISTS stance_link;
CREATE TABLE stance_link (
  stanceLinkId int(32) NOT NULL auto_increment,
  stanceId int(32) NOT NULL,
  argumentId int(32) NOT NULL,

  INDEX(stanceId,argumentId),
  PRIMARY KEY  (stanceLinkId)

) TYPE=MYISAM PACK_KEYS=1;

#
# rows of point categories
# there can be many points in each topic
# a point can also be in many categories
#
DROP TABLE IF EXISTS topic;
CREATE TABLE topic (
  topicId int(32) NOT NULL auto_increment,
  title char(64) default NULL,
  PRIMARY KEY  (topicId)
) TYPE=MYISAM PACK_KEYS=1;


#
# row for each point/topic link
#
DROP TABLE IF EXISTS point_topic;
CREATE TABLE point_topic (
  pointTopicId int(32) NOT NULL auto_increment,
  pointId int(32) NOT NULL,
  topicId int(32) NOT NULL,

  INDEX(pointId,topicId),
  PRIMARY KEY  (pointTopicId)

) TYPE=MYISAM PACK_KEYS=1;
  
#
# userId is id of user that is moderating
#
#	result:
#		0: arg1 wins over arg2
#		1: arg2 wins over arg1
#		2: both are redundant
#
DROP TABLE IF EXISTS moderation;
CREATE TABLE moderation (
  modId int(32) NOT NULL auto_increment,
  argId1 int(32) NOT NULL,
  argId2 int(32) NOT NULL,
  userId int(32) NOT NULL,
  score int(32) default 0,
  result int(32) default 0,
  date datetime default NULL,

  INDEX(argId1,argId2,userId),
  INDEX(userId),
  PRIMARY KEY  (modId)

) TYPE=MYISAM PACK_KEYS=1;

DROP TABLE IF EXISTS debate;
CREATE TABLE debate (
  debateId int(32) NOT NULL auto_increment,
  title char(32) default NULL,
  PRIMARY KEY (debateId)
) TYPE=MYISAM PACK_KEYS=1;

DROP TABLE IF EXISTS debate_user;
CREATE TABLE debate_user (
  id int(32) NOT NULL auto_increment,
  debateId int(32) NOT NULL,
  userId int(32) NOT NULL,

  INDEX(userId,debateId),
  PRIMARY KEY  (id)

) TYPE=MYISAM;

DROP TABLE IF EXISTS debate_point;
CREATE TABLE debate_point (
  id int(32) NOT NULL auto_increment,
  debateId int(32) NOT NULL,
  pointId int(32) NOT NULL,

  INDEX(pointId,debateId),
  PRIMARY KEY  (id)

) TYPE=MYISAM;

DROP TABLE IF EXISTS bill;
CREATE TABLE bill (
  billId int(32) NOT NULL auto_increment,
  session int(32) default NULL,
  type char(16) default NULL,
  number int(32) NOT NULL,
  introduced datetime default NULL,
  sponsor int(32) default NULL,
  title_short char(255) default NULL,
  title text default NULL,
  signed datetime default NULL,
  PRIMARY KEY  (billId)
) TYPE=MYISAM PACK_KEYS=1;

DROP TABLE IF EXISTS bill_vote;
CREATE TABLE bill_vote (
  id int(32) NOT NULL auto_increment,
  billId int(32) NOT NULL,
  userId int(32) NOT NULL,
  vote char(3) NOT NULL,
  PRIMARY KEY  (id)
) TYPE=MYISAM PACK_KEYS=1;

#
# representative term
#
DROP TABLE IF EXISTS rep_term;
CREATE TABLE rep_term (
  termId int(32) NOT NULL auto_increment,
  userId int(32) NOT NULL,
  type char(16) default NULL,
  start datetime default NULL,
  end datetime default NULL,
  partyId int(10) default NULL,
  party char(32) default NULL,
  osid char(16) default NULL,
  state char(3) default NULL,
  district int(32) default NULL,
  class char(16) default NULL,
  url char(255) default NULL,
  address char(255) default NULL,
  phone char(16) default NULL,
  email char(64) default NULL,
  PRIMARY KEY  (termId)
) TYPE=MYISAM PACK_KEYS=1;

DROP TABLE IF EXISTS quiz;
CREATE TABLE quiz (
  quizId int(32) NOT NULL auto_increment,
  debateId int(32) NOT NULL,
  title char(32) default NULL,
  PRIMARY KEY (quizId)
) TYPE=MYISAM PACK_KEYS=1;

#
# Table structure for table 'sessions'
#

DROP TABLE IF EXISTS sessions;
CREATE TABLE sessions (
  sessionId int(11) NOT NULL auto_increment,
  userId int(32) default NULL,
  ipaddr char(15) default NULL,
  cookie char(128) default NULL,
  expires datetime default NULL,
  mode int(32) default 0,
  argStyle int(32) default 2,
  modArg1 int(32) default 0,
  modArg2 int(32) default 0,
  modCount int(3) default 0,
  sort_method int(4) default 1,
  sort_reverse char(1) default 0,
  sort_filter float default 0.0,
  PRIMARY KEY  (sessionId)
) TYPE=MYISAM PACK_KEYS=1;


