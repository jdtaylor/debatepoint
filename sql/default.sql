#
# This file is part of debatepoint.com - web-based debate software
#
#   Copyright (C) 2004  James D. Taylor
# 
#   debatepoint is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   any later version.
# 
#   debatepoint is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with debatepoint; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Contact James D. Taylor:  james.d.taylor@gmail.com
#

USE debatepoint;

INSERT INTO user (userId,login,pass,modgroup) VALUES (1000001,"metric",PASSWORD("metric"),1);

INSERT INTO argument (argumentId,stanceId,parentId,stance,userId,title) VALUES ('2048','0','0','2','1000001','news');

INSERT INTO topic (topicId,title) VALUES (1,"politics");
INSERT INTO topic (topicId,title) VALUES (2,"philosophy");
INSERT INTO topic (topicId,title) VALUES (3,"religion");
INSERT INTO topic (topicId,title) VALUES (4,"science");
INSERT INTO topic (topicId,title) VALUES (5,"technology");
INSERT INTO topic (topicId,title) VALUES (6,"world affairs");
INSERT INTO topic (topicId,title) VALUES (7,"education");
INSERT INTO topic (topicId,title) VALUES (8,"business");

INSERT INTO point (pointId,text,date,submitter) VALUES (1, "states should adopt approval voting", "2004-11-18 10:42:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (1, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (1, 1, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (2, "Roosevelt's New Deal lifted the US out of the Great Depression", "2004-11-29 10:42:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (2, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (2, 2, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (3, "abolish the federal reserve", "2004-11-18 02:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (3, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (3, 3, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (4, "recreational drugs shouldn't be outlawed", "2004-11-18 03:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (4, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (4, 4, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (5, "the Republican party is fiscally liberal", "2004-11-18 04:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (5, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (5, 5, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (6, "government is responsible for education", "2004-11-18 05:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (6, 1);
INSERT INTO point_topic (pointId,topicId) VALUES (6, 2);
INSERT INTO stance (stanceId,pointId,userId) VALUES (6, 6, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (7, "government is responsible for the economy", "2004-11-18 06:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (7, 1);
INSERT INTO point_topic (pointId,topicId) VALUES (7, 2);
INSERT INTO stance (stanceId,pointId,userId) VALUES (7, 7, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (8, "private property should be protected by government", "2004-11-18 07:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (8, 1);
INSERT INTO point_topic (pointId,topicId) VALUES (8, 2);
INSERT INTO stance (stanceId,pointId,userId) VALUES (8, 8, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (9, "violence is always bad", "2004-11-18 08:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (9, 2);
INSERT INTO stance (stanceId,pointId,userId) VALUES (9, 9, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (10, "religion is a human behavioural instinct", "2004-11-18 09:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (10, 2);
INSERT INTO point_topic (pointId,topicId) VALUES (10, 3);
INSERT INTO stance (stanceId,pointId,userId) VALUES (10, 10, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (11, "government is worse than corporations", "2004-12-01 09:11:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (11, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (11, 11, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (12, "time travel into the past is possible", "2004-11-18 11:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (12, 2);
INSERT INTO point_topic (pointId,topicId) VALUES (12, 4);
INSERT INTO stance (stanceId,pointId,userId) VALUES (12, 12, 10001);


INSERT INTO point (pointId,text,date,submitter) VALUES (13, "global warming is happening", "2004-11-18 12:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (13, 4);
INSERT INTO stance (stanceId,pointId,userId) VALUES (13, 13, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (14, "global warming should be prevented", "2004-11-18 13:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (14, 4);
INSERT INTO stance (stanceId,pointId,userId) VALUES (14, 14, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (15, "Iraq had ties to 9/11", "2004-11-18 14:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (15, 6);
INSERT INTO stance (stanceId,pointId,userId) VALUES (15, 15, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (16, "democracy is two wolves and a sheep deciding what's for dinner.", "2004-12-01 14:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (16, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (16, 16, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (17, "the US should ratify the kyoto protocol", "2004-11-18 16:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (17, 6);
INSERT INTO stance (stanceId,pointId,userId) VALUES (17, 17, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (18, "teach evolution instead of creationism", "2004-11-18 17:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (18, 7);
INSERT INTO stance (stanceId,pointId,userId) VALUES (18, 18, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (19, "fox news has a conservative political bias", "2004-11-18 18:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (19, 1);
INSERT INTO point_topic (pointId,topicId) VALUES (19, 8);
INSERT INTO stance (stanceId,pointId,userId) VALUES (19, 19, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (20, "cnn has a liberal political bias", "2004-11-18 19:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (20, 1);
INSERT INTO point_topic (pointId,topicId) VALUES (20, 8);
INSERT INTO stance (stanceId,pointId,userId) VALUES (20, 20, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (21, "linux will eventually overtake microsoft", "2004-11-18 20:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (21, 5);
INSERT INTO point_topic (pointId,topicId) VALUES (21, 8);
INSERT INTO stance (stanceId,pointId,userId) VALUES (21, 21, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (22, "vim is a better text editor than emacs", "2004-11-23 21:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (22, 5);
INSERT INTO stance (stanceId,pointId,userId) VALUES (22, 22, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (23, "abolish the US electoral system", "2004-12-02 21:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (23, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (23, 23, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (24, "automation will eventually decrease the number of jobs", "2004-12-03 21:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (24, 2);
INSERT INTO point_topic (pointId,topicId) VALUES (24, 5);
INSERT INTO point_topic (pointId,topicId) VALUES (24, 8);
INSERT INTO stance (stanceId,pointId,userId) VALUES (24, 24, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (25, "PHP is a good language for web applications", "2004-12-18 21:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (25, 5);
INSERT INTO stance (stanceId,pointId,userId) VALUES (25, 25, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (26, "national id card", "2004-12-18 21:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (26, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (26, 26, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (27, "government can regulate radio waves", "2004-12-18 21:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (27, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (27, 27, 10001);


INSERT INTO point (pointId,text,date,submitter) VALUES (40, "legal abortion (during first trimester)", "2004-11-18 22:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (40, 1);
INSERT INTO point_topic (pointId,topicId) VALUES (40, 3);
INSERT INTO stance (stanceId,pointId,userId) VALUES (40, 40, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (41, "affirmative action", "2004-11-18 23:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (41, 1);
INSERT INTO point_topic (pointId,topicId) VALUES (41, 8);
INSERT INTO stance (stanceId,pointId,userId) VALUES (41, 41, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (42, "United Nations", "2004-11-16 01:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (42, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (42, 42, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (43, "public school coordinated prayer", "2004-11-18 01:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (43, 1);
INSERT INTO point_topic (pointId,topicId) VALUES (43, 7);
INSERT INTO stance (stanceId,pointId,userId) VALUES (43, 43, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (44, "free trade", "2004-11-11 00:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (44, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (44, 44, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (45, "gun ownership", "2004-11-20 00:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (45, 1);
INSERT INTO stance (stanceId,pointId,userId) VALUES (45, 45, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (46, "privatize Social Security", "2004-11-18 01:12:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (46, 1);
INSERT INTO point_topic (pointId,topicId) VALUES (46, 8);
INSERT INTO stance (stanceId,pointId,userId) VALUES (46, 46, 10001);

INSERT INTO point (pointId,text,date,submitter) VALUES (47, "death penalty", "2004-11-22 00:00:00", 10001);
INSERT INTO point_topic (pointId,topicId) VALUES (47, 5);
INSERT INTO stance (stanceId,pointId,userId) VALUES (47, 47, 10001);


