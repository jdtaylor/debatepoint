#
# This file is part of debatepoint.com - web-based debate software
#
#   Copyright (C) 2004  James D. Taylor
# 
#   debatepoint is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   any later version.
# 
#   debatepoint is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with debatepoint; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Contact James D. Taylor:  james.d.taylor@gmail.com
#

# MySQL dump 8.14
#
# Host: localhost    Database: 
#--------------------------------------------------------
# Server version	3.23.39-log

USE debatepoint;

INSERT INTO party (partyId,title) VALUES (100,"Republican");
INSERT INTO party (partyId,title) VALUES (101,"Democratic");
INSERT INTO party (partyId,title) VALUES (102,"Libertarian");
INSERT INTO party (partyId,title) VALUES (103,"Green");
INSERT INTO party (partyId,title) VALUES (104,"Constitution");
INSERT INTO party (partyId,title) VALUES (105,"Popular Democratic");
INSERT INTO party (partyId,title) VALUES (106,"Independent");
INSERT INTO party (partyId,title) VALUES (107,"Reform");
INSERT INTO party (partyId,title) VALUES (108,"Socialist");

INSERT INTO user (userId,login,pass,partyId) VALUES (100,"George W. Bush","",100);
INSERT INTO user (userId,login,pass,partyId) VALUES (101,"John F. Kerry","",101);
INSERT INTO user (userId,login,pass,partyId) VALUES (102,"Michael Badnarik","",102);
INSERT INTO user (userId,login,pass,partyId) VALUES (103,"Ralph Nader","",103);
INSERT INTO user (userId,login,pass,partyId) VALUES (104,"Michael Peroutka","",104);

INSERT INTO point (pointId,text,submitter) VALUES (1,"Abortion is a Woman's Right",1);
INSERT INTO point (pointId,text,submitter) VALUES (2,"Affirmative Action",1);
INSERT INTO point (pointId,text,submitter) VALUES (3,"Sexual Orientation Protected by Civil Rights Laws",1);
INSERT INTO point (pointId,text,submitter) VALUES (4,"Permit Prayer In Public Schools",1);
INSERT INTO point (pointId,text,submitter) VALUES (5,"Death Penalty",1);
INSERT INTO point (pointId,text,submitter) VALUES (6,"Mandatory \"Three Strikes\" Sentencing Laws",1);
INSERT INTO point (pointId,text,submitter) VALUES (7,"Absolute Right to Gun Ownership",1);
INSERT INTO point (pointId,text,submitter) VALUES (8,"More Federal Funding For Health Coverage",1);
INSERT INTO point (pointId,text,submitter) VALUES (9,"Privatize Social Security",1);
INSERT INTO point (pointId,text,submitter) VALUES (10,"Parents Choose Schools Via Vouchers",1);
INSERT INTO point (pointId,text,submitter) VALUES (11,"Reduce use of coal, oil, & nuclear energy",1);
INSERT INTO point (pointId,text,submitter) VALUES (12,"Drugs Damage Society: Enforce Laws Against Use",1);
INSERT INTO point (pointId,text,submitter) VALUES (13,"Allow Churches to Provide Welfare Services",1);
INSERT INTO point (pointId,text,submitter) VALUES (14,"Decrease overall taxation of the wealthy",1);
INSERT INTO point (pointId,text,submitter) VALUES (15,"Immigration Helps Our Economy - Encourage It",1);
INSERT INTO point (pointId,text,submitter) VALUES (16,"Support and Expand Free Trade",1);
INSERT INTO point (pointId,text,submitter) VALUES (17,"More Spending on Armed Forces",1);
INSERT INTO point (pointId,text,submitter) VALUES (18,"Reduce Spending on Missile Defense",1);
INSERT INTO point (pointId,text,submitter) VALUES (19,"Link Human Rights to Trade With China",1);
INSERT INTO point (pointId,text,submitter) VALUES (20,"Seek UN approval for military action",1);

INSERT INTO debate (debateId,title) VALUES (1,"2004 Presidential");
INSERT INTO debate_user (debateId,userId) VALUES (1,100);
INSERT INTO debate_user (debateId,userId) VALUES (1,101);
INSERT INTO debate_user (debateId,userId) VALUES (1,102);
INSERT INTO debate_user (debateId,userId) VALUES (1,103);
INSERT INTO debate_user (debateId,userId) VALUES (1,104);
INSERT INTO debate_point (debateId,pointId) VALUES (1,1);
INSERT INTO debate_point (debateId,pointId) VALUES (1,2);
INSERT INTO debate_point (debateId,pointId) VALUES (1,3);
INSERT INTO debate_point (debateId,pointId) VALUES (1,4);
INSERT INTO debate_point (debateId,pointId) VALUES (1,5);
INSERT INTO debate_point (debateId,pointId) VALUES (1,6);
INSERT INTO debate_point (debateId,pointId) VALUES (1,7);
INSERT INTO debate_point (debateId,pointId) VALUES (1,8);
INSERT INTO debate_point (debateId,pointId) VALUES (1,9);
INSERT INTO debate_point (debateId,pointId) VALUES (1,10);
INSERT INTO debate_point (debateId,pointId) VALUES (1,11);
INSERT INTO debate_point (debateId,pointId) VALUES (1,12);
INSERT INTO debate_point (debateId,pointId) VALUES (1,13);
INSERT INTO debate_point (debateId,pointId) VALUES (1,14);
INSERT INTO debate_point (debateId,pointId) VALUES (1,15);
INSERT INTO debate_point (debateId,pointId) VALUES (1,16);
INSERT INTO debate_point (debateId,pointId) VALUES (1,17);
INSERT INTO debate_point (debateId,pointId) VALUES (1,18);
INSERT INTO debate_point (debateId,pointId) VALUES (1,19);
INSERT INTO debate_point (debateId,pointId) VALUES (1,20);


