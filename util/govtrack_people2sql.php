<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("database.php");

$db_link = openDatabase();

$userId = 0;
$osid = 0;
$partyId = 0;

function getParty($partyName, &$party)
{
	$partyId = 0;

	switch(strtolower($partyName))
	{
		case "republican":
		case "gop":
			$partyId = 100;
			$party = "Republican";
			break;
		case "democrat":
		case "democratic":
		case "dnc":
			$partyId = 101;
			$party = "Democratic";
			break;
		case "libertarian":
			$partyId = 102;
			$party = "Libertarian";
			break;
		case "green":
			$partyId = 103;
			$party = "Green";
			break;
		case "constitution":
			$partyId = 104;
			$party = "Constitution";
			break;
		case "popular democrat":
			$partyId = 105;
			$party = "Popular Democratic";
			break;
		case "independent";
			$partyId = 106;
			$party = "Independent";
			break;
		case "reform";
			$partyId = 107;
			$party = "Reform";
			break;
		case "independent";
			$partyId = 108;
			$party = "Socialist";
			break;
		default:
			$partyId=0;
			$party = $attrs['party'];
			break;
	}
	return $partyId;
}

function getSqlDate($str)
{
	if($str == "" || $str == 0)
		return "";

	$n = sscanf($str, "%d-%d-%d", $y, $m, $d);
	if($n && $y)
		return date("Y-m-d H:i:s", mktime(0, 0, 0, $m, $d, $y, FALSE));
	return "";
}

function startPerson($parser, $name, $attrs)
{
	global $userId;
	global $osid;
	global $partyId;

	// <person id='400001' lastname='Abercrombie' firstname='Neil' middlename='' namemod='' nickname='' birthday='1938-06-26' gender='M' religion='' url='http://www.house.gov/abercrombie' party='Democrat' osid='N00007665' title='Rep.' state='HI' district='1' >

	$userId = addslashes($attrs['ID']);
	$login = "";
	$pass = "";
	$modgroup = ""; //$attrs[];
	$gender = addslashes($attrs['GENDER']);
	$religion = addslashes($attrs['RELIGION']);
	$url = addslashes($attrs['URL']);
	$city = ""; //$attrs[];
	$state = addslashes($attrs['STATE']);
	$zip = ""; //$attrs[];
	$fname = addslashes($attrs['FIRSTNAME']);
	$mname = addslashes($attrs['MIDDLENAME']);
	$namemod = addslashes($attrs['NAMEMOD']);
	$lname = addslashes($attrs['LASTNAME']);
	$email = ""; //$attrs[];
	$bday = getSqlDate($attrs['BIRTHDAY']);
	$partyId = getParty($attrs['PARTY'], $party);
	$osid = addslashes($attrs['OSID']);
	$district = addslashes($attrs['DISTRICT']);
	$created = date("Y-m-d H:i:s", time());

	echo "insert into user set userId='$userId', login='$login', pass='', modgroup='$modgroup', gender='$gender', religion='$religion', url='$url', city='$city', state='$state', zip='$zip', fname='$fname', mname='$mname', lname='$lname', email='$email', bday='$bday', partyId='$partyId', party='$party', district='$district', created='$created';\n";
}

function endPerson($parser, $name)
{
}

function startRole($parser, $name, $attrs)
{
	global $userId;
	global $osid;
	global $partyId;

	if($userId == 0) {
		echo "# WARNING: unknown user role: $userId, $attrs[url]\n";
		return;
	}
	// <role type='rep' startdate='2003-01-01' enddate='2004-12-31' party='Democrat' state='HI' district='1' class='201128' url='http://www.house.gov/abercrombie' address='' phone='' email='' />

	$type = addslashes($attrs['TYPE']);
	$start = getSqlDate($attrs['STARTDATE']);
	$end = getSqlDate($attrs['ENDDATE']);
	$pottyId = getParty($attrs['PARTY'], $party);
	if($partyId != $pottyId)
		echo "# wow: $userId\n";

	$state = addslashes($attrs['STATE']);
	$district = addslashes($attrs['DISTRICT']);
	$class = addslashes($attrs['CLASS']);
	$url = addslashes($attrs['URL']);
	$address = addslashes($attrs['ADDRESS']);
	$phone = addslashes($attrs['PHONE']);
	$email = addslashes($attrs['EMAIL']);

	echo "insert into rep_term set userId='$userId', type='$type', start='$start', end='$end', partyId='$partyId', osid='$osid', state='$state', district='$district', class='$class', url='$url', address='$address', phone='$phone', email='$email';\n";
}

function endRole($parser, $name)
{
}

function startElement($parser, $name, $attrs)
{
	switch($name)
	{
		case "PERSON":
			startPerson($parser, $name, $attrs);
			break;
		case "ROLE":
			startRole($parser, $name, $attrs);
			break;
		default:
			echo "# unknown element name $name\n";
	}
}

function endElement($parser, $name)
{
	switch($name)
	{
		case "PERSON":
			endPerson($parser, $name);
			break;
		case "ROLE":
			endRole($parser, $name);
			break;
		default:
			echo "# unknown element name $name\n";
	}
}

echo "use debatepoint;\n";

$xml_parser = xml_parser_create();
xml_set_element_handler($xml_parser, "startElement", "endElement");

while($line = fread(STDIN, 4096))
{
	if(!xml_parse($xml_parser, $line, feof(STDIN))) {
		die(sprintf("XML error: %s at line %d",
                   xml_error_string(xml_get_error_code($xml_parser)),
                   xml_get_current_line_number($xml_parser)));
	}
}

closeDatabase($db_link);

?>
