<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("global.php");
require("database.php");

function do_err($str)
{
	echo "ERR: $str\n";
}

$db_link = openDatabase();
if($db_link == FALSE) {
	echo "failed to open database\n";
	exit;
}

$query = "select * from argument where repliesTotal=0";
$query_result = mysql_query($query);
if($query_result == FALSE) {
	echo "failed to query: $query\n";
	exit;
}

while(($argumentRow = queryNextRow($query_result)) != FALSE)
{
	refreshModeration($argumentRow);
	mysql_query($query);
}
queryFree($query_result);

//
//

$query = "select * from stance";
$query_result = mysql_query($query);
if($query_result == FALSE) {
	echo "failed to query: $query\n";
	exit;
}

while(($stanceRow = queryNextRow($query_result)) != FALSE)
{
	// update images, but not dates
	refreshStanceModeration($stanceRow['stanceId'], FALSE);
}
queryFree($query_result);


closeDatabase($db_link);

?>
