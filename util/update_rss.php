<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("global.php");
require("database.php");

$db_link = openDatabase();

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
echo "<!DOCTYPE rss PUBLIC \"-//Netscape Communications//DTD RSS 0.91//EN\" \"http://my.netscape.com/publish/formats/rss-0.91.dtd\">\n";
echo "<rss version=\"0.91\">\n";
echo "	<channel>\n";
echo "		<title>Most Active Debatepoints</title>\n";
echo "		<link>http://debatepoint.com/</link>\n";
echo "		<description></description>\n";
echo "		<language>en</language>\n";
echo "		<webMaster>james.d.taylor@gmail.com</webMaster>\n";
echo "		<managingEditor>james.d.taylor@gmail.com</managingEditor>\n";
echo "		<pubDate>Wed, 03 Aug 2005 03:17:00 GMT</pubDate>\n";
echo "		<lastBuildDate>Wed, 03 Aug 2005 03:17:00 GMT</lastBuildDate>\n";

$handle = queryActiveConsensus(20, 0, 0);
if($handle != FALSE)
{
	$n = queryNumRow($handle);
	if($n > 0)
	{

while(($pointStanceRow = getNextConsensus($handle)) != FALSE)
{
	$stanceId = $pointStanceRow['stanceId'];

	//$stanceColor = stanceToColor($pointStanceRow);
	echo "\t\t<item>\n";
	printf("\t\t\t<title>%.0f%%", round($pointStanceRow['effective'] * 100));
	echo " - $pointStanceRow[text]</title>\n";
	echo "\t\t\t<link>http://debatepoint.com/debate.php?sid=$stanceId</link>\n";
	echo "\t\t\t<pubDate>Wed, 03 Aug 2005 03:17:00 GMT</pubDate>\n";
	echo "\t\t\t<description>\n";
	echo "\t\t\t</description>\n";
	echo "\t\t</item>\n";
}
	}
	else echo "<!-- no active debatepoints  -->\n";

	queryFree($handle);
}
else echo "<!-- failed to query active debatepoints  -->\n";

echo "	</channel>\n";
echo "</rss>\n";


closeDatabase($db_link);

?>
