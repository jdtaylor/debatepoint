<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("database.php");

$db_link = openDatabase();

function getSqlDate($str)
{
	if($str == "" || $str == 0)
		return "";

//	$n = sscanf($str, "%d-%d-%d", $y, $m, $d);
//	if($n && $y)
		return date("Y-m-d H:i:s", $str); //mktime(0, 0, 0, $m, $d, $y, FALSE));
//	return "";
}

$curDate = time();

$query = "select sessionId from sessions order by date asc";
$query_result = mysql_query($query);
if($query_result == FALSE)
	exit;

while(($sessionRow = mysql_fetch_assoc($query_result)) != FALSE)
{
	$query = "delete from sessions where sessionId='$sessionRow[sessionId]'";
	mysql_query($query);
}
mysql_free_result($query_result);

closeDatabase($db_link);

?>
