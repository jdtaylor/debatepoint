<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("database.php");

$db_link = openDatabase();

function getSqlDate($str)
{
	if($str == "" || $str == 0)
		return "";

//	$n = sscanf($str, "%d-%d-%d", $y, $m, $d);
//	if($n && $y)
		return date("Y-m-d H:i:s", $str); //mktime(0, 0, 0, $m, $d, $y, FALSE));
//	return "";
}

function getCongressType($type)
{
	switch(strtoupper($type))
	{
		case "H":
		case "HOUSE":
		case "HOUSEREP":
			return "h"; // house bill
		case "HR":
		case "HRES":
		case "HOUSERES":
			return "hr"; // house resolution
		case "HJ":
		case "HJR":
		case "HJRES":
		case "HOUSEJR":
		case "HOUSEJRES":
		case "HOUSEJOINTRES":
			return "hj"; // house joint resolution
		case "HC":
		case "HCR":
		case "HCONRES":
		case "HOUSECONRES":
			return "hc"; // house concurrent resolution
		case "S":
		case "SEN":
		case "SENATE":
			return "s"; // senate bill
		case "SR":
		case "SRES":
		case "SENATERES":
			return "sr"; // senate resolution
		case "SJ":
		case "SJR":
		case "SJRES":
		case "SENATEJR":
		case "SENATEJOINTRES":
			return "sj"; // senate joint resolution
		case "SC":
		case "SCR":
		case "SCONRES":
		case "SENATECONRES":
			return "sc"; // senate concurrent resolution
	}
	return "";
}


function startBill($parser, $name, $attrs)
{
	global $session;
	global $type;
	global $number;
	global $introduced;
	global $sponsor;
	global $title_short;
	global $title;

	//<bill session='108' type='h' number='10' introduced='1095998400' sponsor='400169' lastaction='1097248080' title="H.R. 10: 9/11 Recommendations Implementation Act" officialtitle="To provide for reform of the intelligence community, terrorism prevention and prosecution, border security, and international cooperation and coordination, and for other purposes." last_vote_date="" last_vote_where="" last_vote_roll="" last_speech="1096930801" ><calendar date="1097019960"/></bill>

	//<bill session='108' type='hr' number='10' introduced='1041915600' sponsor='400327' lastaction='1042075020' title="H.Res. 10: Congratulating the Ohio State University football team for winning the 2002 NCAA Division I-A collegiate football national championship." officialtitle="Congratulating the Ohio State University football team for winning the 2002 NCAA Division I-A collegiate football national championship." last_vote_date="1042075020" last_vote_where="h" last_vote_roll="12" ><vote date="1042075020" where="h" result="pass" how="roll" roll="12"/></bill>

	$session = addslashes($attrs['SESSION']);
	$type = getCongressType($attrs['TYPE']);
	$number = addslashes($attrs['NUMBER']);
	$introduced = getSqlDate($attrs['INTRODUCED']);
	$sponsor = addslashes($attrs['SPONSOR']);
	$title_short = addslashes($attrs['TITLE']);
	$title = addslashes($attrs['OFFICIALTITLE']);
	$signed = "";
}

function endBill($parser, $name)
{
	global $session;
	global $type;
	global $number;
	global $introduced;
	global $sponsor;
	global $title_short;
	global $title;
	global $signed;

	echo "insert into bill set session='$session', type='$type', number='$number', introduced='$introduced', sponsor='$sponsor', title_short='$title_short', title='$title', signed='$signed';\n";
}

function startSigned($parser, $name, $attrs)
{
	global $signed;

	$signed = getSqlDate($attrs['DATE']);
}

function endSigned($parser, $name)
{
}

function startElement($parser, $name, $attrs)
{
	switch($name)
	{
		case "BILL":
			startBill($parser, $name, $attrs);
			break;
		case "SIGNED":
			startSigned($parser, $name, $attrs);
			break;
		default:
			echo "# unknown element name $name\n";
	}
}

function endElement($parser, $name)
{
	switch($name)
	{
		case "BILL":
			endBill($parser, $name);
			break;
		case "SIGNED":
			endSigned($parser, $name);
			break;
		default:
			echo "# unknown element name $name\n";
	}
}

echo "use debatepoint;\n";

$xml_parser = xml_parser_create();
xml_set_element_handler($xml_parser, "startElement", "endElement");

while($line = fread(STDIN, 4096))
{
	if(!xml_parse($xml_parser, $line, feof(STDIN))) {
		die(sprintf("XML error: %s at line %d",
                   xml_error_string(xml_get_error_code($xml_parser)),
                   xml_get_current_line_number($xml_parser)));
	}
}

closeDatabase($db_link);

?>
