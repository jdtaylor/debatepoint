<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("database.php");

$db_link = openDatabase();


function getSqlDate($str)
{
//	$n = sscanf($str, "%d-%d-%d", $y, $m, $d);
//	if($n && $y)
//	return "";
}

function getCongressType($type)
{
	if(strchr($type, 'h'))
		return 'h';
	if(strchr($type, 's'))
		return 's';
	return '';
}

function startRoll($parser, $name, $attrs)
{
	global $billId;
	global $when;
	global $fileName;

	//<roll where="house" session="108" year="2004" roll="10" when="1075325520" aye="265" nay="99" nv="67" present="1">

	$type = getCongressType($attrs['WHERE']);
	$session = addslashes($attrs['SESSION']);
	$number = addslashes($attrs['ROLL']);
	$when = date("Y-m-d H:i:s", $attrs['WHEN']);
	$year = date("Y", $attrs['WHEN']);
	$month = date("m", $attrs['WHEN']);

	if($type == '')
		return;
	if($type == 'h')
		$query = "select billId,introduced from bill where (type='h' || type='hr' || type='hc' || type='hj') && session='$session' && number='$number'";
	else if($type == 's')
		$query = "select billId,introduced from bill where (type='s' || type='sr' || type='sc' || type='sj') && session='$session' && number='$number'";
	else
		echo "# shouldn't happen, eh\n";

	$query_handle = mysql_query($query);
	if($query_handle == FALSE || mysql_num_rows($query_handle) == 0) {
		echo "# unknown bill: type=$type, session=$session, number=$number\n";
		echo "# hah! $fileName\n";
		exit(42);
	}
	//
	// select the closest bill by date
	$best = 9999999;
	$billRow = queryNextRow($query_handle);
	$firstRow = $billRow;

	while($billRow != FALSE)
	{
		if($billRow['introduced'] != NULL)
		{
			sscanf($billRow['introduced'], "%d-%d-%d", $y, $m, $d);

			$dif = abs($y - $year) * 365;
			$dif += abs($m - $month) * 31;
			if($dif < $best)
			{
				$best = $dif;
				$billId = $billRow['billId'];
			}
		}
		$billRow = queryNextRow($query_handle);
	}
	if($best == 9999999) {
		echo "# backup bill: type=$type, session=$session, number=$number\n";
		$billId = $firstRow['billId'];
	}
	mysql_free_result($query_handle);
}

function endRoll($parser, $name)
{

}

function startVoter($parser, $name, $attrs)
{
	global $billId;
	global $when;

	if($billId == "" || $billId == 0)
		return;

	$userId = addslashes($attrs['ID']);
	if($userId == "0") {
		//echo "# bad user: $userId\n";
		return;
	}
	$vote = addslashes($attrs['VOTE']);

	echo "insert into bill_vote set billId='$billId', userId='$userId', vote='$vote';\n";
}

function endVoter($parser, $name)
{
}

function startElement($parser, $name, $attrs)
{
	switch($name)
	{
		case "ROLL":
			startRoll($parser, $name, $attrs);
			break;
		case "VOTER":
			startVoter($parser, $name, $attrs);
			break;
		default:
			echo "# unknown element name $name\n";
	}
}

function endElement($parser, $name)
{
	switch($name)
	{
		case "ROLL":
			endRoll($parser, $name);
			break;
		case "VOTER":
			endVoter($parser, $name);
			break;
		default:
			echo "# unknown element name $name\n";
	}
}

$xml_parser = xml_parser_create();
xml_set_element_handler($xml_parser, "startElement", "endElement");

$fp = fopen($argv[1], "r");
if($fp == NULL)
	return 0;
// h2004-231.xml
//sscanf($argv[1], "%s-%d.xml", $typeyear, $fileNumber);
$fileName = $argv[1];


while($line = fread($fp, 4096))
{
	if(!xml_parse($xml_parser, $line, feof($fp))) {
		die(sprintf("XML error: %s at line %d",
                   xml_error_string(xml_get_error_code($xml_parser)),
                   xml_get_current_line_number($xml_parser)));
	}
}

fclose($fp);

closeDatabase($db_link);

?>
