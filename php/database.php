<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

function openDatabase()
{
	$db_host = "localhost";
	$db_user = "debatepoint";
	$db_pass = "123pass";

	$link = mysql_connect($db_host, $db_user, $db_pass);
	if($link == FALSE)
		return FALSE;
	if(mysql_select_db("debatepoint") == FALSE)
		return FALSE;

	return $link; // TODO: can mysql_connect return 0?
}

function closeDatabase($link)
{
	mysql_close($link);
}

function db_escape($str)
{
	//return addslashes($str);
	return mysql_real_escape_string($str);
}

function db_restore($str)
{
	return stripslashes($str);
}

function querySeekRow($query_handle, $row)
{
	return mysql_data_seek($query_handle, $row);
}

function queryNextRow($query_handle)
{
	$row = mysql_fetch_assoc($query_handle);
	return $row;
}

function queryFree($query_handle)
{
	mysql_free_result($query_handle);
}

function queryNumRow($query_handle)
{
	return mysql_num_rows($query_handle);
}

function queryResult($query)
{
	$query_handle = mysql_query($query);
	$row = gueryNextRow($query_handle);
	$query_free_result($query_handle);
	if($row)
		return TRUE;
	return FALSE;
}

//  hits_user int(32) default 0,
//  hits_anon int(32) default 0,

function countUserHit()
{
	$query = "update sys set hits_user = hits_user + 1 where sysId=0";
	mysql_query($query);
}

function countAnonHit()
{
	$query = "update sys set hits_anon = hits_anon + 1 where sysId=0";
	mysql_query($query);
}

function newUser($login, $pass, $email)
{
	global $num_modgroup;

	$query = "select * from user where login='$login'";
	$handle = mysql_query($query)
		or do_err("failed: $query");

	if(queryNumRow($handle) != 0)
		do_err("Failed: login name already used. TODO: modify user with auth");
	queryFree($handle);

	// TODO: modgroup should be '0' by default
	//       after some time and good behaviour should users be
	//       allowed to moderate
	$modgroup = mt_rand(1, $num_modgroup);
	$sqldate = date("Y-m-d H:i:s", time());


	putenv("GNUPGHOME=/home/www-data/.gnupg");
	//create a unique file name
	$infile = tempnam("/tmp", "PGP.asc");
	$outfile = $infile.".asc";

	//write form variables to email
	$fp = fopen($infile, "w");
	fwrite($fp, $login . ":" . $pass . ":" . $email);
	fclose($fp);
	//set up the gnupg command
	$command = "/usr/bin/gpg -a --always-trust --batch --no-secmem-warning -e -u 'debatepoint_srv <debatepoint_srv@debatepoint.com>' -r 'debatepoint <debatepoint@gmail.com>' --encrypt -o $outfile $infile";

	//execute the gnupg command
	system($command, $result);

	//delete the unencrypted temp file
	unlink($infile);

	if($result == 0)
	{
		$fp = fopen($outfile, "r");

		if(!$fp || filesize($outfile) == 0)
			$result = -1;
	}

	if($result == 0)
	{
		//read the encrypted file
		$contents = fread($fp, filesize($outfile));
		//delete the encrypted file
		unlink($outfile);

		//send the email
		//mail ($testemail, $emailsubject, $contents, $emailfrom);
	} 
	else
	{
		echo "encryption failed! you won't be able to retrieve your clear-text password!<BR>\n";
		$contents = "";
	}

	$login_esc = db_escape($login);
	$pass_esc = db_escape($pass);
	//$contents_esc = db_escape($contents);
	$query = "insert into user (login,pass,pass_enc,modgroup,created) values ('$login_esc', PASSWORD('$pass_esc'),'$contents','$modgroup','$sqldate')";
	if(mysql_query($query) == FALSE)
		do_err("Failed to create new user: '$login'");
}

function updateUserScore($userId, $score)
{
	$query = "update user set score = score + '$score' where userId='$userId'";
	$handle = mysql_query($query);
	if($handle == FALSE)
		do_err("failed to update user score");
}

function updateUserPrefs($userId, $sort_method, $sort_reverse, $sort_filter)
{
	if($sort_reverse != 0 && $sort_reverse != 1)
		do_err("sort_reverse not a bool parameter");
	if($sort_method <= 0 || $sort_method >= 5)
		do_err("sort_method out of range");
	if($sort_filter < -1.0 || $sort_filter > 1.0)
		do_err("sort_filter out of range");
	
	//echo "updating user: sort_filter:$sort_filter\n<BR>";

	$query = "update user set sort_method='$sort_method',sort_reverse='$sort_reverse',sort_filter='$sort_filter' where userId='$userId'";
	if(mysql_query($query) == FALSE)
		return FALSE; // error not too important
	return TRUE;
}

define("TAG_TYPE_ARG",		0);
define("TAG_TYPE_POINT", 	1);

function newTags($tags, $type, $id)
{
	//
	// we assume that the tags are well formed and of proper characters
	//
	$tag_array = explode(" ", $tags);
	$n = count($tag_array);
	for($i = 0; $i < $n; $i++)
	{
		$tag = db_escape(strtoupper(trim($tag_array[$i])));
		$query = "insert into tag_map set tag='$tag',type='$type',id='$id'";
		if(mysql_query($query) == FALSE)
			return FALSE;
	}
	return TRUE;
}

//
// add up the effective scores of the stance's child arguments, and
// update the stance's oppose,support,certainty fields.
//
function refreshStanceModeration($stanceId, $bDate=TRUE)
{
	$stanceRow = getStance($stanceId);
	if($stanceRow == FALSE)
		do_err("failed to get stance for moderation refresh: $stanceId");

	$query_handle = queryChildArguments($stanceId, 0);
	if($query_handle == FALSE)
		do_err("failed to query stance arguments on moderation refresh");
	$nChild = queryNumRow($query_handle);

	$repliesSupport = 0;
	$repliesOppose = 0;
	$repliesTotal = 0;
	$comments = 0;
	$support = 0.0;
	$oppose = 0.0;
	$total = 0.0;
	$effective = 0.0;

	while(($argRow = queryNextRow($query_handle)) != FALSE)
	{
		if($argRow['stance'] < 0 || $argRow['stance'] > 2)
			do_err("arg:$argRow[argumentId] has a bad stance: $argRow[stance]");

		switch($argRow['stance'])
		{
			case 0:
				$repliesOppose++;
				break;
			case 1:
				$repliesSupport++;
				break;
			case 2:
				$comments++;
				continue; // no moderation
		}
		$repliesTotal += 1 + $argRow['repliesTotal'];

		//
		// if there is insufficient child arguments to determine the
		// proper relative effective score, then use the single stance
		// moderations instead
		// TODO: $nChild here is the number of children including
		// comments.. we want the number of real arguments.
		//
		if($nChild <= $mod_minarg)
			$childEffective = $argRow['stanceEffective'];
		else
			$childEffective = $argRow['effective'];

		if($childEffective <= 0.0)
			continue; // don't score negative replies

		//
		// cumulate the child scores
		//
		switch($argRow['stance'])
		{
			case 0:
				$oppose += $childEffective;
				$total += $childEffective;
				break;
			case 1:
				$support += $childEffective;
				$total += $childEffective;
				break;
		}
	}
	mysql_free_result($query_handle);

	$certainty = $total;
	if($total != 0.0) {
		// stance based only on bias of child arguments
		$effective = $support / $total;
		$support /= $total; // normalize between 0 and 1
		$oppose /= $total;
	}


	$query = "update stance set effective='$effective',support='$support',oppose='$oppose',certainty='$certainty',repliesSupport='$repliesSupport',repliesOppose='$repliesOppose',repliesTotal='$repliesTotal',comments='$comments'";
		
	if($bDate) {
		$sqlDate = date("Y-m-d H:i:s", time());
		$query .= ",modified='$sqlDate'";
	}
	$query .= " where stanceId='$stanceId'";

	//echo "$query<BR>\n";
	if(mysql_query($query) == FALSE)
		do_err("failed to update stance of stance row $stanceId");
	
	$stanceRow['effective'] = $effective;
	$stanceRow['support'] = $support;
	$stanceRow['oppose'] = $oppose;
	$stanceRow['certainty'] = $certainty;
}

//
// TODO: need some atomic locking here!
//
function refreshModeration($parentRow)
{
	global $mod_minarg;

	if($parentRow == FALSE)
		return;
	$argumentId = $parentRow['argumentId']; // for dubuggering

	$query_handle = queryChildArguments($parentRow['stanceId'], $argumentId);
	if($query_handle == FALSE)
		do_err("failed to query child arguments on moderation refresh");

	$repliesSupport = 0; //queryNumRow($query_handle);
	$repliesOppose = 0;
	$repliesTotal = 0;
	$comments = 0;
	$support = 0.0;
	$oppose = 0.0;
	$total = 0.0;

	while(($argRow = queryNextRow($query_handle)) != FALSE)
	{

		if($argRow['stance'] < 0 || $argRow['stance'] > 2)
			do_err("arg:$argRow[argumentId] has a bad stance: $argRow[stance]");

		switch($argRow['stance'])
		{
			case 0:
				$repliesOppose++;
				break;
			case 1:
				$repliesSupport++;
				break;
			case 2:
				$comments++;
				continue; // no moderation
		}
		$repliesTotal += 1 + $argRow['repliesTotal'];

		//
		// if there is insufficient child arguments to determine the
		// proper relative effective score, then use the single stance
		// moderations instead
		//
		if($parentRow['repliesSupport'] + $parentRow['repliesOppose'] <= $mod_minarg)
			$childEffective = $argRow['stanceEffective'];
		else
			$childEffective = $argRow['effective'];

		if($childEffective <= 0.0)
			continue; // don't score negative replies

		//
		// cumulate the child scores
		//
		switch($argRow['stance'])
		{
			case 0:
				$oppose += $childEffective;
				$total += $childEffective;
				break;
			case 1:
				$support += $childEffective;
				$total += $childEffective;
				break;
		}
	}
	mysql_free_result($query_handle);

	if($total != 0.0) {
		$support /= $total; // normalize between 0 and 1
		$oppose /= $total;
	}

	// argument's score based only on moderation
	// percentage of positive moderation:
	if($parentRow['scorePos'] + $parentRow['scoreNeg'] != 0)
		$effective = $parentRow['scorePos'] /
					 ($parentRow['scorePos'] + $parentRow['scoreNeg']);
	else
		$effective = 0.0;

	if($parentRow['stancePos'] + $parentRow['stanceNeg'] != 0)
		$stanceEffective = ($parentRow['stancePos'] - $parentRow['stanceNeg']) /
							($parentRow['stancePos'] + $parentRow['stanceNeg']);
	else
		$stanceEffective = 0;
	//
	// penalize argument based on negative child arguments
	// (effective is reduced by the percentage of opposing replies)
	//
	if($support < $oppose && $parentRow['stance'] != 2)
	{
		// this is a support or oppose argument, not a comment.  The
		// effective score is penalized based on the outcome of the
		// child arguments.

		if($parentRow['stanceLinkId'])
		{
			// TODO, I don't know what to do here :(
		}

		//do_log(0, "", "$argumentId - pre-penalty effective: $effective");
		//do_log(0, "", "$argumentId - total:$total,support:$support,oppose:$oppose");

		// TODO: consider ramping this based on # of replies
		$effective *= $support;

		// in-stance scores might be negative; don't increase negatives!
		if($stanceEffective > 0.0)
			$stanceEffective *= $support;

		//do_log(0, "", "$argumentId - post-penalty effective: $effective");
	}

	$query = "update argument set effective='$effective',stanceEffective='$stanceEffective',support='$support',oppose='$oppose',repliesSupport='$repliesSupport',repliesOppose='$repliesOppose',repliesTotal='$repliesTotal',comments='$comments' where argumentId='$argumentId'";
	//echo "$query<BR>";

	if(mysql_query($query) == FALSE)
		do_err("failed to update stance of argument $argumentId");

	// TODO: for comments, we only want to refresh the immediate parent
	//if($parentRow['stance'] != 2) // comments don't affect their parents
	if($parentRow['parentId'] != 0) {
		$parentRow = getArgument($parentRow['parentId']);
		if($parentRow == FALSE)
			do_err("failed to get parent argument: $parentRow[parentId]");
		refreshModeration($parentRow);
	}
}

function newModerationSingle($userId, $argumentId, $score)
{
	global $anonId;
	global $consensusId;
	global $mod_minarg;

	// don't let anonymous or consensus users moderate
	// I don't think this should ever happen.. ?
	if($userId == $anonId || $userId == $consensusId)
		return FALSE;

	$argRow = getArgument($argumentId);
	if($argRow == FALSE)
		do_err("failed to get argument necessary for moderation");
	
	//if($argRow['replies'] >= $mod_minarg)
	//	do_err("moderation failed; there are more than $mod_minarg replies");

	$stancePos = $argRow['stancePos'];
	$stanceNeg = $argRow['stanceNeg'];

	if($score < 0) {
		$stanceNeg += abs($score);
		$result = 1; // argument1 did not win
	} else {
		$stancePos += abs($score);
		$result = 0; // argument1 did win
	}
	// add score to users total karma
	updateUserScore($argRow['userId'], $score);

	// check if the user has already moderated this argument
	if(getModeration($argumentId, 0, $userId) != FALSE)
		return FALSE; // already moded

	$sqlDate = date("Y-m-d H:i:s", time());
	// 
	$query = "insert into moderation (argId1,argId2,userId,score,result,date) values ('$argumentId','0','$userId','$score','$result','$sqlDate')";
	//echo "$query<BR>";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;

	$query = "update argument set stancePos='$stancePos',stanceNeg='$stanceNeg' where argumentId='$argumentId'";
	//echo "$query<BR>";
	if(mysql_query($query) == FALSE)
		do_err("failed to update stance of argument $parentId");

	$argRow['stancePos'] = $stancePos;
	$argRow['stanceNeg'] = $stanceNeg;

	//
	// single moderation is only used to determine which arguments are
	// listed in which order when displayed.  "cross-stance"
	// moderation is done in newModeration below, and is what defines
	// a stance on a point.  We have to refresh the moderation anyway
	// because it calculates in-stance score: stanceEffective.
	//
	refreshModeration($argRow);
	//
	// in some cases, the stance is modified when # arguments is small
	if($argRow['repliesSupport'] + $argRow['repliesOppose'] <= $mod_minarg)
		refreshStanceModeration($argRow['stanceId']);

	return TRUE;
}

function newModeration($userId, $argId1, $argId2, $result)
{
	global $anonId;
	global $consensusId;
	$bArg1 = FALSE; // update argument 1
	$bArg2 = FALSE; // update argument 2

	// don't let consensus users moderate
	// I don't think this should ever happen.. ?
	// - note that this is called by newArgument even when the user is
	// anonymous
	if($userId == $consensusId)
		return FALSE;

	$argRow1 = getArgument($argId1);
	$argRow2 = getArgument($argId2);
	if($argRow1 == FALSE || $argRow2 == FALSE)
		do_err("failed to get argument necessary for moderation");

	//
	// keep anonymous from moderating.  we still need to call
	// refreshModeration because that is where the 'replies' are
	// counted.
	//
	if($userId != $anonId)
	{
		$userScore1 = 0;
		$userScore2 = 0;
		$score = 1; // consider varying this depending on user's karma

		// comments don't affect the stance (stance==2 means it's a comment)
		if($argRow1['stance'] == 2 || $argRow2['stance'] == 2)
			do_err("cannot moderate a comment");

		// check if the user has already moderated this argument
		// if so, then modify the existing one
		$modRow = getModeration($argId1, $argId2, $userId);
		if($modRow != FALSE)
		{
			//
			// we have to reverse the scoring since we have deleted
			// the moderation.  I've done it this way to minimize the
			// sql updates.
			switch($modRow['result'])
			{
				case 0:
					$argRow1['scorePos'] -= $modRow['score'];
					$argRow2['scoreNeg'] -= $modRow['score'];
					$userScore1 -= $modRow['score'];
					$userScore2 += $modRow['score'];
					$bArg1 = $bArg2 = TRUE; // update both arguments
					break;
				case 1:
					$argRow1['scoreNeg'] -= $modRow['score'];
					$argRow2['scorePos'] -= $modRow['score'];
					$bArg1 = $bArg2 = TRUE; // update both arguments
					$userScore1 += $modRow['score'];
					$userScore2 -= $modRow['score'];
					break;
			}
		}

		switch($result)
		{
			case 0:
				// arg1 won
				$argRow1['scorePos'] += $score;
				$argRow2['scoreNeg'] += $score;
				$bArg1 = $bArg2 = TRUE; // update both arguments
				$userScore1 += $score;
				$userScore2 -= $score;
				break;
			case 1:
				// arg2 won
				$argRow1['scoreNeg'] += $score;
				$argRow2['scorePos'] += $score;
				$bArg1 = $bArg2 = TRUE; // update both arguments
				$userScore1 -= $score;
				$userScore2 += $score;
				break;
		}
		// add score to users total karma
		if($userScore1 != 0)
			updateUserScore($argRow1['userId'], $userScore1);
		if($userScore2 != 0)
			updateUserScore($argRow2['userId'], $userScore2);

		$sqlDate = date("Y-m-d H:i:s", time());

		if($modRow != FALSE)
		{
			// update the current moderation row
			$query = "update moderation set score='$score',result='$result',date='$sqlDate' where modId='$modRow[modId]'";
		}
		else
		{
			// insert a new moderation row
			$query = "insert into moderation (argId1,argId2,userId,score,result,date) values ('$argId1','$argId2','$userId','$score','$result','$sqlDate')";
		}
		//echo "$query<BR>";
		if(mysql_query($query) == FALSE)
			return FALSE;

		if($bArg1)
		{
			$query = "update argument set scorePos='$argRow1[scorePos]',scoreNeg='$argRow1[scoreNeg]' where argumentId='$argId1'";
			//echo "$query<BR>";
			if(mysql_query($query) == FALSE)
				do_err("failed to update stance of argument $argId1");
		}

		if($bArg2)
		{
			$query = "update argument set scorePos='$argRow2[scorePos]',scoreNeg='$argRow2[scoreNeg]' where argumentId='$argId2'";
			//echo "$query<BR>";
			if(mysql_query($query) == FALSE)
				do_err("failed to update stance of argument $argId2");
		}
		do_log(1, "new mod", "arg1:$argId1 arg2:$argId2 result:$result");
	}

	// recursively refresh effective stance counts up the argument tree
	refreshModeration($argRow1);
	refreshModeration($argRow2);

	// top level stance recount. (both args should have same stance!)
	refreshStanceModeration($argRow1['stanceId']);
	return TRUE;
}

//
// previous moderation argId's are passed in via argId1 and argId2.
// This function returns the next arguments in the same fields
//
function nextModeration($userId, &$argId1, &$argId2, $result, $modCount)
{
	global $sessStanceRow;
	global $mod_minarg;

	$stance = $sessStanceRow['stanceId'];
	$repliesTotal = $sessStanceRow['repliesTotal'];
	if($repliesTotal < 2)
		return FALSE;
	$stanceReplies = $sessStanceRow['repliesSupport'] +
					$sessStanceRow['repliesOppose'];

	//
	// this method took me a few hours to figure out.  The object is
	// to get two argumentId's with the same parentId, and in a
	// somewhat random fashion.  I gave up on avoiding repeating pairs
	// in favor of the user re-moderating repeated pairs instead.
	//
	// first get a list of all the arguments that are parents (have
	// replies), and sort them by decreasing number of replies.  get a
	// random number betwenn 0 and the number of total replies for
	// this stance, and determine which argument down the list this
	// number applies to.  Once the arguments get less than 2, repick
	// a random number and repeast process.
	// Use the resulting argument as the parent.
	//
	// next, get a list of all the arguments that are children to the
	// parent, and pick two at random with a bias towards the recent.
	//
	//
	// ISSUE: bias the parent based on number of total replies, or
	// just on the immediate replies.  If total, then the root
	// arguments will get more moderation hits.
	//
	//srand();

	$retry = 0;

	while(42)
	{
		if($retry++ > 3)
		{
			do_warn("excessive retry attempts in selecting nextModeration");
			if($retry > 10)
				return FALSE;
		}
		$random = rand(0, $repliesTotal-1);
		$parentId = 0;

		//echo "try:$retry random:$random stanceReplies:$stanceReplies max:$repliesTotal<BR>\n";

		if($random >= $stanceReplies)
		{
			// 
			// parent isn't the stance, so we need to hop through all the
			// arguments with children
			//
			$query = "select argumentId,repliesSupport,repliesOppose,stanceEffective from argument where stanceId='$stance' && (stance=0 || stance=1) order by repliesTotal desc"; 
			//echo "$query\n";
			$result = mysql_query($query);
			if($result == FALSE)
				do_err("failed: $query");
			$firstRow = queryNextRow($result);


			$argRow = $firstRow;
			$argReplies = $argRow['repliesSupport'] +
				$argRow['repliesOppose'];
			$iReply = $argReplies + $stanceReplies;
			while($argRow != FALSE && $iReply < $random)
			{
				if($argReplies < 2)
					break;
				//echo "iReply:$iReply id:$argRow[argumentId] sup:$argRow[repliesSupport] op:$argRow[repliesOppose]<BR>\n";
				$argRow = queryNextRow($result);
				$argReplies = $argRow['repliesSupport'] +
					$argRow['repliesOppose'];
				$iReply += $argReplies;
			}
			queryFree($result);

			//
			// further constraint requirements for cross-stance moderation.
			// Fall back on moderating immediate stance children.
			if($argRow['repliesSupport'] + $argRow['repliesOppose'] <= $mod_minarg
				|| $argRow['repliesSupport'] < 1 || $argRow['repliesOppose'] < 1
				|| $argRow['stanceEffective'] < 0.0)
			{
				$parentId = 0; // default to stance as parent
			}
			else
			{
				$parentId = $argRow['argumentId'];
				$nSupport = $argRow['repliesSupport'];
				$nOppose = $argRow['repliesOppose'];
			}
		}

		if($parentId == 0)
		{
			$nSupport = $sessStanceRow['repliesSupport'];
			$nOppose = $sessStanceRow['repliesOppose'];
		}

		if($nSupport < 1 || $nOppose < 1)
			continue; // need at least two arguments to moderate

		// get a list of all supporting arguments of $parentId
		$query = "select argumentId from argument where stanceId='$stance' && parentId='$parentId' && stance=1 && stanceEffective>='0.0' order by date desc";
		//echo "$query\n";
		$result = mysql_query($query);
		if($result == FALSE)
			do_err("failed: $query");
		$nSupport = queryNumRow($result);
		if($nSupport <= 0)
			continue;
		$random = rand(0, $nSupport-1);
		//echo "random1: $random1\n";
		querySeekRow($result, $random);
		$row = queryNextRow($result);
		if($row == FALSE)
			do_err("failed to fetch random argument");
		$argId1 = $row['argumentId'];
		queryFree($result);

		// get a list of all supporting arguments of $parentId
		$query = "select argumentId from argument where stanceId='$stance' && parentId='$parentId' && stance=0 && stanceEffective>='0.0' order by date desc";
		//echo "$query\n";
		$result = mysql_query($query);
		if($result == FALSE)
			do_err("failed: $query");
		$nOppose = queryNumRow($result);
		if($nOppose <= 0)
			continue;
		$random = rand(0, $nOppose-1);
		//echo "random1: $random1\n";
		querySeekRow($result, $random);
		$row = queryNextRow($result);
		if($row == FALSE)
			do_err("failed to fetch random argument");
		$argId2 = $row['argumentId'];
		queryFree($result);

		break;
	} // while(42)

	return TRUE;

// 
// I had tried to come up with a query for selecting all permutation
// of arguments that hadn't been moderated yet.  It might still be
// possible, but I gave up in the name of futility:
//
//	"select t1.argumentId,t2.argumentId from argument as t1 inner join argument as t2 on t1.stanceId=t2.stanceId where t1.argumentId!=t2.argumentId && t1.stanceId='$stance' && t1.parentId=t2.parentId order by t1.date desc"
//	"select t1.argumentId,t2.argumentId from argument as t1 inner join argument as t2 on t1.stanceId=t2.stanceId where t1.stanceId='$stance' left join moderation where t1.argumentId != moderation.argId1 && t2.argumentId != moderation.argId2"

}

function getModeration($argId1, $argId2, $userId)
{
	$query = "select * from moderation where argId1='$argId1' && argId2='$argId2' && userId='$userId'";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;

	$modRow = mysql_fetch_assoc($query_handle);
	mysql_free_result($query_handle);
	return $modRow;
}

// ATTENTION: this function doesn't update the the argument and
// stance scores.  a call to refreshModeration is still necessary.
function deleteModeration($argId1, $argId2, $userId)
{

	switch($modRow['result'])
	{
		case 0:
			// arg1 won
			$argRow1['scorePos'] += $score;
			$argRow2['scoreNeg'] += $score;
			$bArg1 = $bArg2 = TRUE; // update both arguments
			// add score to users total karma
			updateUserScore($argRow1['userId'], $score);
			updateUserScore($argRow2['userId'], -$score);
			break;
		case 1:
			// arg2 won
			$argRow1['scoreNeg'] += $score;
			$argRow2['scorePos'] += $score;
			$bArg1 = $bArg2 = TRUE; // update both arguments
			// add score to users total karma
			updateUserScore($argRow1['userId'], -$score);
			updateUserScore($argRow2['userId'], $score);
			break;
	}

	$sqlDate = date("Y-m-d H:i:s", time());
	$query = "insert into moderation (argId1,argId2,userId,score,result,date) values ('$argId1','$argId2','$userId','$score','$result','$sqlDate')";
	//echo "$query<BR>";
	if(mysql_query($query) == FALSE)
		return FALSE;

	if($bArg1)
	{
		$query = "update argument set scorePos='$argRow1[scorePos]',scoreNeg='$argRow1[scoreNeg]' where argumentId='$argId1'";
		//echo "$query<BR>";
		if(mysql_query($query) == FALSE)
			do_err("failed to update stance of argument $argId1");
	}

	if($bArg2)
	{
		$query = "update argument set scorePos='$argRow2[scorePos]',scoreNeg='$argRow2[scoreNeg]' where argumentId='$argId2'";
		//echo "$query<BR>";
		if(mysql_query($query) == FALSE)
			do_err("failed to update stance of argument $argId2");
	}
	return TRUE;
}

function newPoint($userId, $pointText, $topic, $tags)
{
	global $sessUserId;

	// by default, prepend the 
	if($sessUserId == $userId)
		$tags = $sessUserName . $tags;

	$sqlDate = date("Y-m-d H:i:s", time());

	$pointText = db_escape($pointText);
	$tags_esc = db_escape($tags);
	$query = "insert into point (text,date,submitter,tags) values ('$pointText','$sqlDate','$userId','$tags_esc')";
	$handle = mysql_query($query)
		or do_err("Failed to insert new point.");
	
	$pointId = mysql_insert_id();
	if($pointId == 0)
		do_err("point insertion failed to retrieve auto_increment id");

	if($topic)
	{
		$query = "insert into point_topic (pointId,topicId) values ('$pointId','$topic')";
		mysql_query($query)
			or do_err("Failed to insert point topic");
	}

	do_log(1, "new point", "pointId:$pointId text:$pointText");

	if(!newTags($tags, TAG_TYPE_POINT, $pointId))
		// I'm not going to delete the argument, if the tags fail.
		do_log(2,"newPoint","Failed to add new tags to point:$pointId");

	return $pointId;
}

function newStance($pointId, $userId)
{
	// check if the stance is already there
	$query = "select stanceId from stance where pointId='$pointId' && userId='$userId'";
	$query_handle = mysql_query($query);
	if($query_handle != FALSE)
	{
		$stanceRow = queryNextRow($query_handle);
		queryFree($query_handle);
		$stanceId = $stanceRow['stanceId'];
	}

	if($stanceId == FALSE)
	{
		$sqlDate = date("Y-m-d H:i:s", time());
		// 
		// insert and retrieve new stance
		//
		$query = "insert into stance (pointId,userId,modified) values ('$pointId', '$userId','$sqlDate')";
		$handle = mysql_query($query);
		if($handle == FALSE)
			do_err("new stance on new point failed");

		$stanceId = mysql_insert_id();
		if($stanceId == 0)
			do_err("stance insertion failed to retrieve auto_increment id");
	}

	do_log(1, "new stance", "pointId:$pointId stanceId:$stanceId");

	refreshStanceModeration($stanceId);

	return $stanceId;
}

function newStanceLink($stanceId, $argumentId)
{
	$query = "insert into stance_link set stanceId='$stanceId', argumentId='$argumentId'";
	mysql_query($query) or do_err("Failed to insert new stance_link");

	$query = "select stanceLinkId from stance_link where stanceId='$stanceId' && argumentId='$argumentId'";
	$query_handle = mysql_query($query);
	$linkRow = queryNextRow($query_handle);
	if($linkRow == FALSE)
		do_err("Failed to retrieve new stance link row");
	$linkId = $linkRow['stanceLinkId'];
	queryFree($query_handle);

	return $linkId;
}

function formArgument($userId, $stanceId, $parentId, $stance, $title, $text)
{
	$modgroup = mt_rand(1, $num_modgroup);
	$sqlDate = date("Y-m-d H:i:s", time());

	$row['argumentId'] = 0; // invalid
	$row['stanceId'] = $stanceId;
	$row['parentId'] = $parentId;
	$row['userId'] = $userId;
	$row['title'] = $title;
	$row['text'] = $text;
	$row['stance'] = $stance;
	$row['date'] = $sqlDate;
	$row['modgroup'] = $modgroup;
	return $row;
}

function newArgument($userId, $stanceId, $parentId, $stance, $title, $text, $tags, $link)
{
	global $num_modgroup;

	// infinite loop will occur when refreshing if:
	if($link && $stanceId == $link)
		do_err("Cannot link argument to parent stance");

	if($stance < 0 || $stance > 2)
		do_err("Invalid stance type $stance");

	// verify that the parent argument exists
	if($stanceId == 0)
	{
		if($parentId == 0)
			do_err("Invalid parent argument");
	}

	$modgroup = mt_rand(1, $num_modgroup);
	$sqlDate = date("Y-m-d H:i:s", time());

	$title = db_escape($title);
	$text = db_escape($text);
	$tags_esc = db_escape($tags);
	$query = "insert into argument (stanceId,parentId,userId,title,text,stance,date,modgroup,tags) values ('$stanceId','$parentId','$userId','$title','$text','$stance','$sqlDate','$modgroup','$tags_esc')";
	//echo "$query<BR>\n";

	mysql_query($query)
		or do_err("Failed to insert new argument.");

	$argumentId = mysql_insert_id();
	if($argumentId == 0)
		do_err("argument insertion failed to retrieve auto_increment id");

	do_log(1, "new argument", "stanceId:$stanceId argId:$argumentId text:$text");

	if($link)
		newStanceLink($link, $argumentId);

	// logged in users have a default 'up' moderation
	if(!validUser($userId))
	{
		// replies still need to be accounted for.
		$argRow = getArgument($argumentId);
		if($argRow == FALSE)
			do_err("new argument failed. unable to retrieve aid:$argumentId");
		refreshModeration($argRow);
	}
	else
		newModerationSingle($userId, $argumentId, 1); // mod up by 1

	if(!newTags($tags, TAG_TYPE_ARG, $argumentId))
		// I'm not going to delete the argument, if the tags fail.
		do_log(2,"newArgument","Failed to add new tags to argument $argumentId");

	return $argumentId;
}

function getUser($userId)
{
	$query = "select * from user where userId='$userId'";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;

	$userRow = mysql_fetch_assoc($query_handle);
	mysql_free_result($query_handle);

	$userRow['login'] = db_restore($userRow['login']);
	$userRow['pass'] = db_restore($userRow['pass']);
	return $userRow;
}


function getStanceLink($linkId)
{
	$query = "select * from stance_link where stanceLinkId='$linkId'";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;

	$linkRow = mysql_fetch_assoc($query_handle);
	mysql_free_result($query_handle);
	return $linkRow;
}

function queryRecentUserPoints($userId, $count, $offset, $topic)
{
	if($count > 100)
		$count = 100;

	if($topic == 0)
	{
		// whoa, bottleneck here?
		$query = "select * from point inner join stance on point.pointId=stance.pointId && stance.userId='$userId' order by point.date desc limit $count offset $offset";
	}
	else
	{
		$query = "select * from point,point_topic,stance where point.pointId=point_topic.pointId && point.pointId=stance.pointId && point_topic.topicId='$topic' && stance.userId='$userId' order by point.date desc limit $count offset $offset";
	}
	//echo $query;
	return mysql_query($query);
}

function queryRecentConsensus($count, $offset, $topic)
{
	global $consensusId;
	return queryRecentUserPoints($consensusId, $count, $offset, $topic);
}

function queryActiveUserPoints($userId, $count, $offset, $topic)
{
	//
	// TODO: get to the bottom of the count/offset troubles when they
	// aren't quoted.  do_num should keep us safe, but I don't like
	// it.  I bet a lot of mysql webapps are vulnerable to this.
	//
	if($count > 100)
		$count = 100;

	if($topic == 0)
	{
		// whoa, bottleneck here?
		$query = "select * from point inner join stance on point.pointId=stance.pointId && stance.userId='$userId' order by stance.certainty desc limit $count offset $offset";
	}
	else
	{
		$query = "select * from point,point_topic,stance where point.pointId=point_topic.pointId && point.pointId=stance.pointId && point_topic.topicId='$topic' && stance.userId='$userId' order by stance.certainty desc limit $count offset $offset";
	}
//	echo "$query<BR>\n";
	return mysql_query($query);
}

function queryActiveConsensus($count, $offset, $topic)
{
	global $consensusId;
	return queryActiveUserPoints($consensusId, $count, $offset, $topic);
}

function getNextConsensus($query_handle)
{
	$row = queryNextRow($query_handle);
	if($row == FALSE)
		return FALSE;
	$row['text'] = db_restore($row['text']);
	return $row;
}

function getPoint($pointId)
{
	$query = "select * from point where pointId='$pointId'";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;

	$pointRow = mysql_fetch_assoc($query_handle);
	mysql_free_result($query_handle);
	
	$pointRow['text'] = db_restore($pointRow['text']);
	return $pointRow;
}

function queryAllPoints()
{
	$query = "select * from point";
	$query_handle = mysql_query($query);
	return $query_handle;
}

function queryPoints($topicId, $count, $offset)
{
	$query = "select * from point inner join point_topic on point.pointId=point_topic.pointId && point_topic.topicId='$topicId' order by point.date desc limit '$count' offset '$offset'";
	//echo $query;
	return mysql_query($query);
}

function getNextPoint($query_handle)
{
	$pointRow = queryNextRow($query_handle);
	if($pointRow == FALSE)
		return FALSE;
	$pointRow['text'] = db_restore($pointRow['text']);
	return $pointRow;
}

function getArgument($argumentId)
{
	$query = "select * from argument where argumentId='$argumentId'";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;

	$argRow = mysql_fetch_assoc($query_handle);
	mysql_free_result($query_handle);

	$argRow['title'] = db_restore($argRow['title']);
	$argRow['text'] = db_restore($argRow['text']);
	return $argRow;
}

function queryArguments($stanceId, $parentId, $stance, $method, $dir, $score)
{
	if($dir)
		$sort = "asc";
	else
		$sort = "desc";
	
	if($score < -1.0)
		$score = -1.0;
	else if($score > 1.0)
		$score = 1.0;

	$filterField = "stanceEffective";

	switch($method)
	{
		case 1:
			// by cross-stance effective moderation
			$order = "effective";
			break;
		case 2:
			// in-stance effective moderation
			$order = "stanceEffective";
			break;
		case 3:
			// by date
			$order = "date";
			break;
		case 4:
			// by number of replies
			$order = "repliesTotal";
			break;
		default:
			$order = "effective";
	}
	if($stance >= 0 && $stance <= 2)
		$query = "select * from argument where stanceId='$stanceId' && parentId='$parentId' && stance='$stance' && $filterField>='$score' order by $order $sort";
	else
		$query = "select * from argument where stanceId='$stanceId' && parentId='$parentId' && (stance='0' || stance='1') && $filterField>='$score' order by $order $sort";

	//echo "$query<BR>\n";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE; // no arguments
	return $query_handle;
}

function getNextArgument($query_handle)
{
	$argRow = queryNextRow($query_handle);
	if($argRow == FALSE)
		return FALSE;
	$argRow['title'] = db_restore($argRow['title']);
	$argRow['text'] = db_restore($argRow['text']);
	return $argRow;
}


function queryChildArguments($stanceId, $parentArgumentId)
{
	$query = "select * from argument where stanceId='$stanceId' && parentId='$parentArgumentId'";
	$query_handle = mysql_query($query);
	return $query_handle;
}

function queryUserArgs($userId, $count, $offset)
{
	$query = "select * from stance,argument,point where argument.userId='$userId' && argument.stanceId=stance.stanceId && point.pointId=stance.pointId order by argument.date desc limit $count offset $offset";

	//$query = "select * from argument where userId='$userId' order by date desc"; // limit $arg_list_max";
	//echo $query;
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;
	return $query_handle;
}

function getNextUserArg($query_handle)
{
	$row = queryNextRow($query_handle);
	if($row == FALSE)
		return FALSE;
	$row['text'] = db_restore($row['text']); // TODO: collision with point
	return $row;
}

function queryAllDebates()
{
	$query = "select * from debate";
	$query_handle = mysql_query($query);
	return $query_handle;
}

function queryAllQuizes()
{
	$query = "select * from quiz";
	$query_handle = mysql_query($query);
	return $query_handle;
}

function queryDebateUsers($debateId)
{
	$query = "select login,user.userId from user inner join debate_user on user.userId=debate_user.userId && debateId='$debateId' order by user.userId asc";
	$query_handle = mysql_query($query);
	return $query_handle;
}

function getDebateStances($debateId)
{
	$query = "select effective,stance.stanceId,stance.userId,stance.pointId from stance inner join debate_point on stance.pointId=debate_point.pointId && debateId='$debateId'";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;

//	$stanceArray = array(array());

	$debateRow = mysql_fetch_assoc($query_handle);
	while($debateRow != FALSE)
	{
		$stance = $debateRow['effective'];
		$pointId = $debateRow['pointId'];
		$userId = $debateRow['userId'];

//		if(isset($stanceArray[$pointId]) == FALSE)
//			$stanceArray[$pointId] = array();

		$stanceArray[$pointId][$userId]['id'] = $debateRow['stanceId'];
		$stanceArray[$pointId][$userId]['stance'] = $stance;
 
		$debateRow = mysql_fetch_assoc($query_handle);
	}
	mysql_free_result($query_handle);
	return $stanceArray;
}

function queryDebatePoints($debateId)
{
	$query = "select * from debate_point inner join point on debate_point.pointId=point.pointId && debateId='$debateId' order by debate_point.pointId asc";
	$query_handle = mysql_query($query);
	return $query_handle;
}

function getStance($stanceId)
{
	$query = "select * from stance where stanceId='$stanceId'";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;
	$stanceRow = mysql_fetch_assoc($query_handle);
	mysql_free_result($query_handle);
	return $stanceRow;
}

function getStanceIndirect($userId, $pointId)
{
	$query = "select * from stance where userId='$userId' && pointId='$pointId'";
	$query_handle = mysql_query($query);

	if($query_handle == FALSE)
		return FALSE;
	$stanceRow = queryNextRow($query_handle);
	if($stanceRow == FALSE)
		return FALSE;
	mysql_free_result($query_handle);
	return $stanceRow;
}

function getTopic($topicId)
{
	$query = "select * from topic where topicId='$topicId'";
	$handle = mysql_query($query);
	if($handle == FALSE)
		return FALSE;
	$topicRow = queryNextRow($handle);
	queryFree($handle);

	$topicRow['title'] = db_restore($topicRow['title']);
	return $topicRow;
}

function queryAllTopic()
{
	return mysql_query("select * from topic");
}

function getNextTopic($query_handle)
{
	$topicRow = queryNextRow($query_handle);
	if($topicRow == FALSE)
		return FALSE;
	$topicRow['title'] = db_restore($topicRow['title']);
	return $topicRow;
}


function queryAllBillTitles()
{
	$query = "select billId,type,number,title_short from bill";
	$query_handle = mysql_query($query);
	return $query_handle;
}

function getBill($billId)
{
	$query = "select * from bill where billId='$billId'";
	$query_handle = mysql_query($query);
	if($query_handle == FALSE)
		return FALSE;

	$row = mysql_fetch_assoc($query_handle);
	mysql_free_result($query_handle);
	return $row;
}

/*
function queryGroupMembers($groupRow, $count, $offset)
{
	$query = "select * from group_user,user where group_user.userId=user.userId && group_user.groupId='$groupRow[groupId]' limit '$count' offset '$offset'";
	return mysql_query($query);
}
*/


?>
