<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

//
// database information should be updated in database.php:openDatabase()
//

//
// time of inactivity, in minutes, before a session times out.
// if a timeout occurs, a login screen will be displayed instead.
$session_timeout = 525600; // in minutes
$cookie_domain = "localhost"; // TODO: see createNewSession()

//
// number of moderation groups.  each user and argument are givin a
// randomly chusen integer of the range [1:$num_modgroup].  A user may
// only moderate arguments which have the same modgroup number.
//
// this should be increased when the number of possible moderators increases
//
$num_modgroup = 2;

//
// minimum number of arguments over a stance before switching to the
// binary moderation system.  So if there are at most 4 arguments,
// then the single moderation links will appear under the arguments.
$mod_minarg = 4;

//
//  mysql column: TEXT
//  A BLOB or TEXT column with a maximum length of 65,535 (2^16 -1)
//	characters.
//
// maximum character limit for body of an argument.  The default is
// small because I think arguments should be to the point.  If a user
// needs more than this, then they probably should be including a link
// to a more formal source, and summarize it.
//
$max_arg_body = 1024;
$max_arg_title = 64;
$max_domain_len = 16; // length at which to cut off domain name display
$max_word_len = 32;

//
// maximum arguments listed per page.  (ie. when viewing user history)
//
$arg_list_max = 20;

//
// location of base stance triangle images
//
$stance_base_image = "/var/www/images/tristance";

//
// userId's of the anonymous and consensus users
$anonId = 1;
$consensusId = 10001;
$newsArgId = 2048;

//
// number of points that will be displayed at one time
// (this is used in the topic section)
//
$point_page_size = 20;

$max_login_pass = 16;
$max_login_name = 16;
$max_login_email = 128;
$max_login_cookie = 128;

$max_point_body = 128;
$max_tags_len = 255;

//
// limit on the number of html tags that may occor in one
// post.  for instance, <BR> is one tag, and <B></B> is one tag too.
$tag_limit = 10;

###
#### INTERNAL USE ONLY, please don't modify any of the settings below
###

$session_do_timeout = FALSE; // TRUE if recent timeout
$session_do_logout = FALSE; // also internal
$header_printed = FALSE; // whether header has been printed yet
$header_description = "debatepoint.com: web-based debate forum"

?>
