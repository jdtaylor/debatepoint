<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

// returns > .5 for support, and < .5 for oppose
function normalizeStance($stance)
{
	/*
	if($stance < -5.0)
		$stance = -5.0;
	else if($stance > 5.0)
		$stance = 5.0;
	return 1.0 - ($stance + 5.0) / 10.0;
	*/
	//return 1.0 - ($stance / 2 + 0.5); // [-1,1]
	return 1.0 - $stance; // [0,1]
}

function normalizeCertainty($certainty)
{
	global $mod_minarg;  // when debates switch to relative moderation

	if($certainty < 0.0)
		$certainty = 0.0;
	else if($certainty > $mod_minarg)
		$certainty = $mod_minarg;
	return $certainty / $mod_minarg;
}

function getTristancePos($stanceRow, &$x, &$y)
{
	// opppose is on the right
	$dx = normalizeStance($stanceRow['effective']);
	$dy = normalizeCertainty($stanceRow['certainty']);

	// _____________________
	// \(45,30)    (390,30)/        
	//  \                 /
	//       (210,140)
	//
	// (210 - 45) / (140 - 30) = 1.5
	// (390 - 45) / (140 - 30) = 3.1363
	//
	// TODO: proper
	$x = (45 + ((1.0 - $dy) * 110) * 1.5) + (($dy * 110) * 3.1363) * $dx;
	$y = ((1.0 - $dy) * 110) + 30;
}

/*
function updateStanceImages($stanceRow)
{
	global $stance_base_image;
	$stanceId = $stanceRow['stanceId'];

	// opppose is on the right
	$dx = normalizeStance($stanceRow['effective']);
	$dy = normalizeCertainty($stanceRow['certainty']);

	$wand = NewMagickWand();
	$draw = NewDrawingWand();

	//
	// LARGE
	//
//	echo $stance_base_image . "_large.png";
//	echo "<BR>\n";

	MagickReadImage($wand, $stance_base_image . "_large.png")
		or do_err("Failed to open template tristance image");

	//$handle = imagick_readimage($stance_base_image . "_large.png");
	//if($handle == FALSE)
	//	do_err("Failed to open template tristance image");
	//if(imagick_iserror($handle))
	//	do_err(imagemagick_failedreason($handle));

	$pixel = NewPixelWand("#FFFFFF");
	DrawSetFillColor($draw, $pixel);

	//MagickSetWandSize( MagickWand mgck_wnd, int columns, int rows )

	//imagick_begindraw($handle);
	//if(!imagick_setfillcolor($handle, "#FFFF00"))
	//	do_err("Failed to set fill color");

	// _____________________
	// \(45,30)    (390,30)/        
	//  \                 /
	//       (210,140)
	//
	// (210 - 45) / (140 - 30) = 1.5
	// (390 - 45) / (140 - 30) = 3.1363
	//
	// TODO: proper
	$x = (45 + ((1.0 - $dy) * 110) * 1.5) + (($dy * 110) * 3.1363) * $dx;
	$y = ((1.0 - $dy) * 110) + 30;

//	echo "dx:$dx, dy:$dy<BR>\n";
//	echo "x:$x, y:$y<BR>\n";

	//DrawPoint($draw, $x, $y);
	DrawCircle($draw, $x, $y, $x+4, $y+4);

	//if(!imagick_drawpoint($handle, $x, $y))
	//	do_err(imagemagick_failedreason($handle));

	MagickDrawImage($wand, $draw)
		or do_err("Failed to draw on tristance image");

	MagickWriteImage($wand, $stance_base_image . "_$stanceId.png")
		or do_err("Failed to write to new tristance image");

	//if(!imagick_writeimage($handle, $stance_base_image . "_$stanceId.png"))
	//	do_err(imagemagick_failedreason($handle));

	DestroyPixelWand($pixel);
	DestroyDrawingWand($draw);
	DestroyMagickWand($wand);

	//
	// SMALL
	//
//	$handle = imagick_readimage($stance_base_image . "_small.png");
//	if($handle == FALSE)
//		do_err("Failed to open template tristance small template image");
//	if(imagick_iserror($handle))
//		do_err(imagemagick_failedreason($handle));
//
//	imagick_begindraw($handle);
//	if(!imagick_setfillcolor($handle, "#FFFF00"))
//		do_err("Failed to set fill color");
//
//	// __________________
//	// \(12,7)    (88,7)/        
//	//  \              /
//	//      (48,30)
//	//
//	// (48 - 12) / (30 - 7) = 1.5652
//	// (88 - 12) / (30 - 7) = 3.3043
//	$x = (12 + ((1.0 - $dy) * 23) * 1.5652) + (($dy * 23) * 3.3043) * $dx;
//	$y = ((1.0 - $dy) * 23) + 7;
//
//	if(!imagick_drawpoint($handle, $x, $y))
//		do_err(imagemagick_failedreason($handle));
//
//	if(!imagick_writeimage($handle, $stance_base_image . "_s$stanceId.png"))
//		do_err(imagemagick_failedreason($handle));
}
*/

function decToHex($byte)
{
    $byte = round($byte);
    $upper = floor($byte / 16);
    $lower = $byte - ($upper * 16); // remainder

    if($upper > 9)
        $strUpper = chr(ord('A') + ($upper - 10));
    else
        $strUpper = chr(ord('0') + $upper);

    if($lower > 9)
        $strLower = chr(ord('A') + ($lower - 10));
    else
        $strLower = chr(ord('0') + $lower);

    return $strUpper . $strLower;
}

function stanceToColor($stanceRow)
{
	$stance = normalizeStance($stanceRow['effective']);
	$certainty = normalizeCertainty($stanceRow['certainty']);

	if($stance <= 0.5)
	{
		// support
		$mod = $stance * 2;
		$mod *= exp($mod);

		$red = $mod * 255;
		$green = 255;
		$blue = $mod * 255;
	}
	else
	{
		// oppose
		$mod = (1.0 - $stance) * 2;
		$mod *= exp($mod);

		$red = 255;
		$green = $mod * 255;
		$blue = $mod * 255;
	}

	$certainty = 1.0 - $certainty; // lower certainty == higher saturation
	$red += $certainty * 255;
	$green += $certainty * 255;
	$blue += $certainty * 255;

	if($red > 255)
		$red = 255;
	if($green > 255)
		$green = 255;
	if($blue > 255)
		$blue = 255;
	
	return "#" . decToHex($red) . decToHex($green) . decToHex($blue);
}

?>
