<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

function printLogin()
{
	echo "<div class=\"login\">\n";
	echo "<form action=\"index.php\" method=\"post\">\n";
	echo "  login: <input type=\"text\" name=\"loginName\" />\n";
	echo "  pass: <input type=\"password\" name=\"loginPass\" />\n";
	echo "  <input type=\"submit\" value=\"log in\" />\n";
	echo "</form>\n";
	echo "<a href=\"newuser.php\">new user</a> &middot;\n";
	echo "<a href=\"forgotpass.php\">forgot</a>\n";
	echo "</div> <!-- login -->\n";
}

function printTopicSelect()
{
	$handle = queryAllTopic();
	if($handle == FALSE)
		return;

	echo "<select name=\"topicId\">\n";
	echo "<option value=\"0\">no topic</option>\n";

	$topicRow = getNextTopic($handle);
	while($topicRow != FALSE)
	{
		$topicId = $topicRow['topicId'];
		echo "<option value=\"$topicId\">$topicRow[title]</option>\n";
		$topicRow = getNextTopic($handle);
	}
	echo "</select>\n";

	queryFree($handle);
}

/* vim failed here */

function printScoreSelect()
{
	echo "<select name=\"score\">\n";
	echo "<option value=\"1\">up</option>\n";
	echo "<option value=\"0\">down</option>\n";
	echo "</select>\n";
	/*
	echo "<select name=\"stance\">\n";
	echo "<option value=\"2\">strongly supports</option>\n";
	echo "<option value=\"1\">somewhat supports</option>\n";
	echo "<option value=\"0\">not relevant</option>\n";
	echo "<option value=\"-1\">somewhat opposes</option>\n";
	echo "<option value=\"-2\">strongly opposes</option>\n";
	echo "</select>\n";
	*/
}

function printRatingSelect()
{
	echo "<select name=\"rating\">\n";
	echo "<option value=\"2\">excellent</option>\n";
	echo "<option value=\"1\">good</option>\n";
	echo "<option value=\"0\">no rating</option>\n";
	echo "<option value=\"-1\">bad</option>\n";
	echo "<option value=\"-2\">horrible</option>\n";
	echo "</select>\n";
}

function printStanceTitle($stanceRow, $pointRow)
{
	global $consensusId;

	if($stanceRow['userId'] == $consensusId)
	{
		echo "<b>'$pointRow[text]'</b>\n";
	}
	else
	{
		$userRow = getUser($stanceRow['userId']);
		if($userRow == FALSE)
			do_err("Unknown userId:$userId");
		echo "$userRow[login]'s stance on <b>'$pointRow[text]'</b>\n";
	}
}

function printStanceScore($stanceRow)
{
	$score = round($stanceRow['effective'] * 100);
	echo "[ support:$score% &middot; certainty:$stanceRow[certainty] ]";
}

function printStance($stanceRow)
{
	$stanceId = $stanceRow['stanceId'];

	$pointRow = getPoint($stanceRow['pointId']);
	if($pointRow == FALSE)
		do_err("Unknown point: $stanceRow[pointId]");

	echo "<div class=\"stance\">\n";
	echo "<div class=\"stance_title\">\n";
	printStanceTitle($stanceRow, $pointRow);
	echo "</div> <!-- stance_title -->\n";
	printStanceScore($stanceRow);
	echo " &middot; ";
	// show both argument and comment replies
	echo "<a ";
	if($pointRow['tags'] != "") echo "rel=\"$pointRow[tags]\" ";
	echo "href=\"debate.php?sid=$stanceId&amp;mode=0\">";
	echo "[$stanceRow[repliesTotal] ";
	if($stanceRow['repliesTotal'] == 1)
		echo "reply";
	else
		echo "replies";
	echo "]</a> &middot; \n";
	echo "<a href=\"debate.php?sid=$stanceId&amp;mode=1\">";
	echo "[$stanceRow[comments] ";
	if($stanceRow['comments'] > 1)
		echo "comments";
	else
		echo "comment";
	echo "]</a>\n";
	echo "<center><div id=\"stanceCanvas\" style=\"position:relative; height:160px; width:440px;\">\n";
	echo "<img src=\"images/tristance_large.jpg\" alt=\"\" />\n";
	echo "</div> <!-- stanceCanvas --></center>\n";
	echo "</div> <!-- stance -->\n";
}

define("ARG_NONE",		0x00000000);
define("ARG_MOD",	 	0x00000001);
define("ARG_SCORE",		0x00000002);
define("ARG_STANCE",	0x00000004);
define("ARG_DETAIL",	0x00000008);
define("ARG_REPLY",		0x00000010);
define("ARG_ALL",		0x0000FFFF);
define("ARG_DEFAULT", ARG_SCORE | ARG_STANCE | ARG_REPLY);


function printArgument($argumentRow, $argOpt=ARG_DEFAULT)
{
	global $sessUserId;
	global $sessUserRow;
	global $anonId;

	$argumentId = $argumentRow['argumentId'];
	$stanceId = $argumentRow['stanceId'];
	$stance = $argumentRow['stance'];
	$userId = $argumentRow['userId'];
	$userRow = getUser($userId);
	$nReply = $argumentRow['repliesSupport'] + $argumentRow['repliesOppose'];
	$tags = $argumentRow['tags'];

	if($argumentId != 0)
		$modRow = getModeration($argumentId, 0, $sessUserId);
	else
		$modRow = FALSE;

	echo "<div class=\"argument\" id=\"arg$argumentId\">\n";
	echo "<div class=\"argument_header\">\n";

	$effective = $argumentRow['effective'];
	$total = $argumentRow['scorePos'] + $argumentRow['scoreNeg'];
	if($total <= 0) {
		$mod = 0.0;
		$argued = 0.0;
	} else {
		$mod = round($argumentRow['scorePos'] / $total * 100);
		// how much it was argued down
		$argued = round($mod - $effective*100);
		if($argued < 0)
			$argued = 0.0;
	}

	//echo "<a href=\"debate.php?aid=$argumentId\">$argumentRow[title]</a><BR/>\n";

	$score = round($effective*100);
	$stanceScore = round($argumentRow['stanceEffective'] * 100);

	if($argOpt & ARG_STANCE)
	{

		switch($argumentRow['stance'])
		{
			case 0:
				echo "<font color=\"#FF0000\">oppose</font> "; break;
			case 1:
				echo "<font color=\"#0000FF\">support</font> "; break;
			case 2:
				echo "(comment) "; break;
		}
	}
	if($argOpt & ARG_SCORE)
		echo "$score% &middot; ";

	echo "<b>$argumentRow[title]</b><BR/>\n";

	if($argOpt & ARG_DETAIL)
	{
		if($argumentRow['stance'] == 2) // comment
			echo "moderated to $mod%<BR/>\n";
		else
			echo "moderated to $mod%; argued down by $argued%<BR/>\n";
	}

	if($userId == 1)
	{
		echo "by $userRow[login] ";
		echo "on $argumentRow[date]\n";
	}
	else
	{
		echo "by <a href=\"user.php?uid=$userId\">$userRow[login]</a> ";
		echo "on $argumentRow[date]\n";
	}

	echo "</div>\n";
	echo "<div class=\"argument_body\">\n";
	if($argumentRow['stanceLinkId'])
	{
		//
		// this argument is a link to a stance
		//
		$linkRow = getStanceLink($argumentRow['stanceLinkId']);
		if($linkRow == FALSE)
			do_err("Invalid stanceLinkId: $argumentRow[stanceLinkId]");

		$stanceRow = getStance($linkRow['stanceId']);
		if($stanceRow == FALSE)
			do_err("Invalid stanceId: $linkRow[stanceId]");

		echo "/* this argument is a link to the results of ";
		//printStanceTitle($stanceRow);
		echo " \n";
		printStanceScore($stanceRow);
		echo " */<BR/>\n";
	}
	else
	{
		echo "$argumentRow[text]\n";
	}
	echo "</div>\n";
	echo "<div class=\"argument_footer\">\n";

	//
	// argId's of 0 probably means we are in preview mode
	//
	if($argumentId != 0 && $argOpt & ARG_REPLY)
	{
		echo "[ ";

		if($argumentRow['stance'] == 2)
		{
			// comment, so only show comment replies
			echo "<a ";
			if($tags != "") echo "rel=\"$tags\" ";
			echo "href=\"debate.php?aid=$argumentId\">";
			echo "$argumentRow[comments] replies</a>\n";
			echo " &middot; \n";
		}
		else
		{
			echo "<a ";
			if($tags != "") echo "rel=\"$tags\" ";
			echo "href=\"debate.php?aid=$argumentId&amp;mode=0\">";
			if($nReply == 1)
				echo "1 reply</a>\n";
			else
				echo "$nReply replies</a>\n";
			echo " &middot; \n";

			if($argumentRow['comments'] > 0)
			{
				if($nReply == 0)
					echo " &middot; \n";
				echo "<a href=\"debate.php?aid=$argumentId&amp;mode=1\">";
				echo "$argumentRow[comments] comments</a>\n";
				echo " &middot; \n";
			}
		}

		if($argumentRow['stance'] == 2)
		{
			echo "<a href=\"newargument.php?aid=$argumentId&amp;stance=2\">\n";
			echo "reply</a>\n";
		}
		else
		{
			echo "<a href=\"newargument.php?aid=$argumentId&amp;stance=1\">\n";
			echo "argue for</a>\n";
			echo " &middot; \n";
			echo "<a href=\"newargument.php?aid=$argumentId&amp;stance=0\">\n";
			echo "argue against</a>\n";
		}

		// TODO: limit who can link
		//	echo "<a href=\"todo\">[link]</a> &middot; \n";
		//
		
		// NOTE: moderation groups taken out because it's just me..
		//		2004/11/14
		//
		//	if($argumentRow['modgroup'] == $sessUserRow['modgroup'])
		//	{
		if(($argOpt & ARG_MOD) && ($sessUserId != $anonId))
		{
			echo " &middot; \n";
			echo "<span id=\"in-stance$argumentId\">\n";
			echo "$stanceScore% ";

			// check to make sure user hasn't already moded this argument
			if($modRow == FALSE)
			{
				//echo " / <a onClick=\"moderate($argumentId, 1)\">";
				echo "<a href=\"javascript:moderate($argumentId, 1);\">";
				echo "up</a>";
				//echo "<a href=\"debate.php?aid=$argumentId&amp;mod=1\">up</a>";
				//echo " / <a onClick=\"moderate($argumentId, 0)\">";
				echo " / <a href=\"javascript:moderate($argumentId, 0);\">";
				echo "down</a>\n";
			}
			echo "</span> <!-- in-stance -->\n";
		}
		echo " ]";
	}
	echo "</div> <!-- argument_footer -->\n";
	echo "</div> <!-- argument -->\n";
}

function printArgumentFold($argumentRow)
{
	// I kinda like the look already
	printArgument($argumentRow);
}

function printArgumentThread($stanceId, $parentId, $stance, $depth, $method, $dir, $score, $argOpt=ARG_DEFAULT)
{
	if($depth <= 0)
		return;
	$depth--;

	$query_handle = queryArguments($stanceId, $parentId, $stance, $method, $dir, $score);
	if($query_handle == FALSE)
		return FALSE;

	$argumentRow = getNextArgument($query_handle);
	if($argumentRow == FALSE)
		return 0;

	echo "<ul>\n";

	while($argumentRow != FALSE)
	{
		echo "<li>\n";
		printArgument($argumentRow, $argOpt);
		echo "</li>\n";
		printArgumentThread($stanceId, $argumentRow['argumentId'], $stance,
							$depth, $method, $dir, $score, $argOpt);

		$argumentRow = getNextArgument($query_handle);
	}
	echo "</ul>\n";

	queryFree($query_handle);
}

function printArgumentLine($childArgumentId, $argOpt=ARG_DEFAULT)
{
	$nArg = 0;
	$parentId = $childArgumentId;
	do
	{
		$argumentRow = getArgument($parentId);
		if($argumentRow == FALSE)
			break;

		$argLine[$nArg] = $parentId;
		$nArg++;
		$parentId = $argumentRow['parentId'];
	}
	while($parentId != 0);

	for($i = $nArg-1; $i >= 0; $i--)
	{
		$argumentRow = getArgument($argLine[$i]);
		if($argumentRow == FALSE)
			do_err("hmm, argument not there anymore $argLine[$i]");

		echo "<ul><li>\n";
		printArgument($argumentRow, $argOpt);
		echo "</li>";
	}
	for($i = $nArg-1; $i >= 0; $i--)
		echo "</ul>";
	echo "\n";
}

function printArguments($stanceId, $parentId, $stance, $method, $dir, $score,
						$argOpt=ARG_DEFAULT)
{
	$query_handle = queryArguments($stanceId, $parentId, $stance, $method, $dir, $score);
	if($query_handle == FALSE) {
		do_err("failed to query arguments (printArguments)");
		return FALSE;
	}

	$argumentRow = getNextArgument($query_handle);
	if($argumentRow == FALSE)
		return 0;
//	echo "<ul>\n";

	while($argumentRow != FALSE)
	{
//		echo "<li>\n";
		printArgument($argumentRow, $argOpt);
//		echo "</li>\n";
		$argumentRow = getNextArgument($query_handle);
	}
//	echo "</ul>\n";

	queryFree($query_handle);
	return 1;
}

function printOpposeArguments($stanceId, $parentId, $method, $dir, $score,
								$argOpt=ARG_DEFAULT)
{
	echo "<div id=\"arguments_oppose\">\n";

	// 0 for oppositions
	if(!printArguments($stanceId, $parentId, 0, $method, $dir, $score, $argOpt))
	{
		printf("no opposition arguments over %.0f%% ", round($score * 100));
	}
	echo "</div> <!-- arguments_oppose -->\n";
}

function printSupportArguments($stanceId, $parentId, $method, $dir, $score,
								$argOpt=ARG_DEFAULT)
{
	echo "<div id=\"arguments_support\">\n";

	// 1 for supportive
	if(!printArguments($stanceId, $parentId, 1, $method, $dir, $score, $argOpt))
	{
		printf("no supportive arguments over %.0f%% ", round($score * 100));
	}
	echo "</div> <!-- arguments_support -->\n\n";
}

function printCommentArguments($stanceId, $parentId, $method, $dir, $score)
{
	echo "<div id=\"arguments_comment\">\n";
	// 2 for comments, default thread depth of 8
	printArgumentThread($stanceId, $parentId, 2, 8, $method, $dir, $score);
	echo "</div> <!-- arguments_comment -->\n";
}

function printUserInfo($userRow)
{
	echo "<div id=\"user_info\">\n";
	echo "Login: ";
	echo "<input type=text name=\"loginName\" value=\"$userRow[login]\" />\n";
	echo "Pass: ";
	echo "<input type=password name=\"loginPass\" value=\"$userRow[pass]\"/>\n";
	echo "<BR/>\n";
	echo "First Name: ";
	echo "<input type=text name=\"fname\" value=\"$userRow[fname]\" />\n";
	echo "Middle Name: ";
	echo "<input type=text name=\"mname\" value=\"$userRow[mname]\" />\n";
	echo "Last Name: ";
	echo "<input type=text name=\"lname\" value=\"$userRow[lname]\" /><BR/>\n";
	echo "email: ";
	echo "<input type=text name=\"email\" value=\"$userRow[email]\" />\n";
	echo "birthday: ";
	echo "<input type=text name=\"birthday\" value=\"$userRow[bday]\" />\n";
	echo "<BR/>\n";
	echo "party: ";
	echo "<input type=text name=\"party\" value=\"$userRow[party]\" />\n";

	echo "</div> <!-- user_info -->\n";
}

function printUserArgs($userRow, $count, $offset)
{
	global $arg_list_max;
	$userId = $userRow['userId'];

	//
	// TODO: ditto with the unquoted limit and offset!
	//
	$query_handle = queryUserArgs($userId, $count, $offset);
	if($query_handle == FALSE) {
		do_err("Failed to select recent user arguments");
		return FALSE;
	}
	$row = getNextUserArg($query_handle);

	echo "<div class=\"user_args\">\n";
	echo "<ul>\n";
	while($row != FALSE)
	{
		echo "<li>\n";
		echo "<b>'$row[text]'</b><BR/>\n";
		printArgumentLine($row['argumentId']);
		echo "</li><hr />\n";

		$row = getNextUserArg($query_handle);
	}
	echo "</ul>\n";
	echo "</div> <!-- user_args -->\n";
	queryFree($query_handle);
}

function printPoint($pid, $sid, $color, $score, $text)
{
	echo "<div id=\"point$pid\" class=\"pointItem\">\n";
	echo "<div class=\"pointStance\" style=\"background-color:$color\">\n";
	printf("%02d%%", $score);
	echo "</div>\n";
	echo "<div class=\"pointTitle\">\n";
	echo "<a href=\"debate.php?sid=$sid\">$text</a>\n";
	echo "</div>\n";
	echo "</div>\n";
}

function printConsensus($handle, $title, $count, $offset)
{
	echo "<div class=\"consensus\">\n";

	/*
	echo "<table cellpadding=3 style=\"display:inline; color:black; float:right;\"><tr>\n";
	echo "<td bgcolor=#00FF00>support</td>\n";
	echo "<td bgcolor=#FFFFFF>uncertain</td>\n";
	echo "<td bgcolor=#FF0000>oppose</td>\n";
	echo "</tr></table>\n";
	*/
	echo "<div class=\"consensus_title\">\n";
//	echo "<div style=\"text-align:left; width:48%; margin-right:auto;\">\n";
	echo "$title\n";
//	echo "</div>\n";
	
	echo "<div style=\"text-align:right; width:100%; margin-left:auto;\">\n";
	echo "<font color=\"#00FF00\">support</font> &middot; \n";
	echo "<font color=\"#FFFFFF\">uncertain</font> &middot; \n";
	echo "<font color=\"#FF0000\">oppose</font>\n";
	echo "</div>\n";
	
	echo "</div> <!-- consensus_title -->\n";

	while(($pointStanceRow = getNextConsensus($handle)) != FALSE)
	{
		printPoint($pointStanceRow['pointId'],
					$pointStanceRow['stanceId'],
					stanceToColor($pointStanceRow),
					round($pointStanceRow['effective'] * 100),
					$pointStanceRow['text']);
	}
	echo "</div> <!-- consensus -->\n";
}

function printActiveConsensus($count, $offset, $topic)
{
	$handle = queryActiveConsensus($count, $offset, $topic);
	if($handle == FALSE)
		return;
	$n = queryNumRow($handle);
	if($n <= 0)
		return 0;
	
	printConsensus($handle, "most active debate points: ", $count, $offset);

	queryFree($handle);
	return $n;
}

function printRecentConsensus($count, $offset, $topic)
{
	$handle = queryRecentConsensus($count, $offset, $topic);
	if($handle == FALSE)
		return;
	
	printConsensus($handle, "new debate points: ", $count, $offset);

	queryFree($handle);
}

function printTopicList()
{
	$handle = queryAllTopic();
	if($handle == FALSE)
		return;

	echo "<div class=\"topic_list\">\n";
	echo "<div class=\"topic_list_title\">\n";
	echo "point topics:\n";
	echo "</div> <!-- topic_list_title -->\n";

	echo "<ul>\n";

	while(($topicRow = getNextTopic($handle)) != FALSE)
	{
		echo "<li><a href=\"index.php?topic=$topicRow[topicId]\">";
		echo "$topicRow[title]</a></li>\n";
	}
	queryFree($handle);

	echo "</ul>\n";
	echo "</div> <!-- topic_list -->\n";
}

function printPointList($topicId, $count, $offset)
{
	global $consensusId;

	$topicRow = getTopic($topicId);
	if($topicRow == FALSE)
		do_err("Bad topicId");

	// TODO: inner join query with stance table
	$handle = queryPoints($topicId, $count, $offset);
	if($handle == FALSE)
		return;

	echo "<div class=\"point_list\">\n";
	echo "<div class=\"point_list_title\">\n";
	echo "$topicRow[title]\n";
	echo "</div> <!-- point_list_title -->\n";

	echo "<ul>\n";

	while(($pointRow = getNextPoint($handle)) != FALSE)
	{
		//$stanceId = getStanceId($consensusId, $pointRow['pointId']);
		//if($stanceId == FALSE)
		//	continue;
		//$stanceRow = getStance($stanceId);
		$stanceRow = getStanceIndirect($consensusId, $pointRow['pointId']);
		if($stanceRow == FALSE)
			continue;
		$stanceId = $stanceRow['stanceId'];

		echo "<li>\n";
		printf(" %d : %d ", $stanceRow['effective'] * 10,
							$stanceRow['certainty'] * 10);

		echo "&middot; <a href=\"debate.php?sid=$stanceId\">";
		echo "$pointRow[text]</a></li>\n";
	}
	queryFree($handle);

	echo "</ul>\n";
	echo "</div> <!-- point_list -->\n";
}

/*
function printGroupMembers($groupRow, $count, $offset)
{
	$handle = queryGroupMembers($groupRow['groupId'], $count, $offset);
	if($handle == FALSE)
		return;

	echo "<div id=\"group_members\">\n";
	echo "<div class=\"group_members_title\">\n";
	echo "$groupRow[title] members:\n";
	echo "</div> <!-- group_members_title -->\n";

	while(($memberRow = queryNextRow($handle)) != FALSE)
	{
	}
	queryFree($query);

	echo "</div> <!-- group_members -->\n";
}
*/

function printCopyrightNotice()
{
	echo "<div style=\"font-weight: bold; font-size: 120%;\">DO NOT SUBMIT COPYRIGHTED WORK WITHOUT PERMISSION!</div>\n";
	echo "<ul>\n";
	echo "<li>All contributions to debatepoint.com are released under the GNU Free Documentation License (see <a href=\"http://www.gnu.org/copyleft/fdl.html\">GFDL</a> for details).</li>\n";
	echo "<li>By submitting your work you promise you wrote it yourself, or copied it from <a href=\"http://www.wikipedia.org/wiki/Public_domain\">public domain</a> -- this does <b>not</b> include most web pages.</li>\n";
	echo "\n";
	echo "</ul>\n";
}

function printAbout()
{
	echo "<div id=\"about\">\n";
	echo "<p><b>What is debatepoint?</b><BR/><BR/>\n";
	echo "Debatepoint is a deliberation tool.  It can be used to organize arguments and formalize a popular consensus over issues.</p>\n";
	echo "<p>A consensus is determined based on the moderation of supporting and opposing arguments.  Each argument is subject to further deliberation by child arguments in the same fashion.  The structure of each debate forms a tree of arguments.  The root of this tree is a single support/oppose stance for the particular point being debated.</p><BR/>\n";

	echo "<b>Possible areas for use of debatepoint.com:</b><BR/>\n";
	echo "<ul>\n";
	echo "<li><i>politics</i>: find a consensus over political issues</li>\n";
	echo "<li><i>free software</i>: debate features and methods of development</li>\n";
	echo "<li><i>blog feedback</i>: submit points of your blog entries</li>\n";
	echo "</ul><BR/>\n";

	echo "<b>Terminology:</b><BR/>\n";
	echo "<ul>\n";
	echo "<li><i>point</i>: short statement in which to argue over.  ie. a point might assert a position over a known issue.</li>\n";
	echo "<li><i>argument</i>: a statement either supporting or opposing a point or parent argument.</li>\n";
	echo "<li><i>consensus</i>: resulting moderation on points and arguments.</li>\n";
	echo "<li><i>stance</i>: whether an argument is in support of or in opposition to its parent.  The stance of a point is also sometimes used to refer to the resulting consensus over a point.</li>\n";
	echo "<li><i>certainty</i>: the certainty that a consensus is representative \"enough\".</li>\n";
	echo "</ul><BR/>\n";

	echo "<b>Moderation Summary:</b><BR/>\n";
	echo "<BR/>\n";
	echo "Every argument can be moderated in two ways: in-stance and cross-stance.  The outcome of each type of moderation is presented as percentages of their positive versus negative moderation.\n";
	echo "<ul>\n";
	echo "<li>in-stance: eg. moderation between two supporting arguments.</li>\n";
	echo "<ul><li>this moderation controls the presentation order relative to other arguments of like stance.  ie. the arguments vertical placement in the list of arguments.  This default can be changed via the sorting methods available.</li>\n";
	echo "<li>the in-stance moderation is also used as the basis for the consensus of its parent in cases where there are too few arguments available for cross-stance moderation.</li>\n";
	echo "<li>every argument has \"up\" and \"down\" links for in-stance moderation.  If an argument does not, then either you are not logged in, or you have already moderated that argument.</li>\n"; 
	echo "</ul>\n";
	echo "<li>cross-stance: moderation between supportive and opposing arguments.</li>\n";
	echo "<ul>\n";
	echo "<li>moderation of this form is ideally the sole basis for
	consensus.</li>\n";
	echo "<li>the moderator is presented with two random arguments, one supportive and one opposing.  The moderator then chooses which is a more convincing argument and moves on to the next two random arguments.  The idea here is to binary sort the arguments to find the cumulative consensus between all supportive arguments and all opposing arguments.</li>\n";
	echo "<li>early in 2005, there was no cross-stance moderation.  I found that arguments were not being judged fairly, however, so this is my initial solution.  We'll have to see how it turns out and whether it stays or not.  So far, I like its relative nature and how it prevents moderators from selectively judging.  There are still obvious flaws.. like relying on human nature for one.  A moderation of the moderators might be helpful there.</li>\n";
	echo "</ul>\n";
	echo "</ul><br/>\n";
	echo "<center><img src=\"images/in-stance_vs_cross-stance.jpg\" alt=\"\" /></center><br/>\n";

	echo "<p>The stance of a point is usually determined solely by the cross-stance moderation.  In situations where there aren't enough arguments to cross-stance moderate, the in-stance moderation will be used temporarily.";
	echo "</p>\n";
	echo "<center><img src=\"images/cross-stance_pointer.jpg\" alt=\"\" /></center><br/>\n";
	echo "<hr/><br/>\n";
	echo "<div id=\"downloads\">\n";
	echo "Source code for debatepoint.com is hosted in CVS at ";
	echo "<a href=\"http://savannah.nongnu.org/projects/debatepoint/\">";
	echo "Savannah</a>.<BR/>\n";
	echo "It is licensed under the terms of the ";
	echo "<a href=\"http://www.gnu.org/copyleft/gpl.html\">";
	echo "GNU General Public License (GPL)</a><BR/>\n";
	echo "</div> <!-- downloads -->\n";
	echo "<hr/><br/>\n";
	echo "debatepoint is currently being written and hosted by:<br/>\n";
	echo "James D. Taylor &lt;james.d.taylor (a) gmail.com&gt;<br/>\n";
	echo "<div class=\"public_key\">\n";
	echo "-----BEGIN PGP PUBLIC KEY BLOCK-----<br/>\n";
	echo "Version: GnuPG v1.4.1 (GNU/Linux)<br/>\n";
	echo "<br/>\n";
	echo "mQGiBELlUu4RBADe8gRvLOoRQK0WKSr85dVKiGACcTkUTn5r5syi5diyUO4GTYmE<br/>\n";
	echo "nCAHU10Y670B2c2+TN0mkiDM3SiUlmtSFR/R8+aXS0ZlN7QNWnTXZGmqKeUopTjc<br/>\n";
	echo "6EVq9OJCvfY4E/rGPkAm4le/rH+m1gmS7zFrTDqUilLzSvvZif10F5jv8wCgpx0n<br/>\n";
	echo "uFpiJ3rr2ywgrSZIm9+mWcsD/A94ZuP4vYiwOZlAcj4/K5/Uc7RVBMSZNHUt97QU<br/>\n";
	echo "oU71Vo8HOgy36hq+gWHkXYez1tYeDPqzzO8oBq+QXSt052zh81EzNBByftYia1fj<br/>\n";
	echo "blduqEwCQAm/jyFLGKzX8RajrO5f/qYKZh+3/+eiaqaH9DHRIeBp+5kkCBSuqY4j<br/>\n";
	echo "0hptBADB/cGEwE+8FgKx6wclzouRMnPCfyHJfVfPVks9z0S6Mlai7xAQYEVFwXE2<br/>\n";
	echo "mvRkmUiGTnzQzYTSiqWi1GyWwNACjuRmIvPd7o8xD29EZgVh6tkcv8XtYZniWTeV<br/>\n";
	echo "6yt6gZNN6oLMq06rNIynJ2AEyGHvd1h1ChHwBP5uZ+n/PrfMUrQqSmFtZXMgRC4g<br/>\n";
	echo "VGF5bG9yIDxqYW1lcy5kLnRheWxvckBnbWFpbC5jb20+iF4EExECAB4FAkLlUu4C<br/>\n";
	echo "GwMGCwkIBwMCAxUCAwMWAgECHgECF4AACgkQmqWA/xFbtmZKSACgpPAd9cN8vldn<br/>\n";
	echo "gcITY9nEdGD9m5cAn3u/wJmqziTSzxs6dxW6ekCzO4eVuQINBELlUvYQCADWov7z<br/>\n";
	echo "FE86MwZBw2ej7dZbr6kGCb3Jv90jv6LlMpIrv/YzGr6+9pcxd3oErhcLAsZAuvn6<br/>\n";
	echo "GqoRdvb9RfMXzYiUAT4lSRU9bdDkOWlS1wD+6gsgi+iikTIRWg6A49ISsYJi3Nej<br/>\n";
	echo "WI+nkjK1dUnTXfxAy4O7/JQLql7zo86DjZ10wxsqx65Ee+de+d6PuPJ1xNdXzm5O<br/>\n";
	echo "Y+wBZk18R2op1mWhwPNMxE9WpAmmxPbTWrQFyvgCVxrDg3UjLTyWGgmKmu4JlS2F<br/>\n";
	echo "zN+hBagLbGwjjxW06YyjWCKNWlsc60TMMZizSn/1berxkZwru+4As7futWm8ywX3<br/>\n";
	echo "LIkCfPxFLVm2lpqvAAMFCADQcjhojl8tv77jpByRyumy/hNTVWPV1gxzwo3RZ6Uf<br/>\n";
	echo "i8pa+ARL6AhQKJMVzt8Cr0Ak6lmfbm6zggXuJScnvgb0vi7L7lQI5k5ak6gOELvy<br/>\n";
	echo "I7AtbDzzbKdqIoDCZP0rdIqO3MFk7kVoE3MBTsZed/XKjdd3eRxIvw0qaIOemM/8<br/>\n";
	echo "N4b1iG3XQqdrcqHwOlUe0dRJjc8pZjbgwwWQ2gYMxShMRVNdyZ7cmnmbNiXYYUKX<br/>\n";
	echo "DQuGChHPKvTcx/sBk7zgt7LpdZ1sA3sr54dbkliIhjrFa+CtmweesJU3jzuEESWI<br/>\n";
	echo "b9TcTILMN7S1vXPYUev7j4ZOcS2YfDu/851Oiz5DOuy1iEkEGBECAAkFAkLlUvYC<br/>\n";
	echo "GwwACgkQmqWA/xFbtmZSEACeKpdMLhYH5Qv/DeZUBZuaS1gGPtYAnAlD/ON+9pE2<br/>\n";
	echo "dsWB0p5Ea24Rf0b2<br/>\n";
	echo "=IATZ<br/>\n";
	echo "-----END PGP PUBLIC KEY BLOCK-----<br/>\n";
	echo "</div> <!-- public_key -->\n";
	echo "</div> <!-- about -->\n";
}

function printLinks()
{
	echo "<div id=\"links\">\n";
	echo "<b>Links to other debate sites that I'm aware of:</b> ";
	echo "<ul>\n";
	echo "<li><a href=\"http://www.online-deliberation.net/conf2005/\">\n";
	echo "The Second Conference on Online Deliberation at Stanford University</a></li>\n";
	echo "<li><a href=\"http://parliament.sf.net/\">parliament.sf.net</a> :\n";
	echo "parliamentary procedure software from Stanford\n";
	echo "<li><a href=\"http://www.hyperdebat.net\">www.hyperdebat.net</a> :\n";
	echo "(french) debates are structured by a facilitator, who inserts the arguments into a readable tree structure.</li>\n";
	echo "</ul>\n";
	echo "</div> <!-- links -->\n";
}

function printDownloads()
{
}

function printFooter()
{
	echo "<hr/>\n";
	echo "<center><div id=\"copyright\">All text is available under the terms of the <a href=\"http://www.gnu.org/copyleft/fdl.html\">GNU Free Documentation License</a> (see <b><a href=\"copyright.php\">Copyrights</a></b> for details).</div></center>\n";
	echo "</div> <!-- body -->\n";
	echo "</body>\n";
	echo "</html>\n";
}

function printHeader()
{
	global $anonId;
	global $sessUserId;
	global $sessUserName;
	global $sessUserRow;
	global $sessRow;
	global $session_do_timeout;
	global $session_do_logout;

	global $header_description;
	global $header_printed;
	$header_printed = TRUE;

//	apd_set_pprof_trace();

	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n";
	echo "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
	echo "<html xml:lang=\"en\" lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\n";
	echo "<head>\n";
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"default.css\" />\n";
//	echo "<link rel=\"icon\" type=\"image/png\" href=\"/foot-16.png\" />\n";
	echo "<link rel=\"icon\" href=\"favicon.ico\" type=\"image/x-icon\" />\n";
//    echo "<link rel=\"shortcut icon\" href=\"favicon.ico\" type=\"image/x-icon\" />\n";
	echo "<meta name=\"description\" content=\"$header_description\"></meta>\n";
    echo "<meta name=\"keywords\" content=\"debate, issues, moderate, stance\"></meta>\n";
	// only request vector graphics support when in debate.php
	if(strpos($_SERVER['PHP_SELF'], "debate.php")) {
		echo "<script type=\"text/javascript\" src=\"wz_jsgraphics.js\"></script>\n";
		echo "<script type=\"text/javascript\" src=\"async.js\"></script>\n";
	}
	echo "<script src=\"http://www.google-analytics.com/urchin.js\" type=\"text/javascript\"></script>\n";
	echo "<script type=\"text/javascript\">\n";
	echo "\t_uacct = \"UA-69115-1\";\n";
	echo "\turchinTracker();\n";
	echo "</script>\n";
	echo "<title>$header_description</title>\n";
	echo "</head>\n";
	echo "<body>\n";
	echo "<div id=\"header\">\n";
	echo "<div id=\"title\">\n";
	echo "<a href=\"index.php\"><b>debatepoint</b></a> <font size=\"1pt\"><sup>beta</sup></font>\n";
	echo "</div> <!-- title -->\n";
	echo "<div id=\"menu\">\n";
//	echo "<a href=\"debate.php?id=1\">2004 Presidential Debate</a> &middot;\n";

	//if($sessUserId != $anonId)
	//	echo "<a href=\"user.php\">$sessUserName</a> &middot;\n";

	//echo "<a href=\"index.php\">topics</a> &middot;\n";
	echo "<a href=\"news.php\">news</a> &middot;\n";
	echo "<a href=\"newpoint.php\">make point</a> &middot;\n";
	//echo "<a href=\"pizza.php\">pizza</a> &middot;\n";
	if($sessUserId == $anonId)
		echo "<a href=\"logout.php\">login</a> &middot;\n";
	else
		echo "<a href=\"logout.php\">logout</a> &middot;\n";
	echo "<a href=\"about.php\">about</a>\n";
	echo "</div> <!-- menu -->\n";
	echo "</div> <!-- header -->\n";
	echo "<div id=\"body\">\n";

	if($session_do_timeout)
	{
		// TODO: try to retrun to previous session's page.
		echo "Your session has recently expired.  Refresh to continue as anonymous, or log back in, eh:\n";
		printLogin();
		printFooter();
		die("");
	}
	if($session_do_logout)
	{
		deleteOldSessions($sessUserId, $sessRow['ipaddr']);
		echo "You are now logged out.  Take off, or log back in, eh:\n";
		printLogin();
		printFooter();
		die("");
	}
}


?>
