<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

//
// credits to vpribish at shopping dot com
//
// big thanks to wielspm and mittag for the function to convert wide
// utf to html.  I have altered your function to output decimal html
// entities which i hear are more widely supported.  Also I've
// optimized it, it is now twice as fast. 
//
function utf2html($str)
{
	$ret = "";
	$max = strlen($str);
	$last = 0;  // keeps the index of the last regular character

	for($i = 0; $i < $max; $i++)
	{
		$c = $str{$i};
		$c1 = ord($c);

		// 110x xxxx, 110 prefix for 2 bytes unicode
		if($c1 >> 5 == 6)
		{
			// append all the regular characters we've passed
			$ret .= substr($str, $last, $i-$last); 
			$c1 &= 31; // remove the 3 bit two bytes prefix
			$c2 = ord($str{++$i}); // the next byte
			$c2 &= 63;  // remove the 2 bit trailing byte prefix
			$c2 |= (($c1 & 3) << 6); // last 2 bits of c1 become first 2 of c2
			$c1 >>= 2; // c1 shifts 2 to the right

			// this is the fastest string concatenation
			$ret .= "&#" . ($c1 * 100 + $c2) . ";";
			$last = $i+1;       
		}
	}
	// append the last batch of regular characters
	return $ret . substr($str, $last, $i);
}

?>
