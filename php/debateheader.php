<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require_once("global.php");
require_once("session.php");
require_once("database.php");
require_once("prints.php");
require_once("image.php");

//
// levels:
//	0: debug
//	1: access
//	2: warning
//	3: error
//
function do_log($level, $context, $str)
{
	global $sessRow;
	global $sessUserRow;

	$str = "$sessRow[ipaddr] $sessUserRow[userId] - $context - $str";

	//$access = date("Y/m/d H:i:s");
	switch($level)
	{
		case 0: syslog(LOG_DEBUG, $str); break;
		case 1: syslog(LOG_INFO, $str); break;
		case 2: syslog(LOG_WARNING, $str); break;
		case 3: syslog(LOG_ERR, $str); break;
		default: syslog(LOG_ERR, "do_log: unknown log level $level"); break;
	}
}

function do_warn($str)
{
	do_log(2, "warn", $str);
}

function do_err($str)
{
	global $header_printed;

	// consider using error_log() also, in order to get the error to
	// show up in apache's error log

	do_log(3, "err", $str);

	// TODO: post a log message
	if($header_printed == FALSE)
		printHeader();

	echo "$str\n";
	printFooter();
	die("");
}

function alnumex_valid($str, $max, $ex)
{
	$i = 0;
	while($i < $max && $str[$i] != NULL) {
		$c = $str[$i];
		if(!ctype_alnum($c))
		{
			$j = 0;
			while($ex[$j] != NULL) {
				if($c == $ex[$j])
					break;
				$j++;
			}
			if($ex[$j] == NULL)
				return FALSE;
		}
		$i++;
	}
	return TRUE;
}

function do_alnumex($in, $maxlen, $name, $ex = "")
{
	global $db_err;
	if($in == FALSE)
		return FALSE;
	$str = substr($in, 0, $maxlen);
	if($str != $in) {
		$db_err = "Invalid $name, keep it under $maxlen characters";
		return FALSE;
	}
	if(!alnumex_valid($str, $maxlen, $ex)) {
		$db_err = "Invalid $name. Contains at least one non-alpha-numeric character.";
		return FALSE;
	}

	return trim(stripslashes($str)); // strip whitespace from ends
}

//$point = htmlentities($_POST['Point']);
// check if it's valid UTF8 data
//if(iconv('UTF-8','UTF-8',$point) != $point) {
//	// no UTF-8
//	die("Point failed UTF-8 check.");
//}

function do_str($in, $maxlen, $name)
{
	if($in == FALSE)
		return FALSE;
	$str = substr($in, 0, $maxlen);
	if($str != $in)
		do_err("Invalid $name, keep it under $maxlen characters");

	return trim(stripslashes($str)); // strip whitespace from ends
}

/*
//
// taken from php.net docs on strip_tags
// credit to tREXX & Tony Freeman
//
function removeEvilAttributes($tagSource)
{
	$tagSource = stripslashes($tagSource);

	if($tagSource[0] == 'a ' || $tagSource[0] == 'A ')
	{
		// strip everything but 'href' attribute
	$len = strcspn($tagSource, ' ');
	}
//	echo "before: $tagSource<BR>\n";
//	$i = 0;
//	$out = "";
//	while($tagSource[$i] != NULL && $tagSource[$i] != ' ') {
//		$out[$i] = $tagSource[$i];
//		$i++;
//	}
//	echo "after: $tagSource<BR>\n";
	$len = strcspn($tagSource, ' ');
	return substr_replace($tagSource, "", $len);
}

function removeEvilTags($source)
{
	$allowedTags = '<h1><b><i><a><ul><li>';

	$source = strip_tags($source, $allowedTags);
	return preg_replace('/<(.*?)>/ie', "'<'.removeEvilAttributes('\\1').'>'",
						$source);
}

function myHtmlThing($str)
{
	global $max_word_len;

	//echo "b4MyHtmlThing: $str<BR>\n";
	//$str = preg_replace('/https?:\/\/([-\w\/\.#]*)/i',
	//					'<a href="http://\\1">[link]</a>', $str);
//	$str = preg_replace('/\[\[/ie', 'shit', $str);
	$str = preg_replace('/\[\[(.*)\|(.*)\]\]/e',
						"toLink('\\1','\\2')", $str);
	//$str = preg_replace('/https?:\/\/([-_()%\w\/\.#]*)/ie',
	//					"toLink('\\1')", $str);

//	preg_replace("/(<\/?)(\w+)([^>]*>)/e",
//	           "'\\1'.strtoupper('\\2').'\\3'",
//			              $html_body);
//	$str = preg_replace('/&lt;(a|A)(\s*)(href|HREF)=\\\&quot;(.*)\\\&quot;&gt;(.*)&lt;\/(a|A)&gt;/i',
//						'<a href="\\4">\\5</a>', $str);

	$str = preg_replace('/&lt;[bB]&gt;(.*)&lt;\/[bB]&gt;/', '<b>$1</b>', $str);
	$str = preg_replace('/&lt;[iI]&gt;(.*)&lt;\/(i|I)&gt;/', '<i>$1</i>',$str);
	$str = preg_replace('/&lt;[bB][rR]&gt;/', '<br>', $str);
	$str = trim($str);


	//echo "afterMyHtmlThing: $str<BR>\n";
	return trim($str); // strip whitespace from ends
}
*/


function spaceAllWords($str, $max)
{
	$out = "";
	$i = 0;
	$word = "";
	$wordlen = 0;
	while($str[$i] != NULL)
	{
		$word .= $str[$i];
		$wordlen++;

		if($str[$i] == '<')
		{
			$out .= $word;
			$wordlen = 0;
			$word = "";

			$i++;
			if($str[$i] == NULL)
				break;
			while($str[$i] != '>')
			{
				$out .= $str[$i];
				$i++;
				if($str[$i] == NULL)
					break;
			}
			continue;
		}

		if(ctype_space($str[$i]) || $wordlen >= $max)
		{
			if($wordlen >= $max)
				$word .= " ";

			$out .= $word;
			$wordlen = 0;
			$word = "";
		}
		$i++;
	}
	if($wordlen > 0)
		$out .= $word;

	return $out;
}

function my_stripos($haystack,$needle,$offset = 0)
{
	return(strpos(strtolower($haystack),strtolower($needle),$offset));
}

//
// <$tag>
//
function passTagSingle($str, $tag)
{
	global $tag_limit;
	$itag = 0;

	$len = strlen($tag);
	$offset = 0;

	while(42)
	{
		if($itag++ > $tag_limit) {
			do_log(2,"","overflowed max html tag limit with \"$tag\"");
			break;
		}
		//$start = stripos($str, "&lt;$tag&gt;", $offset);
		$start = my_stripos($str, "&lt;$tag&gt;", $offset);
		if($start === FALSE)
			break;
		$str = substr_replace($str, "<$tag/>", $start, $len+8);
		$offset = $start+$len+3; // $str is being modified as we go
	}
	return $str;
}

//
// <$tag>...</$tag>
//
function passTag($str, $tag)
{
	global $tag_limit;
	$itag = 0;

	$len = strlen($tag);
	$offset = 0;
	while(42)
	{
		if($itag++ > $tag_limit) {
			do_log(2,"","overflowed max html tag limit with \"$tag\"");
			break;
		}
		//$start = stripos($str, "&lt;$tag&gt;", $offset);
		$start = my_stripos($str, "&lt;$tag&gt;", $offset);
		if($start === FALSE)
			break;
		//$end = stripos($str, "&lt;/$tag&gt;", $start);
		$end = my_stripos($str, "&lt;/$tag&gt;", $start);
		if($end === FALSE)
			break;

		$str = substr_replace($str, "<$tag>", $start, $len+8);
		$end -= 6; // strlen("&lt;$tag&gt;") - strlen("<$tag>")
		$str = substr_replace($str, "</$tag>", $end, $len+9);
		$offset = $start+$len+2; // $str is being modified as we go
	}
	return $str;
}

function toLink($title, $address)
{
	global $max_domain_len;

	$address = trim($address);
	$len = strlen($address);
	if($len < 1)
		return "<b>baad f00d</b>";

//	$c = $address[strlen($address)-1];
//	if(!ctype_alnum($c) && $c != '(' && $c != ')' && $c != '_' && $c != '-')
//		$address = substr($address, 0, $len-1);

	$domain = preg_replace('/https?:\/\//i', '', $address);

	// select only the domain name portion of the address
	if(strstr($domain, '/'))
		$domain = substr($domain, 0, strpos($domain, '/'));


	// only keep the domain name and top level domain (tld)
	$a = explode(".", $domain);
	if($a[0] == FALSE || $a[1] == FALSE)
		return "<b>baad f00d</b>";
	$tld = array_pop($a);
	$domain = array_pop($a) . "." . $tld;

	// cut off domain name if it's too long
	$domain_cut = substr($domain, 0, $max_domain_len);
	if(strlen($domain) != strlen($domain_cut))
		$domain_cut .= "...";

	//$link = "<a href=\"http://$address\">$title</a>";
	//$link = "<a href=\"$address\">$title</a> ($domain_cut)";
	$link = "<a href=\"$address\">$title</a>";

	//if(!ctype_alnum($c))
	//	$link .= $c;

	return $link;
}

function passLinks($str)
{
	global $tag_limit;
	$itag = 0;

	$offset = 0;

	while(42)
	{
		if($itag++ > $tag_limit) {
			do_log(2,"","overflowed max html tag limit with an anchor");
			break;
		}
		$start = strpos($str, "[[", $offset);
		if($start === FALSE)
			break;
		$mid = strpos($str, "|", $start+2);
		if($mid === FALSE)
			break;
		$end = strpos($str, "]]", $mid+1);
		if($end === FALSE)
			break;

		//echo "start:$start mid:$mid end:$end<BR>\n";

		// TODO: do some validations on $start, $mid, and $end

		$title = substr($str, $start+2, $mid - ($start+2));
		$url = substr($str, $mid+1, $end - ($mid+1));
		$link = toLink($title, $url);

		$str = substr_replace($str, $link, $start, $end - $start + 2);
		//
		// I assume that the html encoded link is longer in length..
		//
		$offset = $start + strlen($link); // $str is being modified as we go
	}
	return $str;
}

function do_html($in, $maxlen, $name)
{
	global $max_word_len;

	if($in == FALSE)
		return FALSE;
	$str = substr($in, 0, $maxlen);
	if($str != $in)
		do_err("Invalid $name, keep it under $maxlen characters");

	$str = str_replace('<!DOCTYPE', '<DOCTYPE', $str);
	$str = trim(stripslashes($str));

	//$str = removeEvilTags($str); //strip_tags($str);
	$str = htmlentities($str, ENT_QUOTES);
	$str = passTag($str, "b");
	$str = passTag($str, "i");
	$str = passTagSingle($str, "br");
	$str = passLinks($str);

	$str = spaceAllWords($str, $max_word_len);
	return trim($str);
}

function do_no_html($in, $maxlen, $name)
{
	if($in == FALSE)
		return FALSE;
	$str = substr($in, 0, $maxlen);
	if($str != $in)
		do_err("Invalid $name, keep it under $maxlen characters");

	$str = str_replace('<!DOCTYPE', '<DOCTYPE', $str);
	$str = trim(stripslashes($str));

	return htmlentities($str, ENT_QUOTES);
}

function do_num($in, $name)
{
	if($in == FALSE || $in == "")
		return 0;
//	$str = substr($in, 0, $maxlen);
//	if($str != $in)
//		do_err("Invalid $name, keep it under $maxlen characters");

	//echo "$name == $in\n<BR>";

	for($i = 0; $i < strlen($in); $i++)
	{
		if($in[$i] < '0' || $in[$i] > '9')
		{
			if($in[$i] != '.' && $in[$i] != '-')
				do_err("Invalid $name, this is a numeric parameter only");

			if($i > 0)
			{
				if($in[$i] == '.' && $in[$i-1] == '.')
					do_err("Invalid $name, this is a numeric parameter only");
				if($in[$i] == '-' && $in[$i-1] == '-')
					do_err("Invalid $name, this is a numeric parameter only");
			}
		}
	}

//	if(strcspn($in, "0123456789") != 0)
//		do_err("Invalid $name, this is a numeric parameter only");

	return $in;
}

function do_bool($in, $name)
{
	if($in == FALSE)
		return FALSE;
	if($in != 0 && $in != 1)
		do_err("Invalid $name, this is a boolean parameter only");
	return $in == 1 ? TRUE : FALSE;
}

function validUser($userId)
{
	global $anonId;
	global $consensusId;

	if($userId == $anonId)
		return FALSE;
	if($userId == $consensusId)
		return FALSE;
	return TRUE;
}

define_syslog_variables();
openlog("debatepoint", LOG_ODELAY, LOG_USER);

$loginPass = do_alnumex($_POST['loginPass'], $max_login_pass, "password", "-_");
$loginName = do_alnumex($_POST['loginName'], $max_login_name,"login name","-_");
$loginCookie = do_str($_COOKIE['DEBATE_POINT_SESSION'], $max_login_cookie, "cookie",":");


if(isset($_POST['logout']))
{
	$session_do_logout = $_POST['logout'];
	if($session_do_logout != TRUE)
		$session_do_logout = FALSE;
}

// connect to mysql and pick database
$db_link = openDatabase();
if($db_link == FALSE) 
	do_err("Failed to make connection to database. hello slashdot?");

$sessRow = sessionStart($loginName, $loginPass, $loginCookie);
if(!isset($sessRow))
	do_err("Failed to start session.");
$sessUserRow = getUser($sessRow['userId']);
if($sessUserRow == FALSE)
	do_err("Failed to retrieve user from session");
$sessUserId = $sessUserRow['userId'];
$sessUserName = $sessUserRow['login'];

// add 1 to table 'sys' for our web hit counter
if($sessUserId == $anonId)
	countAnonHit();
else
	countUserHit();

$bSessModified = FALSE; // whether to save session row in debatefooter

?>
