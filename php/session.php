<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("database.php");


function getUniqueId()
{
	$salt = "abchefghjkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	srand((double)microtime()*1000000);

	$id = "";
	$i = 0;
	for($i = 0; $i < 24; $i++)
		$id .= $salt[rand() % 62];

	return $id;
}

function createNewSession($userId, $remoteAddr)
{
	global $session_timeout; // timeout in minutes
	global $cookie_domain;
	global $anonId;

	// how many seconds do we want the session to stay active after idle
	if($userId == $anonId)
		$sessionSeconds = 60 * 48; // two days
	else
		$sessionSeconds = $session_timeout * 60;

	$sqldate = date("Y-m-d H:i:s", time() + $sessionSeconds);

	$id = getUniqueId();
	$cookieThing = "$userId=$id";

	// TODO: use $cookie_domain.  it breaks when "localhost" though
	setcookie("DEBATE_POINT_SESSION", $cookieThing, time()+$sessionSeconds);
//			"/", $cookie_domain);

	$query = "insert into sessions (userId,ipaddr,cookie,expires) values ($userId,'$remoteAddr','$cookieThing','$sqldate')";

	$query_result = mysql_query($query)
		or do_err("If this happens then I am wrong. :(");

	$sessionId = mysql_insert_id();
	if($sessionId == 0)
		do_err("session insertion failed to retrieve auto_increment id");

	$query = "select * from sessions where sessionId='$sessionId'";
	$query_result = mysql_query($query)
		or do_err("Failed to retrieve newly created session");
	$sessionRow = mysql_fetch_assoc($query_result);
	mysql_free_result($query_result);
	return $sessionRow;
}

function deleteOldSessions($userId, $remoteAddr)
{
	$query = "delete from sessions where ipaddr='$remoteAddr' && userId='$userId'";

	$query_handle = mysql_query($query)
		or do_err("delete old session failed");
}

function findPreviousSession($remoteAddr, $cookieThing)
{
	global $session_timeout;
	global $session_do_timeout;

	if($cookieThing == FALSE)
		return FALSE;

	// TODO: pull userId from beginning of cookie, and use it to
	// narrow this query
	$query = "select * from sessions where cookie='$cookieThing'";
	$query_result = mysql_query($query)
		or do_err("find previous session query failed");

	$numRows = mysql_num_rows($query_result);
	if($numRows < 1)
		return FALSE;

	$rtrn = FALSE;
	$refreshed = FALSE;

	while($numRows--)
	{	
		$sessionRow = mysql_fetch_assoc($query_result);
		$sessionId = $sessionRow['sessionId'];
		$sqldate = $sessionRow['expires'];

		list($year,$month,$day,$hour,$minute,$second) =
			sscanf($sqldate,"%d-%d-%d %d:%d:%d");

		$expireTime = mktime($hour,$minute,$second,$month,$day,$year);
		$currentTime = time();

		if($refreshed || $currentTime > $expireTime)
		{
			$session_do_timeout = TRUE;

			// delete this entry cuz its OlD;
			$query = "delete from sessions where sessionId='$sessionRow[sessionId]'";
			mysql_query($query)
				or do_err("couldn't delete row from sessions");
		}
		else
		{
			$sqldate = date("Y-m-d H:i:s", time() + ($session_timeout * 60));

			$query = "update sessions set expires='$sqldate' where sessionId='$sessionRow[sessionId]'";
			if(mysql_query($query) == FALSE)
				do_err("failed to update session expiration time");

			$rtrn = $sessionRow; 
			$refreshed = TRUE;
		}
	}
	mysql_free_result($query_result);

	if($rtrn != FALSE)
		$session_do_timeout = FALSE;

	return $rtrn;
}

function authLogin($loginName, $loginPass)
{
	if($loginPass == "")
		do_err("NULL passwords not allowed");

	$login_esc = db_escape($loginName);
	$pass_esc = db_escape($loginPass);
	$query = "SELECT * from user where login='$login_esc' && pass=PASSWORD('$pass_esc')";
	$query_result = mysql_query($query)
		or do_err("Query on database failed: ");

	$num_of_rows = mysql_num_rows($query_result)
		or do_err("Sorry, you are not in our database, please create an account:");

	$compRow = mysql_fetch_assoc($query_result);

	mysql_free_result($query_result);

	return $compRow;
}

function sessionStart($loginName, $loginPass, $loginCookie)
{
	// retrieve any cgi data
	$REMOTE_ADDR = getenv("REMOTE_ADDR");
	$PHP_AUTH_USER = getenv("PHP_AUTH_USER");

	if($loginName != FALSE)
	{
		// check the form data 
		$userRow = authLogin($loginName, $loginPass);
		if($userRow == FALSE)
			do_err("Failed to authenticate '$loginName'");
		$userId = $userRow['userId'];

		//do_err("$loginName, $loginPass, $loginCookie");

		deleteOldSessions($userId, $REMOTE_ADDR);
		$sessionRow = createNewSession($userId, $REMOTE_ADDR);
		if($sessionRow == FALSE)
			do_err("Failed to add new session");

		//do_err("$sessionRow[userId]");
	}
	else
	{
		$sessionRow = findPreviousSession($REMOTE_ADDR, $loginCookie);
		if($sessionRow == FALSE)
		{
			$loginName = "anonymous";
			$loginPass = "anonymous";

			// as a last resort, pop up a dialog and prompt for user/pass
			//if(!isset($PHP_AUTH_USER))
			//{
			//	Header("WWW-Authenticate: Basic realm=\"debatepoint\"");
			//	Header("HTTP/1.0 401 Unauthorized");
			//	do_err("Sorry, not authorized");
			//}
			//$userRow = authLogin($PHP_AUTH_USER, $PHP_AUTH_PW);
			$userRow = authLogin($loginName, $loginPass);

			$sessionRow = createNewSession($userRow['userId'], $REMOTE_ADDR);
		}      
	}
	return $sessionRow;
}

function saveSession()
{
	global $sessRow;
	global $bSessModified;
	
	//echo "bSessModified: $bSessModified\n";
	if(!$bSessModified)
		return; // no need to update

	$query = "update sessions set mode='$sessRow[mode]',modArg1='$sessRow[modArg1]',modArg2='$sessRow[modArg2]',sort_method='$sessRow[sort_method]',sort_reverse='$sessRow[sort_reverse]',sort_filter='$sessRow[sort_filter]' where sessionId='$sessRow[sessionId]'";
	if(mysql_query($query) == FALSE)
		do_err("error updating session info");
	return TRUE;
}

?>
