<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");

$uid = do_num($_GET['uid'], "user id");

printHeader();

if($uid == 0)
	$uid = $sessUserId;

if($uid != $anonId)
{
	$userRow = getUser($uid);
	if($userRow == FALSE)
		do_err("Unknown user: $uid");

//	echo "<form action=\"newuser.php?uid=$uid\" method=\"post\">\n";
//	printUserInfo($userRow);
//	echo "</form>\n";
	printUserArgs($userRow, $arg_list_max, 0);

/*
	$query_handle = queryAllDebates();
	$debateRow = queryNextRow($query_handle);

	echo "<div id=\"debate_list\">\n";
	echo "<ul>\n";

	while($debateRow != FALSE)
	{
		echo "<a href=\"debate.php?id=$debateRow[debateId]\">\n";
		echo "<li>$debateRow[title]</li></a>\n";
		$debateRow = queryNextRow($query_handle);
	}
	echo "</ul>\n";
	echo "</div> <!-- debate_list -->\n\n";

	queryFree($query_handle);
	*/
}


printFooter();

require("debatefooter.php");

?>
