<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");

printHeader();

echo "<div id=\"comments_header\">\n";

echo "latest news<BR>\n"; //<hr>\n";
if($sessUserId == 1000001) {
	echo "<a href=\"newargument.php?aid=$newsArgId&stance=2\">";
	echo "[make comment]</a> &middot; sort by: [todo]\n";
}
echo "</div> <!-- comments_header -->\n";
echo "<div id=\"comments\">\n";

echo "<div id=\"news\">\n";
// print one level of threaded comments with descending date
printArgumentThread(0, $newsArgId, 2, 1, 3, 0, 0.0, ARG_NONE);
echo "</div> <!-- news -->\n";

echo "</div> <!-- comments -->\n";

printFooter();

require("debatefooter.php");

?>
