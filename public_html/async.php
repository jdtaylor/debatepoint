<?

require("debateheader.php");

$aid = do_num($_GET['aid'], "argument id");

if(isset($_GET('op'])
{
	$op = do_alnumex($_GET['op'], "op", "_-");

	switch($op)
	{
		case 'mod':
			if(!validUser($sessUserId))
				break;
			// moderating an argument
			$score = do_bool($_GET['mod'], "mod");
			if($score == FALSE)
				$score = -1;
			else
				$score = 1;

			if(newModerationSingle($sessUserId, $aid, $score) == FALSE)
				echo "<b>Moderation failed.</b><BR />\n";

			$argRow = getArgument($aid);
			$stanceScore = round($argRow['stanceEffective'] * 100);

			#
			# TODO: the above moderation might change any parent arguments.
			# We need to send those to the client as well.  I think we should
			# always send all parents, plus the stance data.
			# 
			echo "$aid,$stanceScore";
			break;
		case 'get_recent_points':
			break;
	}
}

require("debatefooter.php");

?>
