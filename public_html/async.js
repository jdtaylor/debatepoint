

function getHTTPObject()
{
	var xmlhttp;

/*@cc_on
@if (@_jscript_version >= 5)
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}
@else
	xmlhttp = false;
@end @*/

	if(!xmlhttp && typeof XMLHttpRequest != 'undefined')
	{
	//	try
			xmlhttp = new XMLHttpRequest();
	//	catch(e)
	//		xmlhttp = false;
	}
	return xmlhttp;
}

function handleHttpResponse()
{
	if(http.readyState == 4)
	{
		if(http.responseText.indexOf('invalid') == -1)
		{
			// Split the comma delimited response into an array
			results = http.responseText.split(",");
			e = document.getElementById('in-stance' + results[0]);

			// delete all child nodes
			while(e.hasChildNodes())
				e.removeChild(e.firstChild);

			// append new moderation score
			e.appendChild(document.createTextNode(results[1] + '%'));

			isWorking = false;
		}
	}
}

var isWorking = false;

function moderate(aid, score)
{
	if(!isWorking && http)
	{
		//var zipValue = document.getElementById("arg" + aid).value;
		// + escape(zipValue), true);
		http.open("GET", "async.php?aid=" + aid + "&op=mod&mod=" + score, true);
		http.onreadystatechange = handleHttpResponse;
		isWorking = true;
		http.send(null);
	}
}


var http = getHTTPObject(); // We create the HTTP Object



