<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");

$error = "";

// stance this argument belongs to
$sid = do_num($_GET['sid'], "stance id"); 
 // parent argument to the new one
$aid = do_num($_GET['aid'], "argument id");

$stance = do_num($_GET['stance'], "stance");
if($stance == FALSE)
	$stance = do_num($_POST['stance'], "stance");
if($stance < 0 || $stance > 2)
	do_err("Invalid stance value");

$str = do_alnumex($_POST['op'], 7, "submit operation");
if($str != "Preview")
	$bPreview = FALSE; // default to preview
else
	$bPreview = TRUE;

//
// the _filt variables are so I don't overwrite
// the textarea contents with the htmlentity output
// in case of an error.
//
$title = do_html($_POST['title'], $max_arg_title, "title");
if($title == FALSE) {
	$title = "";
	$bPreview = FALSE;
}
// completely escaped html (no tags retained)
$title_esc = do_no_html($_POST['title'], $max_arg_title, "title");

$argument = do_html($_POST['argument'], $max_arg_body, "argument");
//echo "argument: $argument<BR>\n";
$argument_esc = do_no_html($_POST['argument'], $max_arg_body, "argument");
//echo "argument_esc: $argument_esc<BR>\n";

$tags = do_alnumex($_POST['tags'], $max_tags_len, "tags", "-_ ");
if($_POST['tags'] != "" && $tags == FALSE) {
	$bPreview = TRUE;
	$error = $db_err; // set by do_alnumex
	$tags_esc = do_no_html($_POST['tags'], $max_tags_len, "tags");
}
else
	$tags_esc = $tags;

$argumentRow = FALSE;
if($aid != 0) {
	$argumentRow = getArgument($aid);
	if($argumentRow == FALSE)
		do_err("Unknown parent argument: $aid");
	// override sid with argument's stance id.  is this correct behaviour?
	$sid = $argumentRow['stanceId'];
}

if($aid == $newsArgId && $sessUserId != 1000001)
	do_err("not authorized to reply to this argument");

if(!$bPreview && $argument != "" && $title != "")
{
	newArgument($sessUserId, $sid, $aid, $stance, $title, $argument, $tags, 0);

	if($header_printed)
	{
		echo "<div class=\"new_argument\">\n";
		echo "your point has been taken.  thanks.\n";
		echo "<a href=\"debate.php?sid=$sid&aid=$aid\">go debate new point</a>\n";
		echo "</div> <!-- new_argument -->\n";
	}
	else
	{
		// redirect user directly to the new arguments debate
		header("Location: debate.php?sid=$sid&aid=$aid");
		exit;
	}
}

printHeader();

{
	$previewRow = FALSE;
	if($bPreview) {
		$previewRow = formArgument($sessUserId, $sid, $aid, $stance,
									$title, $argument);
	}
	// don't let users reply
	$optArg = ARG_DEFAULT & ~(ARG_SCORE | ARG_REPLY);

	if($argumentRow != FALSE)
	{
		echo "<ul><li>\n";
		printArgument($argumentRow, $optArg);
		if($bPreview != FALSE) {
			echo "<ul><li>\n";
			printArgument($previewRow, $optArg);
			echo "</li></ul>\n";
		}
		echo "</li></ul>\n";

		// prepend "re: " to the parent argument's title
		if($title_esc == "")
		{
			if(strncmp($argumentRow['title'], "Re:", 3) != 0)
				$title_esc = "Re: ";
			$title_esc .= $argumentRow['title'];
		}
	}
	else if($bPreview != FALSE) {
		echo "<ul><li>\n";
		printArgument($previewRow, $optArg);
		echo "</li></ul>\n";
	}

	echo "<div id=\"new_argument\">\n";

	if($error != "")
		echo "<b>Error:</b> $error<BR><BR>\n";

	echo "<b>";
	switch($stance)
	{
		case 0: echo "enter new opposing argument:\n"; break;
		case 1: echo "enter new supporting argument:\n"; break;
		case 2: echo "enter new comment:\n"; break;
	}
	if($title_esc == "" && $argument_esc == "")
		echo "";//A title and argument are required.<BR>\n";
	else if($title_esc == "")
		echo "A title is required.<BR>\n";
	else if($argument == "")
		echo "An argument is required.<BR>\n";
	echo "</b> (see below for help)<BR>\n";

	echo "<form action=\"newargument.php?sid=$sid&aid=$aid&stance=$stance\" method=post>\n";
//	echo "<select name=\"stance\" value=\"$stance\">\n";
//	echo "<option value=\"0\">opposing</option>\n";
//	echo "<option value=\"1\">supportive</option>\n";
//	echo "</select>\n";
	echo "<input type=\"text\" name=\"title\" value=\"$title_esc\" maxlength=$max_arg_title /> : title<BR>\n";
	echo " argument:<BR>\n";
	echo "<textarea wrap=\"none\" rows=\"10\" cols=\"50\" maxlength=$max_arg_body name=\"argument\" bgcolor=#EEEEEE>$argument_esc</textarea><BR>\n";
	echo "<input type=\"text\" name=\"tags\" value=\"$tags_esc\" maxlength=$max_tags_len /> : tags (space seperated; multi-word tags should use underscores)<BR><BR>\n";
	echo "<input type=\"SUBMIT\" name=\"op\" value=\"Submit\" />\n";
	echo "<input type=\"SUBMIT\" name=\"op\" value=\"Preview\" />\n";
	echo "</form>\n";

	echo "<BR>\n";

	echo "<b>The maximum</b> length of a title is $max_arg_title characters.<BR>\n";
	echo "The max length of the body is <b>only</b> $max_arg_body characters!<BR>\n";
	echo "So be brief and stay to the point.<BR>\n";
	echo "If you need more than the limit, then you should probably be including a link to a more authoritative source instead.  Feel free to <a href=\"newpoint.php\">make a new debatepoint</a> about it if you think the limit should be increased.<BR>\n";
	echo "<b>Supported HTML tags:</b><BR>\n";
	echo "<ul>\n";
	echo "<li>&lt;b&gt;<b>bold</b>&lt;/b&gt;</li>\n";
	echo "<li>&lt;i&gt;<i>italic</i>&lt;/i&gt;</li>\n";
	echo "<li>&lt;br&gt; -- line break</li>\n";
	echo "<li>for links; [[title|URL]] ie: [[ google | http://www.google.com ]]</li>\n";
	//echo "<li>&lt;a href=\"http://debatepoint.com\"&gt;<a href=\"http://debatepoint.com\">anchors</a>&lt;/a&gt;</li>\n";
	echo "<li>every other tag will be translated into text.</li>\n";
	echo "</ul>\n";
	echo "<BR>\n";
	printCopyrightNotice();
	echo "</div> <!-- new_argument -->\n";
}

printFooter();

require("debatefooter.php");

?>
