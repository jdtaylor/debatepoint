<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");

$login = do_alnumex($_POST['login'], $max_login_name, "login name", "-_");
$pass = do_alnumex($_POST['pass'], $max_login_pass, "password", "-_");
$email = do_alnumex($_POST['email'], $max_login_email, "email", "-_@");

printHeader();

if($login != FALSE && $pass != FALSE)
{
	newUser($login, $pass, $email);

	echo "<div id=\"login_container\">\n";
	echo "user $login created..\n";
	printLogin();
	echo "</div> <!-- login_container -->\n";
}
else
{
	echo "<div id=\"new_user_container\">\n";
	echo "<div id=\"new_user\">\n";
	echo "<form action=\"newuser.php\" method=post>\n";
	/*
	echo "<select name=\"stance\">\n";
	echo "<option value=\"2\">strongly supports</option>\n";
	echo "<option value=\"1\">somewhat supports</option>\n";
	echo "<option value=\"0\">not relevant</option>\n";
	echo "<option value=\"-1\">somewhat opposes</option>\n";
	echo "<option value=\"-2\">strongly opposes</option>\n";
	echo "</select>\n";
	*/
	echo "<input type=text name=\"login\">\n";
	echo ": new login name<br>\n";
	echo "<input type=password name=\"pass\">\n";
	echo ": new pass<br>\n";
	echo "<input type=text name=\"email\">\n";
	echo ": email (not required <b>*</b>)<br>\n";
	echo "<input type=\"SUBMIT\" value=\"Submit\">\n";
	echo "</form>\n";
	echo "<b>*</b> an email address is only required if you plan on forgetting your password.  I can't remind you of your password unless I have a way to contact you.  NOTE: both your email and password are stored encrypted on the server with my GPG public key.  Your info \"should\" be secure even in the event of a break-in... until someone decides to own my personal machines, that is.<br>\n";
	echo "</div> <!-- new_user -->\n";
	echo "</div> <!-- new_user_container -->\n";
}


printFooter();

require("debatefooter.php");

?>
