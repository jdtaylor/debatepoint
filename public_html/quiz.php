<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");

$qid = do_num($_GET['qid'], "quiz id");

printHeader();


if($qid != FALSE)
{

// select each candidate name in the quiz
"select user.login from user inner join quiz_user on user.userId=quiz_user.userId && quizId=$qid order by userId";

	$pointRow = queryQuizPoints($pid);

	while($pointRow != FALSE)
	{
		$pointId = $pointRow[pointId];

		// get row for each stance of candidate
		"select stance from stance inner join quiz_user on userId where quizId=$qid && pointId=$pointId order by userId"
	}

	echo "<div class=\"point_header\">\n";
	echo "</div> <!-- point_header -->\n";
	echo "<div class=\"point_body\">\n";
	echo "$pointRow[text]\n";
	echo "</div> <!-- point_body -->\n";
	echo "<div class=\"point_footerr\">\n";
	echo "<a href=\"newpoint.php?pid=$pid\">[reply to this point]</a>\n";
	echo "</div> <!-- point_footer -->\n";

	doArgumentRecursion($pid, NULL);
}
else
{
	$query_handle = queryAllQuizes();
	$quizRow = queryNextRow($query_handle);

	echo "<div class=\"quiz_list\">\n";
	echo "<ul>\n";

	while($quizRow != FALSE)
	{
		echo "<a href=\"quiz.php?pid=$quizRow[quizId]\">\n";
		echo "<li>$quizRow[title]</li></a>\n";
		$quizRow = queryNextRow($query_handle);
	}
	echo "</ul>\n";
	echo "</div> <!-- quiz_list -->\n\n";

	queryFree($query_handle);
}


printFooter();

require("debatefooter.php");

?>
