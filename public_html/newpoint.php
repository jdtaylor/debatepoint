<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");

$error = "";

$pid = do_num($_GET['pid'], "point id");
$aid = do_num($_GET['aid'], "argument id");

// only consensus points for now
//$pointUserId = do_num($_POST['pointUserId'], "user id");
$pointUserId = $consensusId;

//
// TODO: need more checks on pointText.. do_no_html?
//
$pointText = do_str($_POST['pointText'], $max_point_body, "point");
$pointText_html = do_html($_POST['pointText'],$max_point_body,"point");
$pointText_esc = do_no_html($_POST['pointText'],$max_point_body,"point");
$topicId = do_num($_POST['topicId'], "topic id");
$tags = do_alnumex($_POST['tags'], $max_tags_len, "tags", "-_ ");
if($_POST['tags'] != "" && $tags == FALSE)
	$error = $db_err;


printHeader();

if(!validUser($sessUserId))
{
	echo "<BR/>";
	echo "&nbsp;Only logged in users may add new debatepoints<BR/>\n";
	echo "<div id=\"login_container\">\n";
	printLogin();
	echo "</div> <!-- login_container -->\n";
	echo "<br>";
}
else
{
	if($pointUserId != "" && $pointText_html != "" && $error == "")
	{
		$pointId = newPoint($sessUserId, $pointText_html, $topicId, $tags);
		$stanceId = newStance($pointId, $pointUserId);

		echo "<div class=\"new_point\">\n";
		echo "your point has been taken.  thanks.<BR/>\n";
		echo "<a href=\"debate.php?sid=$stanceId\">go debate new point</a>\n";
		echo "</div> <!-- new_point -->\n";
	}
	else
	{
		echo "<BR/><BR/>\n";

		echo "<div id=\"new_point\">\n";

		if($error != "")
			echo "<b>Error:</b> $error<br>\n";

		echo "<form action=\"newpoint.php?pid=$pid&aid=$aid\" method=\"post\">\n";
//		echo "<select name=\"pointUserId\">\n";
//		echo "<option value=\"$consensusId\">consensus</option>\n";
//		echo "<option value=\"$sessUserId\">$sessUserName</option>\n";
//		echo "</select>\n";
		echo "enter new point:<BR/>\n";
		echo "<textarea wrap=\"virtual\" name=\"pointText\" rows=\"2\" cols=\"64\" maxlength=$max_point_body></textarea><br>\n";
		echo "<input type=\"text\" name=\"tags\" maxlength=$max_tags_len /> : tags (space seperated)<BR/>\n";
		echo "<br><input type=submit value=\"Submit Point\">\n";
		echo "</form>\n";
		echo "<BR/><BR/>\n";
		echo "The max length of a point is $max_point_body characters.\n";
		echo "<BR/><BR/>\n";
		echo "<b>Supported HTML tags:</b><BR/>\n";
		echo "<ul>\n";
		echo "<li>&lt;b&gt;<b>bold</b>&lt;/b&gt;</li>\n";
		echo "<li>&lt;i&gt;<i>italic</i>&lt;/i&gt;</li>\n";
		echo "<li>for links; [[title|URL]] ie: [[ google | http://www.google.com ]]</li>\n";
		//echo "<li>&lt;a href=\"http://debatepoint.com\"&gt;<a href=\"http://debatepoint.com\">anchors</a>&lt;/a&gt;</li>\n";
		echo "<li>every other tag will be translated into text.</li>\n";
		echo "</ul>\n";
		echo "<BR/>\n";
		echo "Please limit the point to one sentence.  Try to extract ";
		echo "the point of your point, and then express it as briefly as ";
		echo "possible.  If you need more than one sentence, then consider ";
		echo "moving some of it into an initial support or oppose argument.";
		echo "<BR/><BR/>\n";
		echo "Please phrase the point in an assertive way because the validity of the point is what is being argued over.  \n";
		echo "This might be changed in the future to allow for yes/no debates.  \n";
		echo "<BR/><BR/>\n";
		echo "<b>Examples of bad debate points:</b>\n";
		echo "<ul>\n";
		//echo "<li>Is George Bush a lemming?</li>\n";
		//echo "<ul><li>use the more assertive 'George Bush is a lemming'</li></ul>\n";
		echo "<li>What is the capital of Belgium?</li>\n";
		echo "<ul><li>use 'the capital of Belgium is Brussels'</li></ul>\n";
		echo "</ul>\n";
		echo "<BR/>\n";
		printCopyrightNotice();
		echo "<BR/>\n";
		echo "</div> <!-- new_point -->\n";
	}
}

printFooter();

require("debatefooter.php");

?>
