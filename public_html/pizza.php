<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");


// retrive any cgi data
$nPizza = do_num($_POST['nPizzas'], "nPizzas");
$dough_type = do_alnumex($_POST['dough_type'], 1024, "dough_type", " -&");

echo "nPizza:$nPizza type:$dough_type<BR>\n";

if($nPizza == FALSE)
	$nPizza = 2;

printHeader();

	$fs = 1.6; // flour scale

	$g_notes = array(
				"All Purpose" =>
					  "This is a basic dough which, as the
					  name implies, can be used for a wide variety of
					  pizzas. It also can serve as a starting point
					  for custom recipe development.",
				"Fast Rising" =>
					  "When there's only a short
					  time from mixer to pizza-making, use this
					  formula for maximum rise. Adjust water
					  temperature so dough comes from the mixer at
					  about 90 degrees F. If necessary, increase the
					  portion of instant yeast. You may also want to
					  add a dough relaxer (i.e., reducing agent) such
					  as L-cysteine to make the dough easier to mold.
					  Follow product directions.",
				"Easy-to-sheeet" =>
					  "There are various dough relaxers
					  (conditioners). The most common types contain
					  L-cysteine. With it, gluten is relaxed for a
					  short time after mixing but regains strength
					  during fermentation. Follow product directions.
					  We also specify a medium protein flour in the 10
					  to 11.5 percent range, often known as H&R or
					  hotel and restaurant flour. If you don't
					  want cornmeal, substitute flour for it and
					  increase the water portion 1 to 2 percent.",
				"High-browning" =>
					  "Want to see a lot of brown? This
					  will do it. But take care that the inside gets
					  done, too. There may be more browning than you
					  need. Also, you may have to lower the baking
					  temperature and increase the baking time.",
				"Lean and Chewy" => "Self-explanatory",
				"Rich and Tender" =>
					  "We're specifying a medium
					  protein flour in the 10 to 11.5 percent range,
					  often known as H&R or hotel and restaurant
					  flour. If that makes the dough too soft, you
					  might substitute high gluten flour, but increase
					  the water portion by 2 to 3 percent of flour
					  weight.",
				"Thin and Crackery" =>
					  "This formula is used with the bulk
					  dough method. Let it relax for 3 to 4 hours, or
					  refrigerate overnight, before rolling. Due to
					  its extreme stiffness the batch size for this
					  recipe has been reduced. If using a planetary
					  mixer, mix on low speed for 3 to 5 minutes. A
					  dough roller is needed for rolling. The dough
					  relaxer portion can be increased up to 3
					  percent, but follow product directions. To avoid
					  bubbling, this dough must usually be perforated
					  with a docker after rolling.",
				"Chicago Deep-dish" =>
					  "Every pan pizza in Chicago has a
					  different recipe, but this one is commonly
					  associated with Chicago-style deep dish pizza.
					  We're specifying a medium protein flour
					  in the 10 to 11.5 percent range, often known as
					  H&R or hotel and restaurant flour. You might
					  substitute high gluten, but increase water 2 to
					  3 percent.",
				"Basic Neapolitan" =>
					  "This is a basic lean dough
					  that's used for thin crust pizzas and is
					  often associated with Neapolitan and New York
					  style pizza.",
				"Basic Sicilian" =>
					  "This is a basic crust, richer than
					  Neapolitan, and is often associated with thicker
					  pizzas such as pan pizza. For a tenderer version
					  substitute medium protein (H&R) flour in the 10
					  to 11.5 percent range -- and decrease the
					  water portion by 2 to 3 percent of flour
					  weight.",
				"Whole Wheat" =>
					  "For the white flour you can use a
					  high protein clear grade of flour and, thereby,
					  possibly save a little money. If possible, use
					  whole wheat flour that's milled from hard
					  white wheat, as it produces a lighter color and
					  less bitter flavor than other types of whole
					  wheat flour milled from traditional red wheat.
					  This recipe contains 50 percent whole wheat
					  flour. You can also do it with 100 percent whole
					  wheat, but decrease the water portion by 2 to 3
					  percent of flour weight.",
				"Three-grain" =>
					  "The three grain mix can be most
					  anything, but for starters we suggest 1/3 whole
					  wheat flour, 1/3 yellow cornmeal, and 1/3 ground
					  oats.",
				"Higher-protein" =>
					  "Soy flour, being high in usable
					  protein, makes this a higher protein crust.
					  Adding 2 to 3 percent non-fat dry milk and/or
					  eggs would increase it even more.",
				"Herb & Wine" =>
					  "Roughly speaking, 1/4 oz garlic
					  powder equals two tea�spoons. For flavor
					  retention, mix the garlic and herbs with the
					  oil, shake well, and let stand overnight.
					  Otherwise, mix them with the salt and sugar.
					  Fresh garlic may be substituted for dehydrated.
					  Virtually any leafy herb can be tried. (Tarragon
					  is elegant, but sometimes expensive.) Add more
					  herb for more flavor. For more wine flavor,
					  substitute a portion of water with wine.",
				"Pepper-Cheese" =>
					  "We suggest grated Parmesan or Romano
					  cheese; however, other hard cheeses, such as
					  sharp cheddar, will work too. Mix the pepper
					  with the salt and sugar. Put the cheese on top
					  of the flour and stir it in. A cheese powder can
					  be used in place of grated cheese.",
				"Beer" =>
					  "Three pounds of beer equals four
					  12-ounce cans; one and a half pounds equals two
					  cans. If you want more beer flavor, substitute a
					  portion of the water for more beer. If you want
					  to avoid alcohol, use non-alcoholic beer (i.e.,
					  malt beverage). Check out various brands as it
					  comes in many differing flavor strengths.",
				"Garlic & Butter" => "",
				"Italian Accent" =>
					  "Roughly speaking, 1/4 oz garlic
					  powder equals two teaspoons, and 1/8 oz equals
					  one teaspoon. Mix the garlic powder with the
					  salt and sugar. Fresh garlic flavor may be
					  substituted for dehydrated. Mix it with the oil
					  first. Use virgin olive oil (with a greenish
					  color and distinct olive flavor).",
				"French Sour Dough" =>
					  "Use cottage cheese whey powder and
					  50 grain (5 percent) vinegar. Or, use one of the
					  commercial sour dough starters in place of those
					  two ingredients. (This is the quick method of
					  making a sour dough. The traditional way
					  involves 'building' a sour dough
					  starter over several days. For details refer to
					  a baking cookbook.) If you want more crust
					  browning, add 1 to 2 percent non-fat dry milk or
					  whey additive.",
				"Crostata" =>
					  "This is a very rich recipe that
					  borders on being a pastry dough. Note that it
					  calls for lard rather than oil.",
				"Diet-enhanced" =>
					  "This formula contains no sugar and,
					  because it's made with whole wheat flour,
					  is relatively high in fiber. It calls for using
					  whole wheat flour milled from white wheat as
					  opposed to the more-traditional red wheat.
					  That's because whole white wheat flour
					  has a less-bitter flavor. It also calls for the
					  flour being stone ground because that type of
					  grind has a larger particle size which converts
					  less-rapidly to glucose in the digestive system.
					  (Note that stone ground flour is not the same as
					  roller milled flour.) For info on white wheat
					  flour contact the American White Wheat Producers
					  Association, 913-367-4422.) Finally, the recipe
					  calls for olive oil because that oil is low in
					  saturated and poly-unsaturated fats."
					  );

	$g_ingr = array(
				"All Purpose" =>
				array(array(n => "High-gluten flour", p => (100 * $fs)),
					  array(n => "Water", p => 58),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 1),
					  array(n => "Salt", p => 2),
					  array(n => "Oil", p => 3),
					),
				"Fast Rising" =>
				array(array(n => "High-gluten flour", p => (100 * $fs)),
					  array(n => "Water", p => 59),
					  array(n => "INSTANT Yeast", p => 1.5),
					  array(n => "Sugar", p => 4),
					  array(n => "Salt", p => 1),
					  array(n => "Non-fat Dry Mild", p => 1),
					  array(n => "Oil", p => 2),
					),
				"Easy-to-sheeet" =>
				array(array(n => "Medium-gluten Flour", p => (90 * $fs)),
					  array(n => "Yellow Cornmeal", p => (10 * $fs)),
					  array(n => "Water", p => 50),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 3),
					  array(n => "Salt", p => 1),
					  array(n => "Oil", p => 8),
					  array(n => "Dough Relaxer", p => 2),
					),
				"High-browning" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 63),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 1),
					  array(n => "Salt", p => 2),
					  array(n => "Non-fat Dry Milk", p => 6),
					  array(n => "Oil", p => 2),
					),
				"Lean and Chewy" =>
				array(array(n => "High-gluten Flour", p => (85 * $fs)),
					  array(n => "Semolina", p => (15 * $fs)),
					  array(n => "Water", p => 55),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Salt", p => 2.5),
					  array(n => "Egg Whites", p => 2),
					),
				"Rich and Tender" =>
				array(array(n => "Medium-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 54),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 8),
					  array(n => "Salt", p => 1),
					  array(n => "Non-fat Dry Milk", p => 4),
					  array(n => "Egg Yolks", p => 2),
					  array(n => "Oil", p => 8),
					),
				"Thin and Crackery" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 40),
					  array(n => "Active Dry Yeast", p => 0.3),
					  array(n => "Sugar", p => 1),
					  array(n => "Salt", p => 2.5),
					  array(n => "Vegetable Oil", p => 2),
					  array(n => "Dough Relaxer", p => 1),
					),
				"Chicago Deep-dish" =>
				array(array(n => "Medium-gluten Flour", p => (84 * $fs)),
					  array(n => "Yellow Cornmeal", p => (16 * $fs)),
					  array(n => "Water", p => 44),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 2),
					  array(n => "Salt", p => 1),
					  array(n => "Oil", p => 10),
					),
				"Basic Neapolitan" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 59),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Salt", p => 2),
					),
				"Basic Sicilian" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 54),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Salt", p => 2),
					  array(n => "Oil", p => 10),
					),
				"Whole Wheat" =>
				array(array(n => "High-gluten Flour", p => (50 * $fs)),
					  array(n => "Whole Wheat Flour", p => (50 * $fs)),
					  array(n => "Water", p => 50),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 8),
					  array(n => "Salt", p => 2),
					  array(n => "Egs", p => 2),
					  array(n => "Oil", p => 4),
					),
				"Three-grain" =>
				array(array(n => "High-gluten Flour", p => (70 * $fs)),
					  array(n => "Three Grain Mix", p => (30 * $fs)),
					  array(n => "Water", p => 49),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 2),
					  array(n => "Salt", p => 2),
					  array(n => "Eggs", p => 3),
					  array(n => "Oil", p => 3),
					),
				"Higher-protein" =>
				array(array(n => "High-gluten Flour", p => (90 * $fs)),
					  array(n => "Soy Flour", p => (10 * $fs)),
					  array(n => "Water", p => 59),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 5),
					  array(n => "Salt", p => 2),
					),
				"Herb & Wine" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 52),
					  array(n => "White Wine", p => 6),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 3),
					  array(n => "Salt", p => 2),
					  array(n => "Garlic Powder", p => .06),
					  array(n => "Dry Leaf Herbs", p => .75),
					  array(n => "Oil", p => 4),
					),
				"Pepper-Cheese" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 58),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Salt", p => 2),
					  array(n => "Grated Cheese", p => 10),
					  array(n => "White Pepper", p => 0.5),
					  array(n => "Oil", p => 4),
					),
				"Beer" =>
				array(array(n => "High-gluten Flour", p => (95 * $fs)),
					  array(n => "Yellow Cornmeal", p => (5 * $fs)),
					  array(n => "Water", p => 44),
					  array(n => "Beer", p => 12),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 3),
					  array(n => "Salt", p => 2),
					  array(n => "Oil", p => 5),
					),
				"Garlic & Butter" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 57),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 1),
					  array(n => "Salt", p => 2),
					  array(n => "Garlic Powder", p => .08),
					  array(n => "Butter-flavored Oil", p => 5),
					),
				"Italian Accent" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 53),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 2),
					  array(n => "Salt", p => 2),
					  array(n => "Eggs", p => 2),
					  array(n => "Garlic Powder", p => .06),
					  array(n => "Virgin Olive Oil", p => 8),
					),
				"French Sour Dough" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 56),
					  array(n => "White Vinegar*", p => 7),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Salt", p => 2),
					  array(n => "Whey*", p => 10),
					  array(n => "Oil", p => 1),
					),
				"Crostata" =>
				array(array(n => "High-gluten Flour", p => (100 * $fs)),
					  array(n => "Water", p => 51),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Sugar", p => 2),
					  array(n => "Salt", p => 2),
					  array(n => "Non-fat Dry Milk", p => 2),
					  array(n => "Eggs", p => 3),
					  array(n => "Lard", p => 20),
					),
				"Diet-enhanced" =>
				array(array(n => "Stone Ground Whole White Wheat Flour", p => (100 * $fs)),
					  array(n => "Water", p => 63),
					  array(n => "Active Dry Yeast", p => 1),
					  array(n => "Salt", p => 2),
					  array(n => "Extra Light Olive Oil", p => 3),
					),
		 	 );

	function imperial_format($flour_cups, $percent)
	{
		$leftover = $flour_cups * ($percent / 100);

		if($leftover >= 16) {
			$temp = floor($leftover / 16);
			$s = $s . "$temp gallons, ";
			$leftover = $leftover - ($temp * 16);
		}
		if($leftover >= 0.5) {
			$temp = floor($leftover * 2);
			$temp = $temp / 2;
			$s = $s . "$temp cups, ";
			$leftover = $leftover - $temp;
		}

		$leftover = $leftover * 16; // convert to tablespoons
		if($leftover >= 1) {
			$temp = floor($leftover);
			$s = $s . "$temp Ts., ";
			$leftover = $leftover - $temp;
		}
		$leftover = $leftover * 3; // convert to teaspoons
		if($leftover > 0) {
			$temp = round($leftover,2); //floor($leftover);
			$s = $s . "$temp ts.";
			$leftover = $leftover - $temp;
		}
		return $s;
	}

	$cupPerPizza = 1.25; // cups of flour per pizza
	$flourCups = $nPizza * $cupPerPizza;

	echo "<BR><BR><BR>\n";
	echo "<table border=0 cellpadding=5 align=center bgcolor=\"#fefefe\">\n";
	echo "<tr valign=top>\n";
	echo "<td align=center bgcolor=\"#cccccc\">\n";
	echo "<font size=+2>PHPizza</font>\n";
	echo "</td></tr>\n";
	echo "<tr valign=top>\n";
	echo "<td align=center bgcolor=\"#aaaaaa\">\n";
	echo "</td></tr>\n";
	echo "<tr valign=center>\n";
	echo "<td align=center bgcolor=\"#aaaaaa\">\n";
	echo "<FORM action=\"pizza.php\" method=\"post\"><BR>\n";
	echo "	Number of Pizzas: <input type=text name=\"nPizzas\" value=$nPizza>\n";
	echo "	<BR><BR><select name=\"dough_type\">\n";
	foreach($g_ingr as $type_name => $value)
		echo "		<option value=\"$type_name\">$type_name</option>\n";
	echo "  	</select>\n";
	echo "  	<input type=\"submit\" value=\"Make Pizza\">\n";
	echo "</FORM>\n";
	echo "</td></tr></table>\n";
	echo "</CENTER>\n";
	echo "<BR><BR>\n";

if($dough_type != FALSE)
{
	echo "<table border=1 cellpadding=8 align=center ";
	echo "bgcolor=\"#fefefe\" width=\"60%\">\n";
	echo "<tr valign=top>\n";
	echo "<td align=center bgcolor=\"#aaaaaa\">\n";
	echo "<font size=+2>$nPizza <b>$dough_type</b> Pizzas</font>\n";
	echo "</td></tr>\n";
	echo "<tr valign=center>\n";
	echo "<td align=center bgcolor=\"#aaaaaa\">\n";

	echo "<table border=0 cellpadding=2 align=center>\n";
	$dtype = $g_ingr[$dough_type];
	foreach($dtype as $i => $ingr)
	{
		$name = $ingr["n"];
		$temp = imperial_format($flourCups, $ingr["p"]);
		echo "<tr><td align=right>$name</td><td>---</td><td>$temp</td></tr>\n";
	}
	echo "</table>\n";

	echo "</td></tr>\n";
	echo "<tr valign=top>\n";
	echo "<td align=center bgcolor=\"#aaaaaa\">\n";
	$notes = $g_notes[$dough_type];
	echo "$notes\n";
	echo "</td></tr>\n";
	echo "</table>\n";
}

printFooter();

require("debatefooter.php");

?>
