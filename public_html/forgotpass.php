<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");


printHeader();

?>

<div id="forgotpass">
Email me with a subject title of "debatepoint pass", and 
include your username somewhere in the body of the email.
I have to decrypt them manually, so It will probably take 
at most a few days to respond.  The response will be sent to 
the email address you used when you created your account.
<BR><BR>
James D. Taylor<BR>
james.d.taylor (a) gmail.com
</div> <!-- forgotpass -->

<?

printFooter();

require("debatefooter.php");

?>
