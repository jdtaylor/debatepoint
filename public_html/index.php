<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");

if(!isset($_GET['topic']))
	$topicId = -1;
else
	$topicId = do_num($_GET['topic'], "topic id");

if(!isset($_GET['offset']))
	$offset = 0;
else
	$offset = do_num($_GET['offset'], "offset");

printHeader();

echo "<div id=\"toolbar\">\n";
if($sessUserId == 1) // anonymous user
	printLogin();
printTopicList();
echo "</div> <!-- toolbar -->\n";

echo "<div id=\"contents\">\n";

if($topicId == -1)
{
	//echo "<BR>\n";
	echo "<div class=\"welcome\">\n";
	echo "<div class=\"welcome_title\">\n";
	echo "Welcome <b>$sessUserName</b>\n";
	echo "</div> <!-- welcome_title -->\n";
	echo "<div class=\"welcome_body\">\n";
	echo "Submit a point and ";
	echo "people can debate over it by posting arguments to which others ";
	echo "can moderate.  See the example debate on <a href=\"debate.php?sid=63&amp;mode=0\">";
	echo "'Drug Prohibition'</a>, or read about the moderation system in the <a href=\"/about.php\">about</a> section.\n";
	echo "</div> <!-- welcome_body -->\n";
	echo "</div> <!-- welcome -->\n";
}
else
	echo "Welcome <b>$sessUserName</b>\n";

$n = printActiveConsensus(20, $offset, $topicId < 0 ? 0 : $topicId);
//printRecentConsensus(20, 0);

if($offset > 0)
{
	$off = $offset - 20;
	if($off < 0)
		$off = 0;
	echo " <a href=\"index.php?offset=$off\">&lt;</a>";
}
else
	echo " &lt;";
echo " page ";

if($n >= 20)
{
	$off = $offset + 20;
	echo "<a href=\"index.php?offset=$off\">&gt;</a> ";
}
else
	echo "&gt; ";

echo "</div> <!-- contents -->\n";

printFooter();

require("debatefooter.php");

?>
