<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");

$id = do_num($_GET['id'], "bill id");

printHeader();

echo "Welcome $sessUserName\n";

function printBill($billRow)
{
	echo "<div id=\"bill\">\n";
	echo "<a href=\"bill.php?id=$billRow[billId]\">\n";
	echo "$billRow[title_short]:</a><BR><BR>\n";
	echo "$billRow[title]\n";
	echo "</div> <!-- bill -->\n";
}

function listBills()
{
	$query_handle = queryAllBillTitles();
	$billRow = queryNextRow($query_handle);

	echo "<div id=\"bill_list\">\n";
	echo "<ul>\n";

	while($billRow != FALSE)
	{
		echo "<li>\n";
		echo "<a href=\"bill.php?id=$billRow[billId]\">\n";
		echo "$billRow[type]$billRow[number]: $billRow[title_short]\n";
		echo "</li>\n";
		$billRow = queryNextRow($query_handle);
	}
	echo "</ul>\n";
	echo "</div> <!-- bill_list -->\n\n";

	queryFree($query_handle);
}

if($id == 0)
{
	echo "Listing all bills takes too long.  temporary disabled.\n";
	//listBills();
}
else
{
	$billRow = getBill($id);
	printBill($billRow);
}

printFooter();

require("debatefooter.php");
?>
