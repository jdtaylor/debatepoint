<?
/*
 * This file is part of debatepoint.com - web-based debate software
 *
 *   Copyright (C) 2004  James D. Taylor
 * 
 *   debatepoint is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   any later version.
 * 
 *   debatepoint is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with debatepoint; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact James D. Taylor:  james.d.taylor@gmail.com
 */

require("debateheader.php");

$id = do_num($_GET['id'], "debate id");
$sid = do_num($_GET['sid'], "stance id");
$aid = do_num($_GET['aid'], "argument id");

$sessArgRow = FALSE;
if($aid != 0)
{
	$sessArgRow = getArgument($aid);
	if($sessArgRow == FALSE)
		do_err("argument row is bad, id:$aid");
}

$sessStanceRow = FALSE;
if($sid != 0)
	$sessStanceRow = getStance($sid);

if($sessStanceRow == FALSE && $sessArgRow != FALSE) 
{
	// get stance from the argument
	$sid = $sessArgRow['stanceId'];
	$sessStanceRow = getStance($sid);
}

//
// handle single moderation
//
if(isset($_GET['mod']) && validUser($sessUserId))
{
	// moderating an argument

	$score = do_bool($_GET['mod'], "mod");
	if($score == FALSE)
		$score = -1;
	else
		$score = 1;

	if(newModerationSingle($sessUserId, $aid, $score) == FALSE)
		echo "<b>Moderation failed.</b><BR />\n";
	
	// TODO: policy.. should we be doing this?
	// set aid to the parent argument, else 0
	if($aid != 0)
	{
		$argumentRow = getArgument($aid);
		if($argumentRow != FALSE)
			$aid = $argumentRow['parentId'];
		$sid = $argumentRow['stanceId'];
	}
}

//
// this is the current debate display mode.  whether we are looking at
// arguments, comments, moderation, or a summary
$mode = $sessRow['mode'];
if(isset($_GET['mode']))
{
	$mode = do_num($_GET['mode'], "debate mode");
	if($mode < 0 || $mode > 3)
		$mode = 0; // argument lists

	$sessRow['mode'] = $mode; // debatefooter.php will save it
	$bSessModified = TRUE;
}

//
// current argument display style.  (whether threaded or not)
$argStyle = $sessRow['argStyle'];
if(isset($_GET['argStyle']))
{
	$argStyle = do_num($_GET['argStyle'], "argument style");
	if($argStyle < 0 || $argStyle > 3)
		$argStyle = 0; // argument lists

	$sessRow['argStyle'] = $argStyle; // debatefooter.php will save it
	$bSessModified = TRUE;
}

// save which arguments are being moderated (if any)
$modArg1 = $sessRow['modArg1'];
$modArg2 = $sessRow['modArg2'];
$modCount = $sessRow['modCount'];
$modArgRow1 = FALSE;
$modArgRow2 = FALSE;
$mod_result = -1;
$prevArg1 = -1;
$prevArg2 = -1;

// mode2 == moderation mode; only valid users can moderate
if($mode == 2 && validUser($sessUserId))
{
	//echo "DBG: moderating\n";

	// mod_result is the enumerated result of the moderation
	if(isset($_GET['mod_result']))
	{
		$mod_result = do_num($_GET['mod_result'], "mod result");
		if($mod_result < 0 || $mod_result > 5)
			do_err("invalid moderation result: $mod_result");

		//echo "DBG: moderating result: $mod_result 1:$modArg1 2:$modArg2\n";

		//
		// since mod_result is set, the user is moderating two arguments
		// which we've stored in the user's session.  nextModeration will
		// return two new argumentId's in modArg1 and modArg2.
		if(newModeration($sessUserId, $modArg1, $modArg2, $mod_result) == FALSE)
		{
			do_err("moderation failed. result:$mod_result 1:$modArg1 2:$modArg2");
			//echo "<b>Moderation failed.</b><BR />\n";
		}
		//
		// determine the next two arguments we will be moderatint
		$prevArg1 = $modArg1;
		$prevArg2 = $modArg2;
		nextModeration($sessUserId, $modArg1, $modArg2, $mod_result, $modCount);

		//echo "DBG: moderating next: $modArg1, $modArg2\n";
	}
	//
	// fetch the argument rows, and validate them
	$modArgRow1 = getArgument($modArg1);
	$modArgRow2 = getArgument($modArg2);
	if($mod_result < 0 || $modArgRow1 == FALSE || $modArgRow2 == FALSE)
	{
		nextModeration($sessUserId, $modArg1, $modArg2, $mod_result, $modCount);
		// get two new arguments to moderate
		$modArgRow1 = getArgument($modArg1);
		$modArgRow2 = getArgument($modArg2);
		if($modArgRow1 == FALSE || $modArgRow2 == FALSE)
			do_err("Perhaps you have exhausted all the arguments?");
	}
	// save which args we are moderating to the session
	$sessRow['modArg1'] = $modArg1;
	$sessRow['modArg2'] = $modArg2;
	$bSessModified = TRUE;

	//echo "DBG: moderating final next: $modArg1, $modArg2\n";
	//
	// we should have two valid arguments to moderate.  Reset the
	// debate to the correct parent argument.  Both arguments should
	// have the same parent.
	$aid = $modArgRow1['parentId'];
	$sessArgRow = getArgument($aid);
}

$sessArgRow = FALSE;
if($aid != 0)
{
	$sessArgRow = getArgument($aid);
	if($sessArgRow == FALSE)
		do_err("argument row is bad, id:$aid");
}

$sessStanceRow = FALSE;
if($sid != 0)
	$sessStanceRow = getStance($sid);

if($sessStanceRow == FALSE && $sessArgRow != FALSE) 
{
	// get stance from the argument
	$sid = $sessArgRow['stanceId'];
	$sessStanceRow = getStance($sid);
}
$pointRow = getPoint($sessStanceRow['pointId']);


// TODO: how safe is this?!
//$args = $_SERVER["argv"][0];
$args = FALSE;
if($sid != 0)
	$args = "sid=$sid";
if($aid != 0)
{
	if($args)
		$args .= "&aid=$aid";
	else
		$args .= "aid=$aid";
}


//
// argument sorting methods are long lasting (persistant), so we store
// them in the user table:
if(!validUser($sessUserId)) {
	$dfl_sort_method = $sessRow['sort_method'];
	$dfl_sort_reverse = $sessRow['sort_reverse'];
	$dfl_sort_filter = $sessRow['sort_filter'];
} else {
	$dfl_sort_method = $sessUserRow['sort_method'];
	$dfl_sort_reverse = $sessUserRow['sort_reverse'];
	$dfl_sort_filter = $sessUserRow['sort_filter'];
}
$sort_method = $dfl_sort_method;
$sort_reverse = $dfl_sort_reverse;
$sort_filter = $dfl_sort_filter;

if(isset($_GET['sort_method']))
{
	$sort_method = do_num($_GET['sort_method'], "sort method");
	if($sort_method <= 0 || $sort_method >= 5)
		$sort_method = $dfl_sort_method;

	$sort_reverse = $_GET['sort_reverse'] == "on" ? 1 : 0;
	$sort_filter = do_num($_GET['sort_filter'], "sort filter");
	if($sort_filter < -1.0 || $sort_filter > 1.0)
		$sort_filter = $dfl_sort_filter;

	if(!validUser($sessUserId)) {
		$sessRow['sort_method'] = $sort_method;
		$sessRow['sort_reverse'] = $sort_reverse;
		$sessRow['sort_filter'] = $sort_filter;
		$bSessModified = TRUE;
	}
	else
		updateUserPrefs($sessUserId, $sort_method, $sort_reverse, $sort_filter);
}

if($pointRow != FALSE)
	$header_description = "debatepoint.com: $pointRow[text]";

printHeader();


function listDebates()
{
	$query_handle = queryAllDebates();
	$debateRow = queryNextRow($query_handle);

	echo "<div id=\"debate_list\">\n";
	echo "<ul>\n";

	while($debateRow != FALSE)
	{
		echo "<a href=\"debate.php?id=$debateRow[debateId]\">\n";
		echo "<li>$debateRow[title]</li></a>\n";
		$debateRow = queryNextRow($query_handle);
	}
	echo "</ul>\n";
	echo "</div> <!-- debate_list -->\n\n";

	queryFree($query_handle);
}

function listStances($debateId)
{
	// select each candidate name in the debate
	$debateUsers = queryDebateUsers($debateId);
	$nUser = queryNumRow($debateUsers);;
	$userArray = array($nUser);

//	echo "<div class=\"stance_table\">\n";
	echo "<table id=\"stance_table\">\n";
	echo "<TR>\n";
	echo "<TD></TD>\n";

	for($i = 0; $i < $nUser; $i++)
	{
		$debateUserRow = queryNextRow($debateUsers);
		if($debateUserRow == FALSE)
			do_err("Unexpected end of debateUserRow");

		echo "<TD><a href=\"user.php?uid=$debateUserRow[userId]\">\n";
		echo "$debateUserRow[login]</a></TD>\n";

		$userArray[$i] = $debateUserRow['userId'];
	}
	echo "</TR>\n";
	queryFree($debateUsers);

	$stanceArray = getDebateStances($debateId);

	$debatePoints = queryDebatePoints($debateId);
	$debatePointRow = queryNextRow($debatePoints);
	while($debatePointRow != FALSE)
	{
		$pointId = $debatePointRow['pointId'];

		echo "<TR><TD>$debatePointRow[text]</TD>\n";

		for($i = 0; $i < $nUser; $i++)
		{
			$userId = $userArray[$i];

			echo "<TD>";
			if(isset($stanceArray[$pointId][$userId])) 
			{
				echo "<a href=\"debate.php?sid=".$stanceArray[$pointId][$userId][id]."\">";
				echo $stanceArray[$pointId][$userId]['stance'];
				echo "</a>\n";
			}
			echo "</TD>\n";
		}
		echo "</TR>\n";
		$debatePointRow = queryNextRow($debatePoints);

	}
	echo "</table>\n";
//	echo "</div> <!-- stance_table -->\n";

	queryFree($debatePoints);
}

if($sid == 0 && $aid == 0)
{
	printActiveConsensus(50, 0);
	//printRecentConsensus(50, 0);
	// this is code leftover from the 2004 presidential debate stuff
//	if($id == 0)
//		listDebates();
//	else
//		listStances($id);
}
else
{

	if($sessStanceRow == FALSE)
		do_err("Unknown stance for stanceId:$sid, argumentId:$aid");

	if($aid != 0)
	{
		$nReply = $sessArgRow['repliesSupport'] + $sessArgRow['repliesOppose'];
		$support = $sessArgRow['support'];
		$oppose = $sessArgRow['oppose'];
	}
	else
	{
		$nReply = $sessStanceRow['repliesSupport'] +
				  $sessStanceRow['repliesOppose'];
		$support = $sessStanceRow['support'];
		$oppose = $sessStanceRow['oppose'];
	}

	printStance($sessStanceRow);
	if($aid != 0)
	{
		echo "<ul>\n";
		echo "<li><p>";
		echo "<a href=\"debate.php?sid=$sid\">parent point</a>";
		echo "</p></li>\n";
		printArgumentLine($aid, ARG_DEFAULT | ARG_DETAIL);
		echo "</ul>\n";
	}

	echo "<div id=\"debate_header\">\n";

	echo "<div style=\"text-align:left; padding-left:10px; padding-top:10px\">\n";
	if($mode != 0)
		echo "<a href=\"debate.php?$args&amp;mode=0\">arguments</a>\n";
	else
		echo "<font size=\"+1\">arguments</font>\n";
	echo " &middot; \n";
	if($mode != 1)
		echo "<a href=\"debate.php?$args&amp;mode=1\">comments</a>\n";
	else
		echo "<font size=\"+1\">comments</font>\n";
	echo " &middot; \n";
	if($mode != 2) {
 		if($nReply > $mod_minarg)
			echo "<a href=\"debate.php?$args&amp;mode=2\">moderate</a>\n";
		else
			echo "moderate\n";
	}
	else
		echo "<font size=\"+1\">moderate</font>\n";
	echo " &middot; \n";
	if($mode != 3)
		echo "<a href=\"debate.php?$args&amp;mode=3\">summary</a>\n";
	else
		echo "<font size=\"+1\">summary</font>\n";
	echo "</div>\n";

	if($mode == 0 || $mode == 1)
	{
		// http://www.cs.tut.fi/~jkorpela/forms/extraspace.html
		echo "<div style=\"text-align:right; width:90%; margin-left:auto; padding-right:10px; padding-bottom:10px;\">\n";
		echo "<form style=\"display:inline; margin:0;\" action=\"debate.php?$args\" method=\"get\">\n";

		if($sid != 0)
			echo "<input type=\"hidden\" name=\"sid\" value=\"$sid\" />\n";
		if($aid != 0)
			echo "<input type=\"hidden\" name=\"aid\" value=\"$aid\" />\n";

		echo "style:";
		echo "<select name=\"argStyle\">\n";
		if($argStyle == 2)
			echo "<option value=\"2\" selected=\"selected\">threaded</option>\n";
		else
			echo "<option value=\"2\">threaded</option>\n";
		if($argStyle == 3)
			echo "<option value=\"3\" selected=\"selected\">columns</option>\n";
		else
			echo "<option value=\"3\">columns</option>\n";
		echo "</select>\n";

		echo "sort by:";
		echo "<select name=\"sort_method\">\n";
		if($sort_method == 1)
			echo "<option value=\"1\" selected=\"selected\">cross-stance</option>\n";
		else
			echo "<option value=\"1\">cross-stance</option>\n";
		if($sort_method == 2)
			echo "<option value=\"2\" selected=\"selected\">in-stance</option>\n";
		else
			echo "<option value=\"2\">in-stance</option>\n";
		if($sort_method == 3)
			echo "<option value=\"3\" selected=\"selected\">date</option>\n";
		else
			echo "<option value=\"3\">date</option>\n";
		if($sort_method == 4)
			echo "<option value=\"4\" selected=\"selected\">replies</option>\n";
		else
			echo "<option value=\"4\">replies</option>\n";
		echo "</select>\n";
		if($sort_reverse == 1)
			echo "reverse: <input type=\"checkbox\" name=\"sort_reverse\" checked=\"checked\" />\n";
		else
			echo "reverse: <input type=\"checkbox\" name=\"sort_reverse\" />\n";
		echo "<select name=\"sort_filter\">\n";
		if($sort_filter == -1.0)
			echo "<option value=\"-1.0\" selected=\"selected\">all</option>\n";
		else
			echo "<option value=\"-1.0\">all</option>\n";
		if($sort_filter == -0.75)
			echo "<option value=\"-0.75\" selected=\"selected\">-75%</option>\n";
		else
			echo "<option value=\"-0.75\">-75%</option>\n";
		if($sort_filter == -0.5)
			echo "<option value=\"-0.5\" selected=\"selected\">-50%</option>\n";
		else
			echo "<option value=\"-0.5\">-50%</option>\n";
		if($sort_filter == -0.25)
			echo "<option value=\"-0.25\" selected=\"selected\">-25%</option>\n";
		else
			echo "<option value=\"-0.25\">-25%</option>\n";
		if($sort_filter == 0.0)
			echo "<option value=\"0.0\" selected=\"selected\">&gt;=0%</option>\n";
		else
			echo "<option value=\"0.0\">&gt;=0%</option>\n";
		if($sort_filter == 0.1)
			echo "<option value=\"0.1\" selected=\"selected\">10%</option>\n";
		else
			echo "<option value=\"0.1\">10%</option>\n";
		if($sort_filter == 0.25)
			echo "<option value=\"0.25\" selected=\"selected\">25%</option>\n";
		else
			echo "<option value=\"0.25\">25%</option>\n";
		if($sort_filter == 0.5)
			echo "<option value=\"0.5\" selected=\"selected\">50%</option>\n";
		else
			echo "<option value=\"0.5\">50%</option>\n";
		if($sort_filter == 0.75)
			echo "<option value=\"0.75\" selected=\"selected\">75%</option>\n";
		else
			echo "<option value=\"0.75\">75%</option>\n";
		echo "</select>\n";
		echo "<input type=\"submit\" value=\"change\" />\n";
		echo "</form>\n";
		echo "</div>\n";
	}
	echo "</div> <!-- debate_header -->\n";

	switch($mode)
	{
	case 0:
		// debate arguments

		switch($argStyle)
		{
		case 0:
		case 1:
		case 2:
			echo "<div id=\"arguments_header\">\n";
			echo "<a href=\"newargument.php?sid=$sid&amp;aid=$aid&amp;stance=1\">";
			echo "[make supporting argument]</a> &middot; &middot; &middot; \n";
			echo "<a href=\"newargument.php?sid=$sid&amp;aid=$aid&amp;stance=0\">";
			echo "[make opposing argument]</a>\n";
			echo "</div> <!-- arguments_header -->\n";

			$argOpt = ARG_DEFAULT | ARG_STANCE | ARG_MOD;

			printArgumentThread($sid, $aid, -1, 20, $sort_method,
								$sort_reverse, $sort_filter, $argOpt);
			break;
		case 3:
			echo "<table width=\"100%\"><tr>\n";
			echo "<td width=\"50%\" valign=\"top\">\n";

			echo "<div id=\"arguments_header_support\">\n";

			$argOpt = ARG_DEFAULT & ~ARG_STANCE;
			//
			// remnant from the time when moderation was all relative..
			// ie. no stancePos, stanceNeg, or stanceEffective
			//
			//if($nReply <= $mod_minarg) // single moderate?
			$argOpt |= ARG_MOD;

			echo "<i>supporting arguments</i>\n";
			printf(" %d%% &middot; \n", $support * 100);
			echo "<a href=\"newargument.php?sid=$sid&amp;aid=$aid&amp;stance=1\">";
			echo "[make argument]</a>\n";

			echo "</div> <!-- arguments_header_support -->\n";

			printSupportArguments($sid, $aid, $sort_method,
								$sort_reverse, $sort_filter, $argOpt);

			echo "\n</td>\n";
			echo "<td width=\"50%\" valign=\"top\">\n\n";

			echo "<div id=\"arguments_header_oppose\">\n";


			echo "<i>opposing arguments</i>\n"; //<hr>\n";
			printf(" %d%% &middot; \n", $oppose * 100);
			echo "<a href=\"newargument.php?sid=$sid&amp;aid=$aid&amp;stance=0\">";
			echo "[make argument]</a>\n";

			echo "</div> <!-- arguments_header_oppose -->\n";

			printOpposeArguments($sid, $aid, $sort_method,
								$sort_reverse, $sort_filter, $argOpt);

			echo "</td>\n";
			echo "</tr></table>\n";
		}
		break;
	case 1:
		// comments
		echo "<div id=\"comments_header\">\n";

		echo "comments<BR />\n"; //<hr>\n";
		echo "<a href=\"newargument.php?sid=$sid&amp;aid=$aid&amp;stance=2\">";
		echo "[make comment]</a> &middot; sort by: [todo]\n";

		echo "</div> <!-- comments_header -->\n";
		echo "<div id=\"comments\">\n";

		printCommentArguments($sid, $aid, $sort_method,
								$sort_reverse, $sort_filter);

		echo "</div> <!-- comments -->\n";
		break;
	case 2:
		// moderation
		$bSkip = FALSE;

		if(!validUser($sessUserId))
		{
			echo "Only logged in users can moderate.";
			echo "<div id=\"login_container\">\n";
			printLogin();
			echo "</div> <!-- login_container -->\n";
			$bSkip = TRUE;
		}

		if($nReply <= $mod_minarg)
		{
			echo "Cross-stance moderation is only valid when there is a minimum of $mod_minarg child arguments.";
			$bSkip = TRUE;
		}

		if(!$bSkip)
		{
			// don't show current score to moderators
			$optArg = ARG_DEFAULT & ~ARG_SCORE;

			echo "<div id=\"moderation_header\">\n";
			echo "moderate\n";
			if($prevArg1 > 0 && $prevArg2 > 0)
			{
				$prevArgRow1 = getArgument($modArg1);
				$prevArgRow2 = getArgument($modArg2);
				$score1 = round($prevArgRow1['effective'] * 100);
				$score2 = round($prevArgRow1['effective'] * 100);
				echo "<div id=\"moderation_result\">\n";
				if($mod_result == 0)
					echo "<b>arg1:$score1%</b> &middot; arg2:$score2%\n";
				else
					echo "arg1:$score1% &middot; <b>arg2:$score2%</b>\n";
				echo "</div> <!-- moderation_result -->\n";
			}
			echo "</div> <!-- moderation_header -->\n";

			$modRow = getModeration($modArgRow1['argumentId'],
									$modArgRow2['argumentId'], $sessUserId);

			echo "<div id=\"moderation\">\n";
			echo "<table width=\"100%\"><tr>\n";
			echo "<td width=\"33%\">\n";
			echo "<a href=\"debate.php?$args&amp;mod_result=0\">\n";
			echo "argument1 wins</a>\n";
			if($modRow != FALSE && $modRow['result'] == 0)
			{
				echo "\n<BR />(previous winner)\n";
			}
			echo "</td><td width=\"33%\">\n";
			echo "<a href=\"debate.php?$args&amp;mod_result=2\">\n";
			echo "redundant</a><br/>\n";
			echo "<a href=\"debate.php?$args\">\n";
			echo "skip it</a>\n";
			echo "</td><td width=\"33%\">\n";
			echo "<a href=\"debate.php?$args&amp;mod_result=1\">\n";
			echo "argument2 wins</a>\n";
			if($modRow != FALSE && $modRow['result'] == 1)
			{
				echo "\n<BR />(previous winner)\n";
			}
			echo "</td></tr></table>\n";

			echo "<table><tr>\n";
			echo "<td width=\"47%\" style=\"text-align:left;\" valign=\"top\">\n";
			printArgument($modArgRow1, $optArg);
			echo "</td><td width=\"3%\" valign=\"top\">\n";
			echo "VS\n";
			echo "</td><td width=\"47%\" style=\"text-align:left;\" valign=\"top\">\n";
			printArgument($modArgRow2, $optArg);
			echo "</td></tr></table>\n";

			echo "</div> <!-- moderation -->\n";
		}
		break;
	case 3:
		// summary
		echo "<div id=\"summary_header\">\n";
		echo "summary\n";
		echo "</div> <!-- summary_header -->\n";

		echo "<div id=\"summary\">\n";
		if($aid != 0)
		{
			$nReply = $sessArgRow['repliesSupport'] + $sessArgRow['repliesOppose'];
			$support = $sessArgRow['support'];
			$oppose = $sessArgRow['oppose'];
		}
		else
		{
			$nReply = $sessStanceRow['repliesSupport'] + $sessStanceRow['repliesOppose'];
			$support = $sessStanceRow['support'];
			$oppose = $sessStanceRow['oppose'];
		}
		echo "<br/>\n";
		if($aid == 0)
		{
			$pointRow = getPoint($sessStanceRow['pointId']);
			if($pointRow != FALSE) {
				echo "text: '$pointRow[text]'<BR />\n";
				echo "tags: $pointRow[tags]<BR />\n";
				echo "date: $pointRow[date]<BR />\n";
				$userRow = getUser($pointRow['submitter']);
				if($userRow != FALSE) {
					echo "submitter: <a href=\"user.php?uid=$userRow[userId]\">";
					echo "$userRow[login]</a><br/>\n";
				}
			}
			echo "effective: $sessStanceRow[effective]<br/>\n";
			echo "support: $sessStanceRow[support]<br/>\n";
			echo "oppose: $sessStanceRow[oppose]<br/>\n";
			echo "certainty: $sessStanceRow[certainty]<br/>\n";
			echo "repliesSupport: $sessStanceRow[repliesSupport]<br/>\n";
			echo "repliesOppose: $sessStanceRow[repliesOppose]<br/>\n";
			echo "repliesTotal: $sessStanceRow[repliesTotal]<br/>\n";
			echo "comments: $sessStanceRow[comments]<br/>\n";
			echo "modified: $sessStanceRow[modified]<br/>\n";
		}
		else
		{
			echo "title: '$sessArgRow[title]'<BR />\n";
			echo "text: '$sessArgRow[text]'<BR />\n";
			echo "tags: $sessArgRow[tags]<BR />\n";
			echo "date: $sessArgRow[date]<BR />\n";
			$userRow = getUser($sessArgRow['userId']);
			if($userRow != FALSE) {
				echo "submitter: <a href=\"user.php?uid=$userRow[userId]\">";
				echo "$userRow[login]</a><br/>\n";
			}
			echo "effective: $sessArgRow[effective]<br/>\n";
			echo "support: $sessArgRow[support]<br/>\n";
			echo "oppose: $sessArgRow[oppose]<br/>\n";
			echo "stancePos: $sessArgRow[stancePos]<br/>\n";
			echo "stanceNeg: $sessArgRow[stanceNeg]<br/>\n";
			echo "stanceEffective: $sessArgRow[stanceEffective]<br/>\n";
			echo "repliesSupport: $sessArgRow[repliesSupport]<br/>\n";
			echo "repliesOppose: $sessArgRow[repliesOppose]<br/>\n";
			echo "repliesTotal: $sessArgRow[repliesTotal]<br/>\n";
			echo "comments: $sessArgRow[comments]<br/>\n";
		}
		echo "<br/>\n";
		echo "</div> <!-- summary -->\n";
		break;
	}
}

echo "<script type=\"text/javascript\">\n";
echo "<!--\n";
echo "var jg = new jsGraphics(\"stanceCanvas\");\n";
echo "jg.setColor(\"#FFFF55\");\n";
$x = 210;
$y = 140;
getTristancePos($sessStanceRow, $x, $y);
$x = round($x-4);
$y = round($y-5);
echo "jg.fillEllipse($x, $y, 10, 10);\n";
echo "jg.paint();\n";
echo "//-->\n";
echo "</script>\n";
//echo "x:$x y:$y<BR />\n";

printFooter();

require("debatefooter.php");
?>
